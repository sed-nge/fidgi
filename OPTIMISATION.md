# Optimisation ideas

Some optisations tested by Akira:
 - Optimised a few fbx items but minimal optimisation seems to be working.
        - Removal of `MURS - mur solid` is the best solution.
- Baking the lighting: an option in Unity Dev
        - With `Quest 2` there doesn't seem to be a need to bake the lighting to be functional, but of cause it can be added.
            - `Mixed` would be better since `Baked` adds too much shadows to the objects and it's not that beautiful.
            - Time indicated for `Bake` is not always correct. 3 days have finished in a few hours on a `2.8 GHz Quad-Core Intel Core i7` Mac with `NVIDIA GeForce GT 750M (2 GB)` GPU & also the same for the Windows Expo computer.
- More optimisation is needed if the target is to include Quest.
