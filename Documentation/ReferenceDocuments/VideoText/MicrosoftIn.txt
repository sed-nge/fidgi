Optimisation: A common tool for the common task.
Optimisation : un outil commun pour des tâches communes.

A computer is a tool where we provide input to receive output.
Un ordinateur est un outil avec des entrées et des sorties.
Unlike a spanner, the computer is not limited as a tool by its shape.
Contrairement à un marteau, l'ordinateur n'est pas limité par sa forme.
A computer is limited by the programs that run on it, and the hardware that are connected to it.
Un ordinateur est limité par le programme qu'il exécute et par le matériel qui y est connecté.

Specialized computers such as Flight Management Computers have been flying planes since the early 1980s, while Apollo 11 landed on the moon with computer aide in 1969.
Des ordinateurs spécialisés tels que ceux de gestion de vol ont été utilisés sur les avions depuis les années 1980, alors qu'Apollo 11 alunissait en 1969 aidé par un ordinateur.
Even the weather has been predicted on computers since the 1950s.
Même la météo est prédite sur des ordinateurs depuis les années 1950.

However, the normal office worker could not justify the need for a computer.
Cependant, l'employé de bureau ordinaire ne ressentait pas le besoin d'un ordinateur.
That was until the spreadsheet arrived!
C'était avant l'apparition des tableurs !
A humble analyst with a spreadsheet could become a near genius.
Un modeste analyste avec un tableur pouvait approcher le génie.
It became possible, to instantly recalculate a whole line of sums, just by changing one number.
Il devenait possible de recalculer instantanément une suite complexe d'opérations, juste après avoir changé un nombre.

Seeing the success of startups such as Apple, the industry leader, IBM, entered the Personal Computer market with Microsoft’s PC-DOS as the operating system, and the then industry standard programming language of Microsoft-BASIC preinstalled.
Voyant le succès de jeunes entreprises telles qu'Apple, le leader de l'industrie informatique, IBM, entra dans le marché des ordinateurs personnels avec le système d'exploitation PC-DOS de Microsoft et le langage de programmation Microsoft-BASIC associé.

With the success of IBM-PC, software developers created applications that would run on the computer that used Microsoft’s BASIC interpreter.
Au vu du succès de l'IBM-PC, les développeurs de logiciel ont crée des applications qui pouvaient s'exécuter sur l'ordinateur en utilisant l'interpréteur BASIC de Microsoft.

Though Microsoft’s operating system may not be the first choice of researchers, artists and back end operations, to this day, Microsoft has never let go of its office computer dominance.
Le système d'exploitation de Microsoft n'est peut-être pas le premier choix des chercheurs, des artistes et des serveurs centraux, mais depuis ce jour Microsoft n'est jamais sorti de sa domination dans la bureautique.