The computer today, is a machine that computes (calculates).
L'ordinateur aujourd'hui, est une machine qui calcule.
Before the modern day computer was created, there were mechanical and electrical machines that could calculate. However complex, or sophisticated these devices were, they were often limited to a specific task, or the calculation was not fully automated. 
Avant l'avènement de l'ordinateur moderne actuel, il y avait des machines électromécaniques qui pouvaient calculer. Cependant quelque soit la complexité et la sophistication de ces machines, elles se limitaient souvent à une tâche spécifique, ou le calcul n'était pas complètement automatique.
In 1936, this theory defined what a computer should be.
En 1936, cette théorie a défini ce que devait être un ordinateur.

The automatic machine:
La machine automatique :

1.) The machine is programmable. 
1.) La machine est programmable.
    It can read input and automatically run (execute) a calculation.
    Elle peut lire une entrée et effectuer automatiquement un calcul.

2.) The machine is able to output the result.
2.) La machine est capable de sortir le résultat.

3.) The machine is able to solve a solvable calculation and stop calculating.
3.) La machine est capable de calculer une fonction calculable et d'arrêter le calcul.

4.) The machine is not able to solve unsolvable calculations.
4.) La machine n'est pas capable de calculer une fonction non calculable.

5.) The input will result in the same output on a different machine.
5.) La même entrée sur une autre machine donnera la même sortie.

This may sound extremely simple, and obvious today, but these simple 5 rules are the fundamental concept of modern computers and provided the proof in his examples that allowed mathematicians and scientists to focus their ideas in a unified direction.
Ceci peut sembler extrêmement simple, et évident aujourd'hui, mais ces cinq règles forment le fondement des ordinateurs modernes et fournissent la preuve dans ses exemples d'amener les mathématiciens et scientifiques à combiner leurs idées dans un but commun.
In other words, creating an automatic machine (otherwise known as the Turing machine) that could fulfil these 5 rules became the new problem to solve and basically how a CPU performs today.
Créer une machine automatique (connue sous le nom de machine de Turing) qui pouvait remplir ces cinq règles devint le nouveau problème à résoudre et aboutira au fonctionnement des CPU de nos jours.

This is not the only contribution this scientist left for us. He also created a machine to crack the enigma code, and went on to define a test to measure artificial intelligence.
Ce n'est pas la seule contribution que ce chercheur nous laisse. Il a aussi créé une machine pour décrypter le code Enigma, et imagina un test pour mesurer une intelligence artificielle.
