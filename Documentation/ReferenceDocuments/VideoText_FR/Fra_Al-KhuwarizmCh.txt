Nous ne savons pas grand chose de cet homme. On raconte qu’il vient d’une région qui se situe actuellement en Ouzbékistan, environ au début de l’âge d’or de l’Islam. 
Cependant, ce que nous savons sur lui vient du travail qu’il fit comme membre de la Maison de la Sagesse de Bagdad. 

Sa contribution, spécialement en Mathématiques, Astronomie, et Histoire-géographie est le résumé des connaissances scientifiques Hindoues et Grecques. 
Rédigés pour être compris par le plus grand nombre, ses manuscrits ont été traduits en Latin par des érudits européens et servirent de base à de futures études.