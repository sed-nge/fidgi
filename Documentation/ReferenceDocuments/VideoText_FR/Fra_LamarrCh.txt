Cet inventeur a un brevet à son nom, « US Patent 2,292,387 », mais elle se rendit célèbre comme actrice.  
Sa carrière a commencé en tant qu'actrice apparaissant au cinéma et au théâtre, et grâce à son premier mariage, elle a beaucoup appris sur les armes militaires de l'époque. 
C'est à partir de cette connaissance qu'elle a créé son invention, l’étalement de spectre par saut de fréquence (FHSS).

Au lieu d'utiliser une seule fréquence radio, son idée était de sauter entre différentes fréquences et de créer une méthode de communication qui pourrait éviter les interférences. 
Elle a notamment déposé un brevet sur les systèmes de radioguidage et de télécommunication, un peu en avance sur son temps… il faut attendre les développements de l’électronique pour que les constructeurs de matériels de transmission utilisent la « technique Lamarr ».