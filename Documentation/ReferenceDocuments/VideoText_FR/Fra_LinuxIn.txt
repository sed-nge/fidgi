Quelle invention suis-je?

Le logiciel est de la connaissance, et la connaissance devrait être publique.

Quand un logiciel est écrit, normalement il satisfait un besoin, afin de rendre la vie plus facile.
Imaginez que votre imprimante est loin de vous.
Vous ne voulez pas aller jusqu'à l'imprimante pour découvrir qu'elle n'a pas fini d'imprimer.
Des petites améliorations peuvent être ajoutées à l'imprimante, si son logiciel est accessible.
Mais si elle utilise un système fermé, c'est-à-dire la connaissance pour ajouter une telle fonctionnalité n'est pas disponible, vous êtes limité à ce qui est fourni.

Étendre ce principe à tout l'ordinateur et au système devient extrêmement complexe.
Une personne ne peut pas tout faire, mais si il y a beaucoup de volontaires pour aider, alors la charge de travail et l'expertise nécessaire peuvent être partagées.
Ce modèle de partage, ou ce modèle de développement logiciel décentralisé est la colonne vertébrale de la collaboration libre et opensource (logiciel au code source accessible).

Cependant, un tel modèle ne marcherait pas si des protections n'étaient pas en place pour en autoriser le prolongement.
Les personnes ne veulent pas contribuer su elles voient d'autres tricher avec le système, ou si ils sentent que leurs efforts sont dans une impasse.
C'est la qu'arrive la General Public License (GPL) pour garder les efforts des contributeurs ouverts et accessibles aux autres.

Cette philosophie de la contribution ouverte est ce qui a fait de Linux le système d'exploitation le plus utilisé au monde.
Même si seulement 2% des ordinateurs de bureau en disposent et vous le rencontrez peu souvent, la plupart des super ordinateurs, des serveurs dans le nuage, des routeurs Internet, des smartphones (système Androïd) et même des TVs sont sous Linux.

Je suis la projet GNU (Open source) & du noyau Linux, créa par Richard Stallman & Linus Torvalds.