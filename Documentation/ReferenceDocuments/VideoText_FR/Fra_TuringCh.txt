Il est un personnage que l’on reconnaît comme 
un des pères de l’informatique moderne. Il a proposé en 1936, 
avant l’apparition des premiers ordinateurs, une formalisation 
de la notion de calcul et de ses limites intrinsèques, toujours 
pertinentes 80 ans après ! (Même si von Neumman est crédité pour avoir popularisé ses théories.) 

Comme la plupart de son travail a été fait autour de la Seconde Guerre mondiale, nous ne saurons jamais tout ce qu’il réalisa à cause du secret autour de son travail. Mais ce que nous connaissons c’est son apport au craquage du code Enigma, la création de La Bombe et son intuition que l’intelligence artificielle, un jour, imiterait un humain.