In physics, we are able to measure things with grams or metres. For a computer, things are measured and represented in bits (0 and 1) and it is thanks to the invention of this theory that we know that it is possible.
However, before continuing, an important thing to understand is that the digital world is not perfect.

1.) There is a limit to size, power and speed. For example your mobile phone probably has limits such as a fixed storage, a defined display resolution, processing speed and communication speed (3G, 4G, or 5G).

2.) When information is sent from A to B, it is not guaranteed that it will be 100% the same. For example, if you use your mobile phone beside a microwave heating a dish, the telephone signal will be negatively affected.

Communicating digital information with these two restrictions in mind is the starting point of this theory, and the starting point of the field this scientist created.

The question is how much information is needed to identify “something”, and more importantly, what is the smallest amount of information needed.
At which point is this a photo?


This theory is what is used to make MP3 music files, JPEG image files, video files & ZIP files, etc…

The next question is, how do you verify that the information you have is similar or the same as the original.
For example, if I want to communicate “30-04” but I know that the communication method will be affected by interference I could set the rule that all digits are repeated 3 times as 333000---000444. 


Using methods similar to this is how you can still listen to CDs with scratches on the surface.

This may sound simple and already completed, but now think that the limits of size, power and speed have changed. With the changes in limits, expectations and definitions of adequate minimum information changes.
