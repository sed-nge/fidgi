Human computer interaction: THE HUMAN!

Simple and intuitive. It is a phrase that is repeated a lot in design.
The difficulty of this concept is that what is intuitive for me may not be intuitive for you and the same goes with simplicity.

Features and specs. This too is a mine field to enter. The merit of a device that has the most features that a user wants and the highest spec is easy to explain. But what I want is different from what you want. So what features are needed? Is the information you are hearing coming from the loud minority or just the tip of the iceberg?

The founders of this company saw the consumer’s desire for Personal Computers and they had the skill to make and sell them.

The understanding of consumers continues with how information is displayed on the screen.
Until then, lines of text was the standard, but this company introduced images to display files, applications and folders.
With this they also made the computer mouse a standard accessory for the computer.
Both these things were already invented, but they just weren't a standard feature of the computer.

Even though this company nearly went bankrupt in 1997, it has still continued to identify the moments when consumers are ready for new technologies.
More importantly, they had an understanding of how intelligent, but not tech-savvy people, wanted to interact with devices.

Queue the iPod, the iPhone, the iPad, simple network configuration with AirPort, easy backups with TimeMachine, plug and play for connected devices, synchronizing data between multiple devices, etc.

Just like Grace Hopper and Margerat Hamilton did for programming and coding, this company has a good understanding of what should be done by the human user, and what is better for the computer, to do for them.