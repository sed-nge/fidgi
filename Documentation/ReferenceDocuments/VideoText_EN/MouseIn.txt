Selecting items on the screen.

How do you select items or navigate your way around the folders and files in a computer.

The terminal is a way to interact with the computer with only the keyboard.
You can run programs, execute commands, read and write files, even email someone.
Then there is an application called Emacs.
A multi-purpose application that made these tasks a lot easier than using the terminal.
Some people, to this day, only use this app to do the main tasks of their work.

If options like this, that only use the keyboard to interact with the computer, were the only means, computers would still be a rare item that only researchers, engineers and trained clerks use.

How can you draw with only a keyboard?
Of course it’s possible, but the effort required for the return is too much for the majority to try.

When the computer screen changed, from only displaying text, to having a graphical user interface, the importance of this question increased.
Joysticks, Light pens, styluses, mice, touch screens, track pads, etc.

A variety of options existed and still do, but which is the optimal choice?
Cheap to manufacture, easy for a human to control, isn't limited by size or expensive hardware.

Though Apple didn’t invent it, Apple was the company that made it popular, when it supplied it with the first Macintosh computer and designed the GUI to be used with this pointing device.

The future of this device as a pointer is unknown, but it will surely stay around for as long as we have a physical keyboard.