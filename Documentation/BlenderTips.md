# Blender tips:  

## File location
To make Unity update all the blender items when blender items are modified, place them in the Asset folder:  
- For Fidgi, place them in `/fidgi/Src/Fidgi/Assets/Fidg/Items/`.  
- Place a copy of the master file in `/fidgi/Documentation/RawFiles/`.

## Editing  
### Importing items  
Importing Blender items from different blender files select the file with the item you want to copy from:  
- `File -> Append...`  

### [Grouping objects](https://all3dp.com/2/blender-how-to-group-objects/)  
Parent - Child Relationship (Reversable)  
- Select items to be the `Child` first, and then last of all, select the item to be the `Parent` and press `Ctrl+P`.  
- To remove linkage, select items and press `Alt+P`.
- Not yet verified what will happen with `Shift+P`.


### Before Importing files to Unity:
1. Delete any `light` and `cameras` used in the Blender scene.
2. Make sure to apply the rescale so that the `Dimensions` of the object become actual and not reliant on ***scaling*** (x, y, z = 1.0, 1.0, 1.0).  
Refer [here](https://blender.stackexchange.com/questions/47318/why-do-the-measurements-of-this-object-seem-erroneous/47320#47320) for details.  
    - Select the item then `Ctrl+A` -> `Scale`  
3. Make sure that the `Rotation` is set to 0 (x, y, z = 0d, 0d, 0d) before importing to Unity.  
    - Select the item then `Ctrl+A` -> `Rotate`  
4. Make sure that the `Location` is set to 0 (x, y, z = 0m, 0m, 0m) before importing to Unity.  
    - Select the item then `Ctrl+A` -> `Location`


### Subdivide
- `Edit Mode` -> `Right Click` -> `S`.  
- `Edit Mode` -> `Shift + Space` -> `Ctrl + R`.  

### To add thickness to sides
- `Edit Mode` -> `E` -> pull face in direction you want to add thickness.  
