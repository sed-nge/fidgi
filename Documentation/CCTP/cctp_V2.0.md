---
title: Inside V2.0 Cahier des Charges Techniques Particulières (CCTP)
subtitle:  
author: Bertrand.Wallrich@inria.fr
date: 18/03/2022
---


# Introduction

Inside V0.1 est un jeu en VR (réalité virtuelle) développé sur Oculus par Inria.
Dans ce jeu, le joueur est plongé dans un ordinateur (une tour de *gamer*) et doit manipuler des mémoires (représentées par des tablettes transparentes). Les mémoires contiennent chacune une information sur un inventeur, ou une invention.

En plaçant une mémoire dans le lecteur vidéo, cette mémoire est lue. Le joueur découvre alors son contenu.
En faisant se toucher deux cartes, ces dernières fusionnent si elles correspondent (inventeur - invention).


Le développement d'un jeu vidéo est complexe. Le résultat final n'est pas unique.

## Objectif du jeu

Jeu de type « *escape game* » en réalité virtuelle, pour apprendre l’histoire de l’informatique, ses grands noms et ses grandes avancées.
Ce jeu s'utilise sur un casque  *Oculus Quest2*. Le joueur est à l’intérieur d’un ordinateur et doit manipuler des « mémoires », découvrir ce qu’elles contiennent, et les associer pour ensuite les sauvegarder.

La batterie de l'ordinateur diminue régulièrement. Le joueur doit réaliser quatre bonnes associations et les sauvegarder dans le disque dur (SSD) avant l’extinction du PC.
D’autres éléments perturbent le joueur durant sa découverte des mémoires et ses tentatives d’association.

Enfin, dans un contexte  de classe, le contenu du casque est diffusé sur un écran aux autres élèves. Ces derniers peuvent ainsi participer en aidant le joueur.

### Objectifs pédagogiques

* Objectif pédagogique n°1 :

    Découvrir l’histoire de l’informatique. Comprendre que le numérique n’est pas arrivé ces dernières années mais que nos ordinateurs descendent des mathématiques et prennent racine dans l’histoire de l’humanité.

* Objectif pédagogique n°2 (secondaire) :

    Donner envie aux collégiens / lycéens au travers de ce jeu, d’embrasser une carrière dans les mathématiques et le numérique. L’immersion dans un ordinateur, les grandes avancées présentées de manière ludique sont là pour l’aider à se dire « oui c’est possible pour moi ».

### Notions abordées

Les notions abordées sont l’histoire de l’informatique, les grands hommes et femmes qui y ont contribué, la grande et la petite histoire qui se cachent derrière nos ordinateurs et téléphones intelligents actuels.

Des notions complexes telles que la logique booléenne, l’algorithmique, la compilation, les réseaux de données sont abordées de manière simplifiées et les plus percutantes possible.

### Exemple de rendu du jeu

![](./images/OculusScreenshot1633081239.jpeg)

![](./images/OculusScreenshot1633081265.jpeg)

![](./images/OculusScreenshot1633081340.jpeg)

![](./images/OculusScreenshot1633081363.jpeg)


## Données techniques

Le jeu dans sa version actuelle représente :

|||
|--|--|
| Taille totale | 16Go |
| Animations | 34 |
| Objets 3D (GameObjects) | 592 |
| Vidéos (hors périmètre de ce CCTP)| 45 |
| Phrases prononcées durant le jeu | 152 |
| Nombre de lignes de code (*C#*) | 8635 |

# Evolutions à réaliser

Les évolutions portent sur 4 domaines :

| Catégorie    | Temps estimé |
| ------------ | ------------ |
| Multiplayer  | 9w           |
| Oculus Store | 5w           |
| Éducation    | 8w           |

## Multiplayer (9W)

Le jeu pouvant se jouer dans une salle avec un groupe de personnes, le joueur portant le casque est seul. Il s'agit de faire participer les autres (*ceux qui regardent*) au jeu. Actuellement ils disposent d'un écran sur lequel est diffusé ce que voit le joueur.

### Vue salle

Nous estimons qu'il faut faire une **vue différente**, avec des éléments complémentaire au jeu, afin d'aider le joueur avec le casque :

* Des informations sur les paires possibles,
* Des informations sur les virus,
* Information sur le temps restant,
* ...

Cette vue sera nommée **vue salle**.
Le style visuel peut être une vue *station de contrôle devant les écrans de surveillance*.

Techniquement, cela représente une vue différente (autre caméra) et surtout un passage **MLAPI** du jeu. Cette vue fonctionnera sur PC.

Cette vue aura également des objets 3D ou 2D (UI) associés. Il convient de définir avec l'équipe technique les éléments précis de cette vue. Plusieurs *mockups* seront fournis en annexe.

Le clavier ne sera utilisé que pour les choses suivantes :

* Dans la salle de tutoriel
    * Relancer le jeu sans réinitialiser les scores
    * Relancer le tutoriel
    * Entrer le nom du joueur pour le *Hall of Fame*
    * Choisir le niveau de jeu souhaité

* Dans le jeu:
    * Abandonner la partie et retourner dans la salle de tutoriel
    * Mettre en évidence une carte mémoire

### Hall of Fame

Dans le jeu, dans la salle tutoriel, la présence d'un "Hall of Fame" 3D représentant les 10 meilleurs scores. La mécanique des scores est déjà en place. Seul l'affichage, la saisie du nom du joueur (ou pseudo) doit être réalisé.

## Oculus Store (5w)

Afin d'être disponible dans le *store Oculus*, l'application doit répondre aux critères techniques suivants :

* Optimisation graphique
    * Réduction du nombre de *mesh* de certains *gameobjects*.
    * Travail de l'éclairage pour avoir un rendu pré-calculé au maximum.
* Allègement du *package* (trop gros)
    * Certains éléments ne sont pas utilisés et pourtant sont dans le programme final.
* Dédoublage de l'application lié au mode multijoueur précédemment cité.

## Éducation (8w)

L'objectif est de renforcer l'acquisition des connaissances au travers du jeu. L'équilibre entre le ludique et l'apprentissage est difficile. C'est pourquoi nous proposons des piste de développement du jeu, qui seront à valider en test.

Les développements seront les suivants :

### Questions

* Ajouter une question pour la validation de l'appairage. Cette question pourrait apparaître uniquement dans la vue salle. Le joueur n'aura lui que les boutons sans texte. Ce sera aux personnes dans la salle de lui dicter leur réponse.

* Lors de la contamination par un virus, une question sera posée au joueur et à la **vue salle** pour avoir une chance d'éviter la destruction de la carte par le virus.

Les questions seront des choix multiples de 2 à 4 réponses possibles. Une phrase d'explication en cas d'erreur et en cas de succès sera jouée.

Un sablier visuel indiquera le délai pour répondre.

### Différents niveaux de jeu

Différents niveaux de jeu sont possible et influent sur la vitesse de propagation des virus, du nombre de paires à faire.

Il s'agit de coder dans la salle de tutoriel le choix du niveau de jeu, les UI et les objets adaptés. 3 niveaux seront proposés.


# Mode de travail souhaité

Le mode de travail sera en interaction forte avec les ingénieurs de l'équipe technique et l'équipe éditoriale pour la validation des contenus.

## Équipe Inria

L'équipe technique est la suivante :

* Bertrand.Wallrich@inria.fr (Chef de projet)
* Laurent.Pierron@inria.fr

L'équipe éditoriale est la suivante :

* Veronique.poirel@inria.fr
* Justine.Galet@inria.fr

## Développements

Les développements se feront sur la forge https://gitlab.inria.fr selon les outils *gitlab* et *git*. Le principe de *commit* se fera selon des flux *gitflow-style* (gestion de branches et de *merges*).

Compte tenu de la nature du projet, il n'y a pas de tests automatiques ni même d'intégration continue (CI).

La méthode sera de type agile (sans pour autant envisager de *sprints*). Le chef de projet Inria fera office de *scrum master*.

Le *backlog* sera géré dans l'outil gitlab à l'aide d'*issues* et de *tags*.

## Directives

### Gameobjects

Les *gamobjects* à créer pour le jeu ne font pas l'object de ce CCTP. Des designs simples et fonctionnels seront suffisant. Un designer refera des objets 3D avec un meilleur rendu.

### Animations

Les animations des *gameobjects* seront à réaliser avec *animator controller* et diverses animations (pas de mode *legacy*).

### prefab

L'utilisation de *prefabs* dans Unity sera privilégié. Chaque *prefab* embarquera son *animator controller*, ses animations, ses scripts, ses *materials*.
