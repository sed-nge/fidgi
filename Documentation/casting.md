# Principe de la diffusion en miroir (cast)

La diffusion en miroir des `Oculus Quest 2` se fait sur des moniteurs en passant à travers des `Google Chromecast` connectés à un réseau local commun avec les `Oculus Quest 2`.

# Configurer le réseau Chromecast

Les Google Chromecast sont configurés pour fonctionner avec le réseau local du routeur WiFi tp-link.

Les identifiants réseau tp-link :
- `SSID: TP-Link_C6C4`
- `Password: 93801836`

Au démarrage des Chromecast, ils doivent pouvoir se connecter sur Internet, il faut donc configurer le routeur WiFi tp-link pour se connecter à Internet.

Une solution consiste à partager une connexion WiFi soit sur un réseau 4G, soit sur un réseau WiFI, c'est le mode WISP (Operation Mode) du routeur, c'est celui qui est configuré par défaut.

Pour connecter le routeur a un nouveau réseau, il faut accéder à la configuration du routeur, en se connectant sur ce dernier via un navigateur Web à l'adresse 192.168.0.1.

La connexion au routeur depuis un PC peut se faire via un câble ou Ethernet, ou alors via WiFi avec un smartphone ou une tablette en plaçant le smartphone sur le réseau WiFi (TP-Link_C6C4) du routeur.

La page Web de configuration du routeur demande un mot de passe, ce dernier est écrit au dos du routeur, c'est : **admin938**.

![Password first screen](./casting/password.png)

Une fois la page de configuration affichée, choisir dans la liste des fonctions dans la colonne de gauche : `Wireless -> Basic Settings`

![Wireless settings page](./casting/wireless_settings.png)

Une fois la page de configuration de la connexion WiFi affichée, changez le nom du SSID du réseau, vous pouvez cliquer sur le bouton "scan" pour trouver le nom du réseau, puis changez le mot de passe et éventuellement les autres paramètres de la connexion WiFi.

![SSID scan page](./casting/SSID_scan.png)

Redémarrez le boîtier tp-link, après un moment le voyant de connexion Internet (voyant du milieu sur le routeur) doit devenir vert. Si le voyant reste orange vérifiez vos paramètres de connexion.

Quand les voyants de connexion Internet et de WiFi sont verts, les Chromecast devraient se connecter sur le routeur et sur Internet. On peut vérifier que l'écran de veille du Chromecast affiche le nom du Chromecast (INSIDE007 ou INSIDE009) et la météo courante.
