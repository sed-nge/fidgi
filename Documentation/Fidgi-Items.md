# The Icons, GameObjects for Fidgi Quiz Items
## Icons, GameObjects List per Quiz Pair
|No.|Object|Names|Year|Icon (Shown after Activation of GPU)|GameObject| Audio Video |
|---|---|---                                                            |---         |---           |---                           |---  |
|01|Pair|[Al-Khwârizmî](./Text_Audio_v1/Text.md#AlKhwarizmi)          | 820 | n/a | n/a | n/a |
|-|Character| AlKhwarizmi | 780-850 | [Face](https://fr.wikipedia.org/wiki/Al-Khwârizmî#/media/Fichier:1983_CPA_5426.jpg) |  | Persian, (Current part of Uzbekistan and Turkmenistan)... |
|-|Invention| AlKhwarizmi | 820 | f(x) = Sin x | 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 (in a sphere) | Clarified modern day mathematics in documentation with a counting system that starts with zero. The concept of zero was not in the roman numbers; I II, III, IV, V... etc. |
|-|Combined| AlKhwarizmi | 820 | [Flowchart of algorithm](https://en.wikipedia.org/wiki/Algorithm) | Complicated equation entering a box, with simple number coming out | n/a |
|02|Pair|[George Boole](./Text_Audio_v1/Text.md#Boole)                | 1847 | n/a | n/a | n/a |
|-|Character| Boole | 1815-1864 | [Face](https://en.wikipedia.org/wiki/George_Boole#/media/File:George_Boole_color.jpg) | A shoe ("Church's" style classical shoe) | Son of a shoemaker (which today would be considered upper middle class) who enjoyed amateur science and experimentation, teacher from 16, self-taught after primary school. (& or not), creator of an algebria with only 0 and 1 called "algebre de boole"... |
|-|Invention| Boole | 1847 | = (True / False) | Book with "The Mathematical Analysis of Logic" as title (3D) | the logical with variables, semantic, and algebra with booleans... |
|-|Combined| Boole | 1847 | = (True / False) | NOR diagram  (3D) | n/a |
|03|Pair|[Ada Lovelace](./Text_Audio_v1/Text.md#Lovelace)             | 1843 | n/a | n/a | n/a |
|-|Character| Lovelace | 1815-1852 | [Face](https://en.wikipedia.org/wiki/Ada_Lovelace#/media/File:Ada_Lovelace_portrait.jpg) | 3D version of the icon | the world`s first coder, died at 36 (same age as her father)... |
|-|Invention| Lovelace | 1843 | Numbers representing letters, musical note and objects | The concept of a computer (A computer that isn’t finished) | Vision of mathematical machine that could be transformed into a modern day computer... |
|-|Combined| Lovelace | 1843 | “Hello world”, half code, half output | The concept of a computer (A computer that isn’t finished) with the “Hello World” code not inserted | n/a |
|04|Pair|[John Von Neumann](./Text_Audio_v1/Text.md#Neumann)          | 1949 | n/a | n/a | n/a |
|-|Character| Neumann | 1903-1957 | Game Theory | MANIAC computer | Mathematical genius..., son of Banker who was a Doctor of Law, mother who was from a rabbinic family. Said to have been able to divide two eight-digit numbers in his head at the age of six.|
|-|Invention| Neumann | 1949 | A computer (2D image of MANIAC I) | MANIAC computer |  |
|-|Combined| Neumann | 1949 |  |  | n/a |
|05|Pair|[Grace Hopper](./Text_Audio_v1/Text.md#Hopper)               | 1959 | n/a | n/a | n/a |
|-|Character| Hopper | 1906-1992 | [Face in military uniform, but no details of the face, just uniform, glasses and outline of the face](https://fr.wikipedia.org/wiki/Grace_Hopper#/media/Fichier:Commodore_Grace_M._Hopper,_USN_(covered).jpg) | 3D version of the icon |  |
|-|Invention| Hopper | 1959 |  |  | Programming as a Human language and not as a Computer language. |
|-|Combined| Hopper | 1959 |  |  | n/a |
|06|Pair|[Alan Turing](./Text_Audio_v1/Text.md#Turing)                | 1939 | n/a | n/a | n/a |
|-|Character| Turing | 1912-1954 | [Face](https://en.wikipedia.org/wiki/Alan_Turing#/media/File:Alan_Turing_Aged_16.jpg) | Logic graph | name, dates, running, enigma, bletchey park, homosexuality, walt Disney, suicide, turing test... |
|-|Invention| Turing | 1939 | A computer | The bombe | computer formalization (turing machine), turing tests |
|-|Combined| Turing | 1939 | A computer | Turing Test | n/a |
|07|Pair|[Claude Shannon](./Text_Audio_v1/Text.md#Shannon)            | 1948 | n/a | n/a | n/a |
|-|Character| Shannon | 1916-2001 | Knight (chess piece) | Knight (chess piece: 3D) | Son of a business man and language teacher, distant cousin of Thomas Edison, Needed to take German exam at uni twice, Computer circuits (Digital logic “& or not” to build electronic circuits)... |
|-|Invention| Shannon | 1948 | IEEE |  |  |
|-|Combined| Shannon | 1948 |  |  | n/a |
|08|Pair|[Tim Berners-Lee](./Text_Audio_v1/Text.md#Berners-Lee)       | 1990 | n/a | n/a | n/a |
|-|Character| Lee | 1955- | “Shared Task” | http (3D) | Son of computer scientists that worked on the one of the first commercially built general purpose computer. Does not have a M.S. or PhD. Made a TV into a computer... |
|-|Invention| Lee | 1990 | https://www.foo.html | </> (3D) |  |
|-|Combined| Lee | 1990 | W3C | </> (3D) | n/a |
|09|Pair|[Le transistor](./Text_Audio_v1/Text.md#le_transistor)       | 1947 | n/a | n/a | n/a |
|-|Character| Transistor |  |  | [4th Bell Labs](https://www.beatriceco.com/bti/porticus/bell/images/hires_1939_logo.gif) logo | All three are American with parents in education, 1957 Nobel prize of physics (all three)... |
|-|Invention| Transistor | 1947 | Transistor image | Transistor (3D) |  |
|-|Combined| Transistor | 1947 | Transistor with a Bell labs logo | Transistor with a Bell labs logo (3D) | n/a |
|10|Pair|[Le circuit intégré](./Text_Audio_v1/Text.md#circuit_integre)| 1958 | n/a | n/a | n/a |
|-|Character| IC |  | Texas Instrument logo and Fairchild Semiconductor/Intel logo | 3D image of two men dressed in a tie and suit (faces are not in detail) | Two men who didn’t know each other, Texas Instruments employee and co-founder of Fairchild Semiconductor & Intel, Kilby also invented the handheld calculator, Noyce as with Gates an extremely strong will to win, at all costs... |
|-|Invention| IC | 1958 | Simple IC chip drawing | IC chip (3D) |  |
|-|Combined| IC | 1958 | Simple IC chip drawing | IC chip (3D) | n/a |
|11|Pair|[La souris](./Text_Audio_v1/Text.md#La_souris)               | 1968 | n/a | n/a | n/a |
|-|Character| Mouse | 1925-2013 |  |  | Make the tools easier to use, to help the smart people think of good solutions |
|-|Invention| Mouse | 1968 | I/O | Engelbart’s Prototype mouse  | Sometimes called the presentation of the century... |
|-|Combined| Mouse | 1968 | I/O | Modern gaming mouse (3D) | n/a |
|12|Pair|[Le réseau Ethernet](./Text_Audio_v1/Text.md#Ethernet)       | 1980 | n/a | n/a | n/a |
|-|Character| Ethernet | 1946- 1950- | Network connection image (connection between major cities) | Xerox 3D logo | The idea (Metcalfe) and the means (Boggs), not the son of a high achiever, 2 Bachelor of Science degrees (electrical engineering Industrial Management), M.S in Applied Science, Failed his first PhD dissertation but didn’t give up to finally get his PhD in Computer Science (applied Mathematics). |
|-|Invention| Ethernet | 1980 | IEEE 802.3 | IEEE 802.3 (3D) |  |
|-|Combined| Ethernet | 1980 | IEEE 802.3 | IEEE 802.3 (3D) | n/a |
|13|Pair|[Le réseau Internet](./Text_Audio_v1/Text.md#Internet)       | 1965 | n/a | n/a | n/a |
|-|Character| Internet |  | Seed planter | An apple seed (3D) | He saw the future. "Computing's Johnny Appleseed", the mind behind Ethernet, intercomputer communication etc. |
|-|Invention| Internet | 1965 | [Internet map](https://fr.wikipedia.org/wiki/Internet#/media/Fichier:Internet_map_1024.jpg) | Earth with internet map (3D sphere) |  |
|-|Combined| Internet | 1965 | Internet map | Earth with internet map (3D sphere) | n/a |
|14|Pair|[Microsoft Windows](./Text_Audio_v1/Text.md#MSWindows)       |1975-      | n/a | n/a | n/a |
|-|Character| Microsoft |---|[Glasses on a smiling](https://images.app.goo.gl/rvphhxAC1gYVX1m78) face (Take a photo of Bill Gates, keep the shape of the face and smile but remove all other features and make into a mask)| [Bill Gates’ Glasses on a smiling](https://images.app.goo.gl/rvphhxAC1gYVX1m78) face (simple mask-like face in 3D) | Son of a lawyer and financial company board member, White hacker in his teens, Genius understanding of programing, law and marketing, undermined but eventually feared by IBM staff, he had programmed the first DOS operating system... |
|-|Invention| Microsoft | 1975- |[86-DOS startup](https://en.wikipedia.org/wiki/86-DOS#/media/File:86-DOS_running_assembler_and_HEX2BIN_(screenshot).png) screen| Monitor with black background and green text (3D) | BASIC, IBM partnership for PC-DOS and MS-DOS for others, Windows 95 GUI as main interaction, Microsoft Office, X-Box, market share 87%... |
|-|Combined| Microsoft | 1975- |Current Windows Logo|3D Windows rotating Logo| n/a |
|15|Pair|[l'informatique nomade](./Text_Audio_v1/Text.md#informatique_nomade)| 1976 | n/a | n/a  | n/a |
|-|Character|Apple|---| Actual apple with a side bitten off like the logo| Motherboard & soldering iron | Garage making pocket money, understanding of non-geek needs matched with engineering talent, artists/creators are not programmers, difficult personalities, simplification for masses (Jumps of evolution), addition of “simplification”... |
|-|Invention|Apple| 1976 |[Le "Picasso" du Macintosh](https://fr.wikipedia.org/wiki/Macintosh#/media/Fichier:Macintosh_logo.svg) image   | Macintosh 128K with mouse and Keyboard | Computer company, creation of the first computer with an graphical interface for people, Education & DTP, made great by non-founder, Decline due to competition -> Near bankruptcy, simplified lineup, Re-simplification for to use for non-geek, eco-system, macOS 9% market share... |
|-|Combined|Apple| 1976 |Current Apple Logo| Mobility-Connectivity (Image of human walking with an Apple watch, iPhone, AirPods(?), and a MacBook in his bag) | n/a |
|16|Pair|[Open source et Linux](./Text_Audio_v1/Text.md#Linux)        | 1983 | n/a | n/a | n/a |
|-|Character| Linux | 1983- 1991- | [GNU Logo](https://images.app.goo.gl/hxPTea8U7NwwrgH78) | GNU Logo (3D) | 2 men who were great at modifying software, one saw the world shifting to proprietary software, while the other wanted a usable cheap kernel, Richard Stallman : define and make popular the concept of free software, Linus Torwald : build an opensource operating system. its name was derivated from his name... |
|-|Invention| Linux | 1983 | [Tux Logo](https://images.app.goo.gl/SvtGtju382Ye5yVq8) | Tux Logo (3D) | Variety of Linux distributions (Debian, Fedora, Ubuntu,” etc.), used on most servers and research computers, market share 2 %... |
|-|Combined| Linux | 1983 | GNU & Tux logo | GNU & Tux logo (3D) | n/a |
|Tuto01|Pair|Hedy Lamarr| 1971 | n/a | n/a | n/a |
|-|Character| Lamarr | 1914-2000 | [Face](https://fr.wikipedia.org/wiki/Hedy_Lamarr#/media/Fichier:Hedy_Lamarr_in_Let's_Live_a_Little_(1948).jpg) | US Patent 2,292,387 (Shadow&Glamour in the image) |  |
|-|Invention| Lamarr | 1971  | Secure remote communication | Remote control |  |
|-|Combined| Lamarr | 1971 | Wi-Fi logo, Bluetooth logo | Wireless communication | n/a |
|Tuto02|Pair|VideoConf| 1968 | n/a | n/a | n/a |
|-|Character| VideoConf | 1925- |  |  |  |
|-|Invention| VideoConf | 1968 | Image of Zoom/Teams/Skype meeting | [1968 AT&T Picturephone](https://images.app.goo.gl/RCDZuqz88jrZYq6s9) | Since the first demo in 1927 between Washington and New York... |
|-|Combined| VideoConf | 1968 | 2 laptop computers with the monitor being used for a Zoom meeting (sound image coming out of computer speakers too) | Modernaized version of 1968 AT&T Picturephone | n/a |
|Tuto03|Pair|[Margaret Hamilton](./Text_Audio_v1/Text.md#Hamilton)        | 1964 | n/a | n/a | n/a |
|-|Character| Hamilton | 1936- | Nasa logo | UML graph | name, dates, programmer, working for the Nasa... |
|-|Invention| Hamilton | 1964 | A rocket | UML graph | modelisation of process, formal systems, object oriented programmation, user interface... |
|-|Combined| Hamilton | 1964 | A sort of workflow | UML graph  | n/a |

## Reference Duration (Word count and Duration)
The duration of the TTS output is very similar to the Male actor reading the text, so as a quick duration verification method, I think using the TTS is a quick and easy way to check the duration of the charactor/invention explanation.  
TTS output command sample (from macOS terminal):  
say -v Audrey -f Hamilton.txt -o Hamilton  
<!--sox Hamilton.aiff Hamilton.wav  -->

|Name                                                           | Gender  | Voice      | Duration  | Voice      | Duration  | Word Count | 
|---                                                            |:-------:|---         |----------:|---         |----------:|         ---| 
|- [Al-Khwârizmî](./Text_Audio_v1/Text.md#AlKhwarizmi)          | &#9794; |TTS Audrey  |71s        |Male Actor  |---        |201 words   |
|- [George Boole](./Text_Audio_v1/Text.md#Boole)                | &#9794; |TTS Audrey  |66s        |Male Actor  |---        |301 words   |
|- [Ada Lovelace](./Text_Audio_v1/Text.md#Lovelace)             | &#9792; |TTS Audrey  |80s        |Male Actor  |---        |265 words   |
|- [John Von Neumann](./Text_Audio_v1/Text.md#Neumann)          | &#9794; |TTS Audrey  |63s        |Male Actor  |---        |219 words   |
|- [Grace Hopper](./Text_Audio_v1/Text.md#Hopper)               | &#9792; |TTS Audrey  |61s        |Male Actor  |---        |261 words   |
|- [Alan Turing](./Text_Audio_v1/Text.md#Turing)                | &#9794; |TTS Audrey  |122s       |Male Actor  |---        |322 words   |
|- [Margaret Hamilton](./Text_Audio_v1/Text.md#Hamilton)        | &#9792; |TTS Audrey  |100s       |Male Actor  |---        |267 words   |
|- [Claude Shannon](./Text_Audio_v1/Text.md#Shannon)            | &#9794; |TTS Audrey  |208s       |Male Actor  |---        |303 words   |
|- Linus Torvalds                                               | &#9794; |TTS Audrey  |---        |Male Actor  |---        |n/a         |
|- Richard Stallman                                             | &#9794; |TTS Audrey  |---        |Male Actor  |---        |n/a         |
|- [Tim Berners-Lee](./Text_Audio_v1/Text.md#Berners-Lee)       | &#9794; |TTS Audrey  |92s        |Male Actor  |---        |256 words   |
|- Steve Jobs                                                   | &#9794; |TTS Audrey  |---        |Male Actor  |---        |n/a         |
|- Stephen Wozniak                                              | &#9794; |TTS Audrey  |---        |Male Actor  |---        |n/a         |
|- Bill Gates                                                   | &#9794; |TTS Audrey  |---        |Male Actor  |---        |n/a         |
|- [Hedy Lamarr](./Text_Audio_v1/Text.md#Lamarr)                | &#9794; |TTS Audrey  |28s        |Male Actor  |---        |79 words    ||                                                               |         |            |           |            | 
|- [Le transistor](./Text_Audio_v1/Text.md#le_transistor)       |---      |TTS Audrey  |124s       |Male Actor  |133s       |324 words   | 
|- [Le circuit intégré](./Text_Audio_v1/Text.md#circuit_integre)|---      |TTS Audrey  |121s       |Male Actor  |134s        |329 words   | 
|- [La souris](./Text_Audio_v1/Text.md#La_souris)               |---      |TTS Audrey  |45s        |Male Actor  |47s        |115 words   | 
|- [Le réseau Ethernet](./Text_Audio_v1/Text.md#Ethernet)       |---      |TTS Audrey  |47s        |Male Actor  |49s        |128 words   | 
|- [Le réseau Internet](./Text_Audio_v1/Text.md#Internet)       |---      |TTS Audrey  |54s        |Male Actor  |56s        |152 words   | 
|- [Open source et Linux](./Text_Audio_v1/Text.md#Linux)        |---      |TTS Audrey  |122s       |Male Actor  |135s       |314 words   | 
|- [l'informatique nomade](./Text_Audio_v1/Text.md#informatique_nomade) |---|TTS Audrey|74s        |Male Actor  |78s        |205 words   | 
|- [Microsoft Windows](./Text_Audio_v1/Text.md#MSWindows)       |---      |TTS Audrey  |68s        |Male Actor  |69s        |171 words   | 

<!--
## Icons, GameObjects List per Quiz Pair
Details of video/audio explanations are listed separately.  

Apple:  
- Year: 1976  
- Character: Steve Jobs, Steve Wozniak  
  - Icon: Garage  
  - gameObject: Motherboard & soldering iron
- Invention: 
  - Icon: Le "Picasso" du Macintosh image
  - gameObject: Macintosh 128K with mouse and Keyboard
- Combined:
  - Icon: Current Apple Logo  
  - gameObject: Mobility-Connectivity (Image of human walking with an Apple watch, iPhone, AirPods(?), and a MacBook in his bag)

Microsoft Windows:  
- Year: 1975  
- Character: Bill Gates   
  - Icon: Glasses on a smiling face (Take a photo of Bill Gates, keep the shape of the face and smile but remove all other features and make into a mask)
  - gameObject: Bill Gates’ Glasses on a smiling face (simple mask-like face in 3D)
- Invention: 
  - Icon: 86-DOS startup screen
  - gameObject: Monitor with black background and green text (3D)
- Combined:
  - Icon: Current Windows Logo
  - gameObject: 3D Windows rotating Logo

Linux:  
- Year: 1983 -, 1991 -  
- Character: Richard Stallman & Linus Torvalds  
  - Icon: GNU Logo
  - gameObject: GNU Logo (3D)
- Invention: 
  - Icon: Tux Logo
  - gameObject: Tux Logo (3D)
- Combined:
  - Icon: GNU & Tux logo
  - gameObject: GNU & Tux logo (3D)

VideoConf:  
- Year: 1927  
- Character: Bell Labs 
  - Icon: 
  - gameObject: 
- Invention: 
  - Icon: Image of Zoom/Teams/Skype meeting
  - gameObject: 1968 AT&T Picturephone
- Combined:
  - Icon: 2 laptop computers with the monitor being used for a Zoom meeting (sound image coming out of computer speakers too)
  - gameObject: 1968 AT&T Picturephone
  
Mouse:  
- Year: 1968  
- Character: Douglas Engelbart  
  - Icon: 
  - gameObject: 
- Invention: 
  - Icon: I/O  
  - gameObject: Engelbart’s Prototype mouse  
- Combined:
  - Icon: I/O
  - gameObject: Modern gaming mouse (3D)

Transistor:  
- Year: 1947  
- Character: John Bardeen (USA), William Shockley (UK) et Walter Brattain (China)  
  - Icon: Bell Labs old bell logo
  - gameObject: 
- Invention: 
  - Icon: Transistor image
  - gameObject: Transistor (3D)
- Combined:
  - Icon: Transistor with a Bell labs logo
  - gameObject: Transistor with a Bell labs logo (3D)

IC:  
- Year: 1960s
- Character: Jack Kilby et Robert Noyce  
  - Icon: Texas Instrument logo and Fairchild Semiconductor/Intel logo
  - gameObject: Brain (to indicate how smart they were - 3D)
- Invention: 
  - Icon: Simple IC chip drawing
  - gameObject: IC chip (3D)
- Combined:
  - Icon: Simple IC chip drawing
  - gameObject: IC chip (3D)

Ethernet:  
- Year: 1970s  
- Character: Robert Metcalfe & David Boggs
  - Icon: Network connection image (connection between major cities)
  - gameObject: Xerox 3D logo
- Invention: 
  - Icon: IEEE 802.3
  - gameObject: IEEE 802.3 (3D)
- Combined:
  - Icon: IEEE 802.3
  - gameObject: IEEE 802.3 (3D)

Internet:  
- Year: 1970s  
- Character: Joseph Licklider  
  - Icon: Seed planter
  - gameObject: An apple seed (3D)
- Invention: 
  - Icon: Internet map
  - gameObject: Earth with internet map (3D sphere)
- Combined:
  - Icon: Internet map
  - gameObject: Earth with internet map (3D sphere)


Al Khwarizmi:  
- Year: 780 - 850
- Character:   
  - Icon: Face
  - gameObject: 
- Invention: 
  - Icon: f(x) = Sin x 
  - gameObject: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 (in a sphere)
- Combined:
  - Icon: Flowchart of algorithm (Refer to Wiki: https://en.wikipedia.org/wiki/Algorithm image)
  - gameObject: Complicated equation entering a box, with simple number coming out


George Boole:  
- Year: 1815 - 1864
- Character:   
  - Icon: Face
  - gameObject: A shoe ("Church's style shoe classical shoe)
- Invention: 
  - Icon: = (True / False)
  - gameObject: NOR diagram  (3D)
- Combined:
  - Icon: = (True / False)
  - gameObject: NOR diagram (3D)


Ada Lovelace:  
- Year: 1815 - 1852
- Character:   
  - Icon: Face
  - gameObject: "Hello World"
- Invention: 
  - Icon: Numbers representing letters, musical note and objects
  - gameObject: The concept of a computer (A computer that isn’t finished)
- Combined:
  - Icon: “Hello world”, half code, half output
  - gameObject: The concept of a computer (A computer that isn’t finished) with the “Hello World” code not inserted


John Von Neumann:  
- Year: 
- Character:   
  - Icon: 
  - gameObject: 
- Invention: 
  - Icon: 
  - gameObject: 
- Combined:
  - Icon: 
  - gameObject: 


Alan Turing:  
- Year: 1939 (The year of the bombe creation)
- Character:   
  - Icon: Face
  - gameObject: logic graph
- Invention: 
  - Icon: a computer
  - gameObject: the bombe
- Combined:
  - Icon: a computer
  - gameObject: Turing Test


Grace Hopper:  
- Year: 
- Character:   
  - Icon: 
  - gameObject: 
- Invention: 
  - Icon: 
  - gameObject: 
- Combined:
  - Icon: 
  - gameObject: 


Claude Shannon:  
- Year: 1948
- Character:   
  - Icon: Knight (chess piece)
  - gameObject: Knight (chess piece: 3D)
- Invention: 
  - Icon: IEEE
  - gameObject: 
- Combined:
  - Icon: 
  - gameObject: 

Margaret Hamilton:  
- Year: 1969 (apollo mission on moon)
- Character:   
  - Icon: Nasa logo
  - gameObject: UML graph
- Invention: 
  - Icon: A rocket
  - gameObject: UML graph
- Combined:
  - Icon: A sort of workflow
  - gameObject: UML graph

Tim Berners-Lee:  
- Year: 1955 -
- Character:   
  - Icon: “Shared Task”
  - gameObject: http (3D)
- Invention: 
  - Icon: https://www.html
  - gameObject: </> (3D)
- Combined:
  - Icon: W3C
  - gameObject: </> (3D)

## Video/Slideshow Contents List per Quiz Pair

Apple:  
- Character:   
  - Garage making pocket money, understanding of non-geek needs matched with engineering talent, artists/creators are not programmers, difficult personalities, simplification for masses (Jumps of evolution), addition of “simplification”  
- Invention: 
  - Computer company, first mass accepted GUI, Education & DTP, made great by non-founder, Decline due to competition -> Near bankruptcy, simplified lineup, Re-simplification for to use for non-geek, eco-system, macOS 9% market share
- Combined:
  - None

Microsoft Windows:  
- Character:   
  - Son of a lawyer and financial company board member, White hacker in his teens, Genius understanding of programing, law and marketing, undermined but eventually feared by IBM staff,
- Invention: 
  - BASIC, IBM partnership for PC-DOS and MS-DOS for others, Windows 95 GUI as main interaction, Microsoft Office, X-Box, market share 87%
- Combined:
  - None


Linux:  
- Character:   
  - 2 men who were great at modifying software, one saw the world shifting to proprietary software, while the other wanted a usable cheap kernel.
- Invention: 
  - variety of Linux distributions (Debian, Fedora, Ubuntu,” etc.), used on most servers and research computers, market share 2 %
- Combined:
  - None


VideoConf:  
- Character:   
  - Content of the video or slideshow:  
- Invention: 
  - since the first demo in 1927 between Washington and New York….
- Combined:
  - None


Mouse:  
- Character:   
  - Sometimes called the presentation of the century.
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None


Transistor:  
- Character:   
  - All three are American with parents in education, 1957 Nobel prize of physics (all three)
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None

IC:  
- Character:   
  - two men who didn’t know each other, Texas Instruments employee and co-founder of Fairchild Semiconductor & Intel, Kilby also invented the handheld calculator, Noyce as with Gates an extremely strong will to win, at all costs
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None


Ethernet:  
- Character:   
  - the idea (Metcalfe) and the means (Boggs), not the son of a high achiever, 2 Bachelor of Science degrees (electrical engineering Industrial Management), M.S in Applied Science, Failed his first PhD dissertation but didn’t give up to finally get his PhD in Computer Science (applied Mathematics).
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None

Internet:  
- Character:   
  - He saw the future. "Computing's Johnny Appleseed", the mind behind Ethernet, intercomputer communication etc.
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None


Al Khwarizmi:  
- Character:   
  - Persian, (Current part of Uzbekistan and Turkmenistan)...
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None

George Boole:  
- Character:   
  - Son of a shoemaker (which today would be considered upper middle class) who enjoyed amateur science and experimentation, teacher from 16, self-taught after primary school. (& or not)...
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None


Ada Lovelace:  
- Character:   
  - the world`s first coder, died at 36 (same age as her father)
- Invention: 
  - Vision of mathematical machine that could be transformed into a modern day computer...
- Combined:
  - None


John Von Neumann:  
- Character:   
  - Mathematical genius...
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None

Alan Turing:  
- Character:   
  - name, dates, running, enigma, bletchey park, homosexuality, walt Disney, suicide, turing test...
- Invention: 
  - computer formalization (turing machine), turing tests
- Combined:
  - None

Grace Hopper:  
- Character:   
  - Content of the video or slideshow:  
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None

Claude Shannon:  
- Character:   
  - Son of a business man and language teacher, distant cousin of Thomas Edison, Needed to take German exam at uni twice, Computer circuits (Digital logic “& or not” to build electronic circuits)...
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None

Margaret Hamilton:  
- Character: Margaret Hamilton  
  - name, dates, programmer, working for the Nasa...
- Invention: 
  - modelisation of process, formal systems, object oriented programmation, user interface...
- Combined:
  - None

Tim Berners-Lee:  
- Character:   
  - Son of computer scientists that worked on the one of the first commercially built general purpose computer. Does not have a M.S. or PhD. Made a TV into a computer...
- Invention: 
  - Content of the video or slideshow: 
- Combined:
  - None
-->
