# The current system
The objective of the current system the education of the general public in the history of computer science. To be specific, to indicate that computer science is not a new domain, but one that has its roots back in ancient Greece.  
Created with a **"Teacher (Information Giver) -- Student (Information Follower)"** style of presentation, the visitor (here on user) of the Virtual World would tour three levels of the virtual world as if in an physical museum. The user would enter a virtual room where nothing moved and listen to the achievements of the prominent figures of history.

## The prominent figures and innovations used in current system (&#9792; : &#9794; = 4 : 15)
The provided links will jump to the explanation text of each figure or item.
Note @Akira &larr; identify the categories and years of the below.

|Name                                                           | Gender  | Year       | Source       | Group                        | Duration  | Word Count | 
|---                                                            |:-------:|---         |---           |---                           |----------:|         ---| 
|- [Euclide](./Text_Audio_v1/Text.md#Euclide)                   | &#9794; |8eBC - 6eAD |Unknown       |                              |---        |---         |
|- [Al-Khwârizmî](./Text_Audio_v1/Text.md#AlKhwarizmi)          | &#9794; | 780 -  850 |7 Famillies   | Alogorithmes & programmation |---        |201 words   |
|- [George Boole](./Text_Audio_v1/Text.md#Boole)                | &#9794; |1815 - 1864 |7 Famillies   | Mathematiques & informatique |---        |301 words   |
|- [Ada Lovelace](./Text_Audio_v1/Text.md#Lovelace)             | &#9792; |1815 - 1852 |7 Famillies   | Alogorithmes & programmation |---        |265 words   |
|- [Joseph-Marie Jacquard](./Text_Audio_v1/Text.md#Jacquard)    | &#9794; |1752 - 1834 |Unknown       |---                           |---        |---         |
|- [John Von Neumann](./Text_Audio_v1/Text.md#Neumann)          | &#9794; |1903 - 1957 |7 Famillies   | Machines & Composants        |---        |219 words   |
|- [Grace Hopper](./Text_Audio_v1/Text.md#Hopper)               | &#9792; |1906 - 1992 |7 Famillies   | Alogorithmes & programmation |---        |261 words   |
|- [Alan Turing](./Text_Audio_v1/Text.md#Turing)                | &#9794; |1912 - 1954 |Unknown       |---                           |---        |322 words   |
|- [Margaret Hamilton](./Text_Audio_v1/Text.md#Hamilton)        | &#9792; |1936 -      |Unknown       |---                           |---        |---         |
|- [Frances Allen](./Text_Audio_v1/Text.md#Allen)               | &#9792; |1932 -      |Unknown       |---                           |---        |---         |
|- [Claude Shannon](./Text_Audio_v1/Text.md#Shannon)            | &#9794; |1916 - 2001 |7 Famillies   | Systemes & reseaux           |---        |303 words   |
|- [Donald Knuth](./Text_Audio_v1/Text.md#Knuth)                | &#9794; |1938 -      |Unknown       |---                           |---        |---         |
|- [Federico Faggin](./Text_Audio_v1/Text.md#Faggin)            | &#9794; |1941 -      |Unknown       |---                           |---        |---         |
|- Linus Torvalds                                               | &#9794; |1969 -      |Unknown       | Systemes & reseaux           |---        |n/a         |
|- Richard Stallman                                             | &#9794; |1953 -      |Unknown       | Systemes & reseaux           |---        |n/a      |
|- [Tim Berners-Lee](./Text_Audio_v1/Text.md#Berners-Lee)       | &#9794; |1955 -      |7 Famillies   | Systemes & reseaux           |---        |256 words   |
|- Steve Jobs                                                   | &#9794; |1955 - 2011 |Unknown       | Systemes & reseaux           |---        |n/a         |
|- Stephen Wozniak                                              | &#9794; |1950 -      |Unknown       | Systemes & reseaux           |---        |n/a         |
|- Bill Gates                                                   | &#9794; |1955 -      |Unknown       | Systemes & reseaux           |---        |n/a         |
|                                                               |         |            |              |                              |           |            | 
|- [Garage Intro audio file](./Text_Audio_v1/Text.md#garage)    |---      |            |Unknown       |                              |18s        |--- words   | 
|- [Le transistor](./Text_Audio_v1/Text.md#le_transistor)       |---      |1947        |Unknown       | Machines & Composants        |134s       |324 words   | 
|- [Le circuit intégré](./Text_Audio_v1/Text.md#circuit_integre)|---      |1960s       |Unknown       | Machines & Composants        |86s        |329 words   | 
|- [La souris](./Text_Audio_v1/Text.md#La_souris)               |---      |1968        |Unknown       | Interaction Homme-Machine    |47s        |115 words   | 
|- [Le réseau Ethernet](./Text_Audio_v1/Text.md#Ethernet)       |---      |1970s       |Unknown       | Systemes & reseaux           |49s        |128 words   | 
|- [Le réseau Internet](./Text_Audio_v1/Text.md#Internet)       |---      |1970s       |Unknown       | Systemes & reseaux           |56s        |152 words   | 
|- [Open source et Linux](./Text_Audio_v1/Text.md#Linux)        |---      |1983- & 1991-|Unknown       | Systemes & reseaux           |136s       |314 words   | 
|- [l'informatique nomade](./Text_Audio_v1/Text.md#informatique_nomade) |---|1976 -    |Unknown       | Systemes & reseaux           |78s        |205 words   | 
|- [Microsoft Windows](./Text_Audio_v1/Text.md#MSWindows)       |---      |1976 -      |Unknown       | Systemes & reseaux           |70s        |171 words   | 
|                                                               |         |            |              |                              |           |            |




# Something to think about
I want something, so I will make it. &larr; How can we make people who experience this VR to think like this?  
- A 14 year old kid with 1 years experience in programming fixed two iOS bugs.
- A 85 year old Japanese woman Masako Wakamiya only started programming after retiring from a job at a bank when she was over 60. Why? Because she wanted games tht she would enjoy as a elderly citizen and not games made for the youger generation.  

How can this game inspire the young, but also the parents to think that this is not something too difficult, or even worse, something that only other people can do?

## What was the problem that each item was solving?  
- Why did these people want to create what they created?  
- What was the gap?
- What was the troubles, difficulties they saw that needed fixing?
- Inspiration comes from *seeing a gap* and thinking that one has the *ability or idea* to fill that gap?

# The good point of the current system
### User interacts with Virtual Reality system
- Interest is still high
- Not all parents are lucky enough to experience VR or other high-tech items, but that does not mean that they don't want to, or that they don't want their children to interact with these items.  
- Not all children are lucky enough to interact with high-tech games.
For the parents it gives them a little satisfaction that they provided their children with an experience that they didn't or couldn't have and test their childrens interest towards VR and computers.
- For children, they are still at a young age where curiosity is high and interest are nurtured.
- VR has more flexibility than a physical presentation, so it is possible to create a virtual museum.  



# What to change with this update
## Education
- Change the education style from **"Information Giver -- Information Follower"** to ***"Opportunity to think"***  
- There is a big hole between 850 to 1815.  
- **Audrey** (1952) first computer system to recognise human words (ASR)
- **Picturephone** (1964) first commercially available telephone with video & audio. It was only over 30 years later in 1999 that a commercially available mobile phone would be able to simultaneously send audio and video signals.  
- **Minitel**  


## Women in computer science (&#9792; : &#9794; = 4 : 15 is not a good balance)  
 - Add more important women to the game (even if their achievements are not as famous as the other men on the list)  

Dr. Laurence Devillers (CNRS - University Paris-Sorbonne) asked me back in 2015 at INTERSPEECH (Dresden) what can be done to inspire girls to enter computer science. The current VR system has a balance of "&#9792; : &#9794; = 4 : 15" which gives too strong an impression that this is a man's world.  
How and were can the balance be made more equal?  
- Jacqueline Vaissière (CNRS - Université Paris 3) is a famous French female scientist, are there more like her?
- Véronique Auberge (CNRS/INSHS au LIG) is another famous French female scientist.
- Hedy Lamarr = Frequency-hopping spread spectrum (https://en.wikipedia.org/wiki/Hedy_Lamarr)

### Can anyone be added from "Les 7 familles de l'informatique"  

- Gender balance of "Les 7 familles de l'informatique"  
&#9792; : &#9794; = 15 : 27+1

|Group or Name           | Gender  | Usage   | Group or Name          | Gender  | Usage   |
|---                     |:-------:|:-------:|---                     |:-------:|:-------:| 
|**Alogorithmes & programmation** | &#9792; : &#9794; = 3 : 3 | 3 ppl used | **Mathematiques & informatique** | &#9792; : &#9794; = 3 : 3 | 1 ppl Used |  
|- Al Khwarizmi          | &#9794; | Used    | - Hypatie D'Alexandrie | &#9792; |         |  
|- Ada Lovelace          | &#9792; | Used    | - George Boole         | &#9794; | Used    |  
|- Grace Hopper          | &#9792; | Used    | - Alonzo Church        | &#9794; |         |  
|- Dorothy Vaughan       | &#9792; |         | - Jacques-Louis Lions  | &#9794; |         |  
|- Gilles Kahn           | &#9794; |         | - Ingrid Daubechies    | &#9792; |         |  
|- Gerard Berry          | &#9794; |         | - Jocelyne Troccaz     | &#9792; |         |  
|                        |         |         |                        |         |         |  
|**Systemes & reseaux**  | &#9792; : &#9794; = 2 : 4| 2 ppl Used | **Machines & Composants**  | &#9792; : &#9794; = 1 : 5| 1 ppl Used |
|- Alexander Graham Bell | &#9794; |         | - Charles Babbage     | &#9794; |         |  
|- Claude Shannon        | &#9794; | Used    | - John von Neumann    | &#9794; | Used    |  
|- Vinton Cerf           | &#9794; |         | - Hedy Lamarr         | &#9792; |         |  
|- Tim Berners-Lee       | &#9794; | Used    | - Seymour Cray        | &#9794; |         |  
|- Pascale Vicat-Blanc   | &#9792; |         | - Gordon Moore        | &#9794; |         |  
|- Anne-Marie Kermarrec  | &#9792; |         | - Hiroshi Ishiguro    | &#9794; |         |  
|                        |         |         |                       |         |         |  
|**Inteligence Artificielle**  | &#9792; : &#9794; = 2 : 4 | 0 ppl Used | **Interaction Homme-Machine**  | &#9792; : &#9794; = 2 : 4| 0 ppl Used |
|- Herbert Simon         | &#9794; |         | - Doug Engelbart      | &#9794; |         |  
|- Marvin Minsky         | &#9794; |         | - Ted Nelson          | &#9794; |         |  
|- Geoffrey Hinton       | &#9794; |         | - Alan Kay            | &#9794; |         |  
|- Rose Dieng-Kuntz      | &#9792; |         | - Joelle Coutaz       | &#9792; |         |  
|- Yann Lecun            | &#9794; |         | - Jean-Marie Hullot   | &#9794; |         |  
|- Cordelia Schmid       | &#9792; |         | - Marie-Paule Cani    | &#9792; |         |  
|                        |         |         |                       |         |         |  
|**Securite & confidentialite** | &#9792; : &#9794; = 2 : 4 | 0 ppl Used |  |  |  |   
|- Jules Cesar           | &#9794; |         |                       |         |         |  
|- Al-Kindi              | &#9794; |         |                       |         |         |  
|- Deffie Hellman        | &#9794; |         |                       |         |         |  
|- Rivest, Shamir, Adleman - RSA | &#9794;|  |                       |         |         |  
|- Shafi Goldwasser      | &#9792; |         |                       |         |         |  
|- Cynthia Dwork         | &#9792; |         |                       |         |         |  
|                        |         |         |                       |         |         |  

- Alan Turning (&#9794;) Cat.: **???**  : Used  









## User initiated movement should only be related to thinking
- Don't let the user be distracted or make the user think where to go.
- Make the action objective obvious; Don't let the user think "Why am I here?" or "What am I meant to do now?"  

## Items to add  
- Should "Teleconferencing" software be added to give the kids something to relate to?

# What not to do
### Don't use tables near the user
- User will try to rest, or lean, or hold the table
### Don't use an open window near the user
- User will try to lean out of the window and might fall
### Don't let the user to move freely
- We don't want the user questioning what to do.
- Unless the reason the user has very clear reason to move, automate all movements


# The VR World
## Entrance to the VR World: The door in the desert concept
### Positive points
New doors can be added to enter different worlds.  
- One door for the "historical timeline" game/quizz  
- One door for a vist to INRIA researchers & developers  
    - This provides a location to place the interviews with INRIA staff  

### Negative points
Too many doors will take away the simplicity of the environment.
- Maybe a limit of 3 or 5 door is needed for future updates to Fidgi  

### What happens when the user places the Oculus Rift over their eyes and starts the VR game/quiz?
1. The users enters a world of desert dunes (sand coloured light gold giving a warm positive feeling), with a clear blue sky. Maybe about 10 or 20 meters in front of the user, there is a door (colour= INRIA Red), just the frame and the door.
    - The meaning of this is "zero/null": every idea starts from a clean blank sheet that looks and feels empty, but every desert has life. It's just not easy to see the life that is in the desert, just like it is difficult to know the history behind new ideas and inventions.  

2. The user automatically moves towards the door while the voice-over explains to the user the concept of the game and what they are to do on the other side of the door.  
    - This should prepare the user and give the user an idea of what to expect. (Not too detailed, but just enough: which is going to be difficult to decide.)  
    - An example could be "Every idea first started on a clean sheet of paper, just like this desert around you, a world of nothing. But in nothing, there is always something. New technology, such as computer science also has a world that we can't easily see. Behind that door you will have a glimpse of the history of computer science ..." (sorry, yes this is already very long, but yet still doesn't explain the game, so not detailed enough. The actual optimal length will have to be decided later)  

3. Once in the door, the user is automatically transported to the location where they are to play the game. 

## The quizz/puzzle room: Inside a notebook computer
### Positive points
This VR world is an introduction to the world of computer science. 

### Negative points
