# <p align="center"> `CODING RULES - FIDGI` </p>

This is not up for debate.

## <p align="center">⚠️ ENGLISH ONLY - CODE / COMMITS / DOC ⚠️</p>

## PascalCase for classes, functions and singletons, camelCase for variables

## Explicit names for variables and functions

##  Public what should be in public

## Use Requires
	[RequireComponent(typeof(...))]
	[AddComponentMenu("...")]
	
## Try as much as possible not to put code in Update()
..Prefer using coroutines / events

## Scripts should be discrete meaningful actions
1 script = 1 action
They should do one thing.  If that thing needs other things to be done, then they should create other,
This keeps scripts small, useful, and easy to read.

## Reference other gameObjects
Through Tags / variales...

## Reference as much as possible in Start()
gameObjectifs / components

## Always test objects != null when 

## No "Magic Numbers"
prefer using variables in functions that require numbers