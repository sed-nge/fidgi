# Concerning Oculus
# Required computer specs
|Component|Recommmended Specs|Minimum Specs|
|---|---|---|
|CPU|Intel i5-4590 / AMD Ryzen 5 1500X or greater|	Intel i3-6100 / AMD Ryzen 3 1200, FX4350 or greater|
|GPU|NVIDIA GTX 1060 / AMD Radeon RX 480 or greater|NVIDIA GTX 1050 Ti / AMD Radeon RX 470 or greater|
|Alternative GPU|NVIDIA GTX 970 / AMD Radeon R9 290 or greater|NVIDIA GTX 960 4GB / AMD Radeon R9 290 or greater|
|Memory|8GB+ RAM|8GB+ RAM|
|OS|Windows 10|Windows 10|
|USB Ports|1x USB 3.0 ports|1x USB 3.0 ports|
|Video Output|Compatible DisplayPort video output|Compatible miniDisplayPort video output|

# Unity tools
## Oculus Integration
Official Oculus asset for Unity that provides Oculus Unity SDK integrations.
- Does not work with VR Simulator.  
## VR simulator
`VR Simulator` made by Thrash Panda.  
Details can be found [here](https://assetstore.unity.com/packages/tools/input-management/vr-simulator-167062).
- Does not work with Oculus Unity SDK tools.  
This simulator works with the `Unity XR Interaction Toolkit`.  
Details can be found [here](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@1.0/manual/index.html).

### Unity settings to use VR simulator
To use the `VR Simulator` asset change the following settings.  
`Edit` -> `Project Settings...` -> `XR Plug-in Management` -> Select `Initialize XR on Startup`, `VR Simulator`.  

Once selected, right-click in the `Hierarchy` window and select `XR` -> `VR Simulator`.

