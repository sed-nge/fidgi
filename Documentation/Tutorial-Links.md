# Usefull Tutorials

## Add useful Tutorials here


### YouTube videos


| description | link |
|--|--|
| first one, unity interface | https://www.youtube.com/watch?v=OBv3EU0DK4c |
| collider & rigid body | https://www.youtube.com/watch?v=OBv3EU0DK4c |
| material & textures | https://www.youtube.com/watch?v=2oV-YN2lQGg |
| animations | https://www.youtube.com/watch?v=zGyj9d1L9so |
| user interface | https://www.youtube.com/watch?v=MAEnKfNLPVc&list=PLDQKHJxRCgUIpRYCPdQgmb0qFlFGJSjOl&index=10 |
| nav mesh | https://www.youtube.com/watch?v=l6_bky6JoOM&list=PLDQKHJxRCgUIpRYCPdQgmb0qFlFGJSjOl&index=12 |
| using VR | https://www.youtube.com/watch?v=gGYtahQjmWQ |
| Blender to Unity | https://www.youtube.com/watch?v=NjflKgMepQs |
| Blender Floppy Disk | https://www.youtube.com/watch?v=E88rboKK08w |  


### Webpages on Blender and Unity


| description | link |
|--|--|
| Blender export notes | https://gamedevacademy.org/how-to-import-blender-models-into-unity-your-one-stop-guide/ |



### Simple Bug fixes

Problem with "Standard assets" and Unity 2020.1.16f1
One error will display after creating a project with the Standard Assets included as a package.  

    Assets/Standard Assets/Unity/SimpleActivatorMenu.cs*11,16(): error CS0619: 'GUIText has been removed. Use UI.Text instead'
Fix:  
Open 'Assets/Standard Assets/Unity/SimpleActivatorMenu.cs' and added 'using UnitEngine.UI;' to the list of 'using' items and replace 'public GUIText camSwitchButton;' with 'public Text camSwitchButton;' (change 'GUIText' to 'Text')

### Notes
- Unity will not import materials (colours/textures) made in other 3D software, so they need to be imported seperately to the item (https://blender.stackexchange.com/questions/106128/exporting-texture-files-as-png-or-jpg).
