# Making Voices

Talking messages are made with the TTS Apple program: `say`, this program has three french voices: Audrey, Aurelie, Thomas.

The TTS program is called by a Python script taking as input a text file with all the sentences to generate.
