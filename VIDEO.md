# Making videos

Explanation how to make and edit videos.

Videos have been made in H.264+AAC audio with Akira's personal Final Cut Pro.

Videos are converted on Unix in Webm Linux compatible format with ffmpeg command:
```
ffmpeg -i AddVideo.mov -c:v libvpx -crf 4 -b:v 0 -c:a libvorbis AddVideo.webm
```

Linux has no problem with playing H.264 ACC but Unity version on Linux seems to incompatible....
