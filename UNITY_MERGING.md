## How to merge Unity 3D `.unity` and `.prefab` files ##

The principle is to use Unity Smart Merge tool (UnityYAMLMerge).
The tool tries merging '.unity' and '.prefab' files, if it successes nothing
else to do, if it fails it runs a three-way merging tool with premerged files where you have to fix the conflicts.

1. Find the **Unity merge tool**: `UnityYAMLMerge`

   Normally, it is in your `Unity/Hub/Editor/2020.3.12f1/Editor/Data/Tools`.

   Let `UNITYYAMLMERGE_PATH` the absolute path of the directory.

2. Define a custom merge driver `unityyamlmerge`

  Add a section to your `$GIT_DIR/config` file (or `$HOME/.gitconfig` file) like this:

  ```
  [merge "unityyamlmerge"]
    name = Unity Yaml Merge
    driver = '/home/pierron/Unity/Hub/Editor/2020.3.12f1/Editor/Data/Tools/UnityYAMLMerge' merge -p "%O" "%B" "%A" "%P"
    recursive = binary
  ```

3. Define which tool to run when there is a merge conflict.

  Replace the lines in `<UNITYYAMLMERGE_PATH>/mergespecfile.txt`:
  ```
  unity use "%programs%\YouFallbackMergeToolForScenesHere.exe" "%l" "%r" "%b" "%d"
  prefab use "%programs%\YouFallbackMergeToolForPrefabsHere.exe" "%l" "%r" "%b" "%d"
  ```

  by these lines, if your favorite three ways merging tool is `meld`:

  ```
  unity use "/usr/bin/meld" "%b" "%l" "%r" -o "%d"
  prefab use "/usr/bin/meld" "%b" "%l" "%r" -o "%d"
  ```

4. Tell `Git` to use your specific merge tool by adding these lines
   into `.gitattributes` file at the root of your project:

    ```
    *.unity merge=unityyamlmerge
    *.prefab merge=unityyamlmerge
    *.asset binary
    ```

5. Using the `mergetool`

   If for some reason, the mergetool is not automatically running, you can start it by the command `git mergetool '*.unity'` for all the `.unity` files.

For more information:
- Configuring merge tool in Git: http://git-scm.com/docs/gitattributes#_defining_a_custom_merge_driver
- How to use Unity Smart Merge: https://docs.unity3d.com/Manual/SmartMerge.html
