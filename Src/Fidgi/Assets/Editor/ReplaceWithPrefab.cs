using UnityEngine;
using UnityEditor;

public class ReplaceWithPrefab : EditorWindow
{
    [SerializeField] private GameObject prefab;

    [MenuItem("Tools/Replace With Prefab")]
    static void CreateReplaceWithPrefab()
    {
        EditorWindow.GetWindow<ReplaceWithPrefab>();
    }

    private void OnGUI()
    {
        prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", prefab, typeof(GameObject), false);

        if (GUILayout.Button("Replace"))
        {
            var selection = Selection.gameObjects;
            var prefabType = PrefabUtility.GetPrefabAssetType(prefab);

            for (var i = selection.Length - 1; i >= 0; --i)
            {
                var selected = selection[i];
                GameObject newObject;

                if (prefabType == PrefabAssetType.Regular || prefabType == PrefabAssetType.Model || prefabType == PrefabAssetType.Variant)
                {
                    newObject = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
                    newObject.name = selected.name;
                }
                else
                {
                    newObject = Instantiate(prefab);
                    newObject.name = selected.name;
                }

                if (newObject == null)
                {
                    Debug.LogError("Error instantiating prefab");
                    break;
                }

                Undo.RegisterCreatedObjectUndo(newObject, "Replace With Prefabs");
                newObject.transform.parent = selected.transform.parent;
                newObject.transform.localPosition = selected.transform.localPosition;
                newObject.transform.localRotation = selected.transform.localRotation;
                newObject.transform.localScale = selected.transform.localScale;
                newObject.transform.SetSiblingIndex(selected.transform.GetSiblingIndex());

                // Copy fidgiObject content usisng JsonUtilty : https://forum.unity.com/threads/how-to-copy-component-value-from-an-object-to-another.697397/
                // Another maybe better solution : https://www.codegrepper.com/code-examples/csharp/unity+copy+component+to+another+gameobject
                // TODO : passing the Components to copy as parameter of the script or looping into the properties see GetProperties see script below
                var from = selected.GetComponent<FidgiObjectScript>();
                var json = JsonUtility.ToJson(from);
                var to = newObject.GetComponent<FidgiObjectScript>();
                JsonUtility.FromJsonOverwrite(json, to);
                
                // Undo.DestroyObjectImmediate(selected);
            }
        }

        GUI.enabled = false;
        EditorGUILayout.LabelField("Selection count: " + Selection.objects.Length);
        EditorGUILayout.LabelField("Prefab type: " + PrefabUtility.GetPrefabAssetType(prefab));
    }
}