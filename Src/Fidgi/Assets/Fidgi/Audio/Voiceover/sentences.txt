Pour l'ambiance jeu :
---------------------
*batt35 Batterie à 35%
*batt30 Batterie à 30%
*batt25 Batterie à 25%
*batt20 Batterie à 20%
*batt15 Batterie à 15%
*batt10 Batterie à 10%
*batt05 Attention! Batterie à 5%!
*batt04 Attention! Batterie à 4%!
*batt03 Attention! Batterie à 3%!
*batt02 Attention! Batterie à 2%! Sauvegardez
*batt01 Attention! Batterie à 1%! Extinction automatique enclenchée

Pour les bon appairages :
-------------------------
*pair_good01 Bravo !
*pair_good02 C'est la bonne association, super !
*pair_good03 Cooool, une paire de plus !

Pour les mauvais appairages :
-----------------------------
*pair_bad01 Hé non, ce n'est pas les bons
*pair_bad02 hélas, l'invention ne corresponds pas.
*pair_bad03 hélas, ils ne correspondent pas. recommences !


Pour l'encouragement :
----------------------
*pair7 Plus que 7 paires à faire
*pair6 Plus que 6 paires à faire
*pair5 Plus que 5 paires à faire
*pair4 Plus que 4 paires à faire
*pair3 Plus que 3 paires à faire
*pair2 Plus que 2 paires à faire
*pair1 Plus que 1 paires à faire, tu y es presque
*pair0 Bravo, tu as fais toutes les paires !

Pour les virus :
----------------
*virus01 Alerte virus ! Cette mémoire est corrompue.
*virus02 Évites qu'elle n'entre en contact avec une autre mémoire. Le virus pourrait se propager.

Lorsque l'utilisateur place sur le SSD :
----------------------------------------
*ssd_one Et une paire de sauvegardée. Es tu sur qu'elles sont dans le bon ordre chronologique ?
*ssd_success Bravo ! tu as réussi ! elles sont toutes là dans le bon ordre ! la sauvegarde s'effectue !
*ssd_all Bravo, tu as toutes les paires. Elles ne sont pas dans le bon ordre... tu y es presque.

Pour la copy machine :
----------------------
*copy_done Copie effectuée
*copy_call Copie demandée
*copy_running Copie en cours

Pour l'entrée dans le jeu :
---------------------------
*start00 Nous voici dans l'ordinateur.
*start01 Mais attention il ne reste plus beaucoup de batterie.
*start02 Vite, tu dois sauvegarder les mémoires avant qu'il ne s'éteigne.
*start03 Utilises le lecteur vidéo pour découvrir ces données.
*start04 Elles parlent d'un personnage important de l'informatique, ou bien d'une invention majeur pour nos ordinateurs.
*start05 Tu dois faire les bonnes associations d'inventeur et d'invention, et les sauvegarder dans l'ordre chronologique.
*start06 C'est parti !

Pour la fin :
----------------
*end_exit Ok, on arrête, à une prochaine fois
*end_backup Sauvegarde effectuée !
*end_backup_fail Toutes les données n'ont pas pu être sauvegardées ! dommage.

Pour le score :
------------------

*score Oh tu as fais un bon score, Entres tes initiales pour être dans le tableau des records.
*merci Merci

Akira Added :
------------------
Pair Help
------------------
*CanPair01 Cherche bien. Tu peux faire une paire.
*CanPair02 La bonne paire peux être faite maintenant.
*CanPair03 Cherche bien, tu peux faire une bonne paire.
*CanPair04 Tu peux faire une paire. Cherche bien.
*CanPair05 Il y a une invention qui peut être combinée.
*CanPair06 Il y a un personnage qui peut être combiné.

GPU Help
------------------
*GPU01 Utilise le lecteur pour voir ce qu'il y a sur la mémoire.
*GPU02 Tu n’as pas encore utilisé le lecteur.
*GPU03 Le lecteur te montrera ce qu'il y a dans la mémoire. Utilise le.
*GPU04 Coucou, tu sais que tu peux utiliser le lecteur?
*GPU05 Le lecteur t’attendes pour être utilisé.

CPU Help
------------------
*Copy01 Le copieur t’attend pour être utilisé.
*Copy02 Sauvegarde un double avec le copieur.
*Copy03 Essaye de faire une copie avant de combiner.
*Copy04 L'unité centrale CPU peut faire des copies.

SSD Help
------------------
*SSD01 Place la mémoire combinée sur le dock, mais dans le bon ordre chronologique.
*SSD02 Une fois combinée, place la mémoire combinée sur le dock. Mais attention à l'ordre.
*SSD03 Tu as une mémoire combinée. Tu peux la placer sur le dock. Mais attention à l'ordre.
*SSD04 Si tu as bien combiné les mémoires, place les sur le dock dans le bon ordre.

Virus Approaching
------------------
*VirusClose01 Attention le virus est à proximité !
*VirusClose02 Attention le virus approche !
*VirusClose03 Attention le virus est proche.
*VirusClose04 Attention au virus.
*VirusClose05 Attention, le virus se rapproche.

Virus Explanation
------------------
*VirusExpl01 Attention ! Si le virus touche une autre mémoire, il va la contaminer.

Final Congratulations
------------------
*Congrat01 Bon, on ne va pas se mentir, c'est pas un super score. Mais on ne lâche rien !
*Congrat02 Félicitation, C'est un bon score. Tu peux tenter de faire mieux si tu veux
*Congrat03 Bien joué ! Très bon score ! Recommences quand tu veux !
*Congrat04 Bravo, c'est parfait ! Tu viens travailler chez nous ?
