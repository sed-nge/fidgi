using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Fidgi Rotate Text")]

public class RotateText : MonoBehaviour
{

    public GameObject text;

    [SerializeField, Range(-150.0f, 150.0f)]
    public float radius = 30f;

    [SerializeField, Range(-90.0f, 90.0f)]
    public float angle = -10f;


    private TextMesh t;
    private Vector3 initialposition;

    private List<GameObject> Letters = new List<GameObject>();

    // Start is called before the first frame update
    private void Start()
    {
        t = text.GetComponent<TextMesh>();
        initialposition = new Vector3(text.transform.localPosition.x, text.transform.localPosition.y, text.transform.localPosition.z);

        draw();
    }

    public void changeText(string newText)
    {
        t.text = newText;
        draw();
    }


    private void erase()
    {
        foreach (GameObject letter in Letters)
        {
            Destroy(letter);
        }
        Letters.Clear();
    }

    private void draw()
    {

        erase();

        // text.transform.localPosition = new Vector3(initialposition.x, initialposition.y, initialposition.z + radius);
        t.offsetZ = -radius;

        Quaternion rot = Quaternion.Euler(0, angle, 0);
        Quaternion q = text.transform.rotation;


        float maxWidth = GetWidthOfChar("w".ToCharArray()[0]);
        float m = 0;
        foreach (char c in t.text)
        {
            m += GetWidthOfChar(c);
        }

        m = m / 2;

        float j = 0;
        foreach (char c in t.text)
        {
            Quaternion qq = Quaternion.Euler(0, angle * j, 0);
            createNewText(c, q * qq);
            float i = GetWidthOfChar(c);
            j += (i / maxWidth + 1);
        }
        text.SetActive(false);

    }


    private float GetWidthOfChar(char c)
    {
        Font font = t.font; //text is my UI text
        CharacterInfo characterInfo = new CharacterInfo();

        font.RequestCharactersInTexture(c.ToString());

        font.GetCharacterInfo(c, out characterInfo, t.fontSize);
        return (float)characterInfo.advance;
    }

    private void createNewText(char c, Quaternion q)
    {
        GameObject newObj = Instantiate(text, gameObject.transform);
        newObj.name = "letter_" + c;
        newObj.SetActive(true);
        newObj.GetComponent<TextMesh>().text = c.ToString();
        newObj.GetComponent<TextMesh>().alignment  = TextAlignment.Center;
        newObj.GetComponent<TextMesh>().anchor  = TextAnchor.MiddleCenter;
        
        newObj.transform.localPosition = text.transform.localPosition;
        newObj.transform.rotation = q;
        Letters.Add(newObj);
    }
}
