using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

[AddComponentMenu("Fidgi Webcam screen")]
[RequireComponent(typeof(Renderer))]


public class WebCamScreen : MonoBehaviour
{
    private WebCamTexture webcamTexture;

    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
        {
            try
            {
                webcamTexture = new WebCamTexture(devices[i].name);
                Renderer renderer = GetComponent<Renderer>();
                renderer.materials[2].mainTexture = webcamTexture;
                Debug.Log("Webcam " + devices[i].name + " started.");
                webcamTexture.Play();

                break;
            }
            catch (Exception e)
            {
                Debug.Log("Unable to initiate webcam " + devices[i].name + " error ");
                Debug.LogException(e);
            }
        }

    }

    private void OnEnable()
    {
        if (webcamTexture != null)
        {
            Debug.Log("Enable Webcam");
            webcamTexture.Play();
        }
    }

    private void OnDisable()
    {
        if (webcamTexture != null)
        {
            Debug.Log("Disable Webcam");
            webcamTexture.Stop();
        }

    }

}