using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEngine.XR.Interaction.Toolkit;


[RequireComponent(typeof(XRSocketInteractor))]
[AddComponentMenu("Excel cell")]


public class ExcelCell : Exe
{


    // --- set in unity interface
    [SerializeField, Tooltip("Id of the cell")]
    public int cellId = 0;

    [SerializeField, Tooltip("Cell year display")]
    public GameObject yearDisplay;
    private TextDelay yearText;

    private bool enabledCell = true;


    // Start is called before the first frame update
    public override void Start()
    {

        base.Start();

        // Debug.Log("Start for cell " + cellId);
        if (cellId >= globalVariables.numberOfCellToFill)
        {
            enabledCell = false;
            StartCoroutine(DisableCell());
        }

        if (yearDisplay)
        {
            yearText = yearDisplay.GetComponent<TextDelay>();
            if (!yearText) Debug.LogError("yearDisplay Object has no TextDelay script !");
            yearText.Clear();
        }

    }


    IEnumerator DisableCell()
    {
        yield return new WaitForSeconds(5f + cellId);
        anim.SetBool("slide", true);
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);

    }


    public void AnimCell()
    {
        anim.SetBool("slide", true);
    }
    public void AnimCellIfOk()
    {
        if (!fidgi) return;
        if (fidgi.isCombined() == false) return;
        anim.SetBool("slide", true);
    }



    public void RestartCell()
    {
        anim.SetBool("slide", false);
    }

    public override void OnSelectEnter()
    {

        base.OnSelectEnter();
        if (!fidgi) return;

        // this cell is disabled
        if (enabledCell == false)
        {
            ThrowObject();
            return;
        }

        Debug.Log("add fidgi object " + fidgi.name + " " + fidgi.isCombined());


        if (fidgi.isCombined() == false)
        {
            playRefuseSound();
            ThrowObject();
            // Debug.Log("add fidgi object NOT " + fidgi.name + " " + fidgi.isCombined());
            anim.SetBool("slide", false);
            return;
        }


        if (yearText)
        {

            yearText.write(string.Format("{0000}", getObjectDate()), 0.2f, 2.5f);
        }
        playAcceptSound();
    }

    public override void OnSelectExit()
    {

        Debug.Log("Remove fidgi object " + fidgi.name + " " + fidgi.isCombined());

        base.OnSelectExit();
    }


    public int getObjectDate()
    {
        if (!fidgi) return (-1);
        return fidgi.year;
    }

}
