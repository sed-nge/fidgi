using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.Events;

[System.Serializable]
public class MyIntEvent : UnityEvent<GameObject>
{
}

[AddComponentMenu("Fidgi Tutorial InCollider")]
public class TutoCollide : MonoBehaviour
{
    [SerializeField, Tooltip("Delay to stay in Collider")]
    public float delayToStay = 3f;

    [SerializeField, Tooltip("Tutorial event To trigger")]
    public MyIntEvent m_MyEvent = new MyIntEvent();

    [SerializeField, Tooltip("Tutorial event To trigger on znter")]
    public UnityEvent onEnterEvent = new UnityEvent();

    private FidgiObjectScript fidgi;
    private XRSocketInteractor xr;

    private GameObject objectToFollow = null;
    public GameObject followingObject { get { return objectToFollow; } }
    //private Grab XR = null;
    private XRGrabInteractable XR = null;
    private float startIn = 0;

    // Start is called before the first frame update
    public void Start()
    {
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == objectToFollow)
        {
            objectToFollow = null;
            startIn = 0;
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        fidgi = other.gameObject.GetComponent<FidgiObjectScript>();
        if (!fidgi) return;

//        XR = other.gameObject.GetComponent<Grab>();
        XR = other.gameObject.GetComponent<XRGrabInteractable>();
        if (!XR) return;

        objectToFollow = other.gameObject;
        startIn = Time.time;


        if (onEnterEvent == null) return;
        if (onEnterEvent.GetPersistentEventCount() == 0) return;

        onEnterEvent?.Invoke();
    }

    private void Update()
    {
        if (!objectToFollow) return;
        if (XR.isSelected) return;

        // check if stay One second (in case of falling outside)
        if (startIn == 0)
        {
            startIn = Time.time;
            return;
        }

        if (Time.time > (startIn + delayToStay))
        {
            if (m_MyEvent == null) return;
            if (m_MyEvent.GetPersistentEventCount() == 0) return;

            m_MyEvent?.Invoke(gameObject);
            startIn = 0;
            objectToFollow = null;
        }
    }

}
