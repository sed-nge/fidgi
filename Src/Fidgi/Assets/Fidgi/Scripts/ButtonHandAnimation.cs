using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Animator))]
[AddComponentMenu("Fidgi Hand help animation Script")]


public class ButtonHandAnimation : MonoBehaviour
{


    public float startDelay = 6f;
    public bool enableAnim = true;

    public GameObject hand = null;
    public GameObject Text = null;

    private AmbiantVoice ambiantVoice;

    private IEnumerator showRoutine = null;

    // Start is called before the first frame update
    void Start()
    {
        stopAnim();



        AmbiantVoice[] s = FindObjectsOfType(typeof(AmbiantVoice)) as AmbiantVoice[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one AmbiantVoice in the scene ;");

        }
        else
        {
            ambiantVoice = s[0];
        }

        showRoutine = StartAnimDelay();
        StartCoroutine( showRoutine );
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void stopAnimationTheUserHasUnderstood() {
        enableAnim = false;
        stopAnim();
    }


    IEnumerator StartAnimDelay() {
        yield return new WaitForSeconds(startDelay);
        startAnim();
        while ( true ) {
            yield return new WaitWhile(() => ambiantVoice.isMuted());
            yield return new WaitForSeconds(Random.Range(5,30) );
            if ( enableAnim ) ambiantVoice.HelpButton();
            yield return new WaitForSeconds(startDelay);
        }
    }


    public void startAnim() {
        if ( ! enableAnim ) return;
        if ( hand ) hand.SetActive(true);
        if ( Text ) Text.SetActive(true);
    }

    public void stopAnim() {
        if ( showRoutine != null ) {
            StopCoroutine( showRoutine );
            showRoutine = null;
        }
        if ( hand ) hand.SetActive(false);
        if ( Text ) Text.SetActive(false);
    }

}
