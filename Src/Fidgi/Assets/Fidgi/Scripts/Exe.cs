using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


[RequireComponent(typeof(XRSocketInteractor))]
[AddComponentMenu("Fidgi Exec")]

/// <summary>
/// Manage the enter, exit in an object
/// </summary>
public abstract class Exe : MonoBehaviour
{

    [SerializeField, Tooltip("Sound on grab")]

    public AudioClip acceptSound = null;
    [SerializeField, Tooltip("Sound on refuse grab")]

    public AudioClip refuseSound = null;


    protected float startTime;

    protected GameObject target;
    protected FidgiObjectScript fidgi;
    protected XRSocketInteractor xr;

    protected Animator anim;
    protected float throwTime = 0f;
    public float throwDelay = 1.2f;
    public float throwForce = 3.0f;
    protected bool mustThrow = false;

    protected GameObject player;
    protected AudioSource sound;
    protected AmbiantVoice ambiantVoice;

    public virtual void Start()
    {
        xr = GetComponent<XRSocketInteractor>();
        xr.socketActive = true;

        anim = GetComponent<Animator>();
        if (!anim)
        {
            Debug.Log("No Animator found in Exe");
        }

        player = GameObject.Find("XR Rig");

        sound = gameObject.GetComponent<AudioSource>();


        AmbiantVoice[] s = FindObjectsOfType<AmbiantVoice>();
        if (s.Length != 1)
        {
            Debug.LogError("More than one or no AmbiantVoice in the scene");

        }
        else
        {
            ambiantVoice = s[0];
        }

    }

    protected void playAcceptSound()
    {
        if (!sound) return;
        if (!acceptSound) return;
        sound.PlayOneShot(acceptSound);
    }

    protected void playRefuseSound()
    {
        if (!sound) return;
        if (!refuseSound) return;
        sound.PlayOneShot(refuseSound);
    }


    public virtual GameObject getTarget()
    {
        if ( ! xr ) Debug.Log("No XR ???");
        if ( ! xr.selectTarget ) return null;
        return xr.selectTarget.gameObject;
    }
    public virtual bool filled()
    {
        return getTarget() ? true : false;
    }

    public virtual void OnSelectEnter()
    {
        if (throwTime > 0.0f)
        {
            return;
        }

        // check if the gameObject is a Fidgi object
        target = gameObject.GetComponent<XRSocketInteractor>().selectTarget.gameObject;
        if (!target) return;
        fidgi = target.GetComponent<FidgiObjectScript>();
        if (!fidgi)
        {
            playRefuseSound();
            ThrowObject();
            return;
        }

        startTime = Time.time;
    }


    public virtual void Update()
    {
        if ((throwTime > 0.0f) && (Time.time > (throwTime + throwDelay)))
        {
            Debug.Log("reset xr");
            xr.socketActive = true;
            throwTime = 0.0f;
        }
    }

    public virtual void OnSelectExit()
    {

        if (throwTime > 0.0f)
        {
            if (target)
            {
                Rigidbody rc = target.GetComponent<Rigidbody>();
                xr.socketActive = false;
                if (rc)
                {
                    rc.isKinematic = false;
                    rc.useGravity = true;
                    Vector3 direction = Vector3.Normalize(player.transform.position - transform.position);

                    rc.AddForceAtPosition(new Vector3(direction.x * throwForce, throwForce, direction.z * throwForce), transform.position, ForceMode.Impulse);
                }
            }
            target = null;
            fidgi = null;
            return;
        }

        throwTime = 0.0f;
        target = null;
        fidgi = null;
    }


    public virtual void OnHoverEnter()
    {
    }

    public virtual void OnHoverExit()
    {

    }


    protected internal void ThrowObject()
    {

        throwTime = Time.time;
        xr.socketActive = false;
        mustThrow = true;
        return;
    }

}
