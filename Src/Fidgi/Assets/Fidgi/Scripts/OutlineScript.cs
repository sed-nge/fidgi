using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Fidgi Outline Effect")]
public class OutlineScript : MonoBehaviour
{

    public float intensity = 3f;
    public List<int> hilightMeshesIndexes = new List<int>();

    private Color EmissionColor;

    private List<Color> EmissionColors = new List<Color>();
    private List<Color> HighlightsColors = new List<Color>();
    private List<OutlineScript> Subs = new List<OutlineScript>();


    private bool initialized = false;
    void Start()
    {
    }


    public void Initialise()
    {
        Renderer render = gameObject.GetComponent<Renderer>();
        if (!render) return;
        Material[] materials = render.materials;

        // --- If no set All materials        
        if (hilightMeshesIndexes.Count == 0)
        {
            for (int i = 0; i < materials.Length; i++)
                hilightMeshesIndexes.Add(i);
        }

        for (int i = 0; i < hilightMeshesIndexes.Count; i++)
        {
            int index = hilightMeshesIndexes[i];
            Color c = (materials.Length > index) ? materials[index].GetColor("_EmissionColor") : Color.black;
            Color highLight = c * intensity;
            EmissionColors.Add(c);
            HighlightsColors.Add(highLight);
        }

        // --- Find all Subs with the same Script
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            Transform t = gameObject.transform.GetChild(i);
            if ( ! t ) continue;
            OutlineScript outs = t.gameObject.GetComponent<OutlineScript>();
            if ( ! outs ) continue;
            Subs.Add(outs);
            outs.Initialise();
        }


        initialized = true;
    }

    public void StartOutline()
    {
        if (!initialized) Initialise();

        Renderer render = gameObject.GetComponent<Renderer>();
        if (!render) return;
        Material[] materials = render.materials;


        for (int i = 0; i < hilightMeshesIndexes.Count; i++)
        {
            int index = hilightMeshesIndexes[i];
            if (materials.Length <= index) continue;
            materials[index].SetColor("_EmissionColor", HighlightsColors[i]);
        }


        for ( int i=0; i <Subs.Count; i++) {
            Subs[i].StartOutline();
        }
        
    }


    public void StopOutline()
    {
        if (!initialized) return;

        Renderer render = gameObject.GetComponent<Renderer>();
        if (!render) return;
        Material[] materials = render.materials;
        for (int i = 0; i < hilightMeshesIndexes.Count; i++)
        {
            int index = hilightMeshesIndexes[i];
            if (materials.Length > index)
            {
                materials[index].SetColor("_EmissionColor", EmissionColors[i]);
            }
        }

        for ( int i=0; i <Subs.Count; i++) {
            Subs[i].StopOutline();
        }


    }
}


