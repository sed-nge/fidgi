using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[RequireComponent(typeof(TextMesh))]
[AddComponentMenu("Fidgi Text Delay")]


public class TextDelay : MonoBehaviour
{

    private TextMesh txt;

    private string currentDisplayed;
    private IEnumerator writeText = null;

    // Start is called before the first frame update
    void Start()
    {
        txt = gameObject.GetComponent<TextMesh>();
        if (!txt)
        {
            Debug.LogError("Unable to write text. no TextMesh into SlotText");
            return;
        }
        txt.text = "";
    }


    private void writeInternal(string text, float speed, float delay)
    {
        if (writeText != null)
        {
            StopCoroutine(writeText);
        }
        writeText = writeDelay(text, speed, delay);
        StartCoroutine(writeText);
    }
    public void write(string text, float speed)
    {
        writeInternal(text, speed, 0f);
    }
    public void write(string text, float speed, float delay)
    {
        writeInternal(text, speed, delay);
    }
    public void write(string text)
    {
        writeInternal(text, 0.05f, 0f);
    }


    IEnumerator writeDelay(string text, float speed, float delayClear )
    {
        currentDisplayed = string.Empty;
        char[] arr = text.ToCharArray();
        for (int i = 0; i < text.Length; i++)
        {
            currentDisplayed += arr[i];
            txt.text = currentDisplayed;

            yield return new WaitForSeconds(speed);
        }
        if ( delayClear > 0 ) {
            yield return new WaitForSeconds(delayClear);
            Clear();
        }
    }


    public void Clear()
    {
        if ( txt ) txt.text = "";
    }

}

//write static in front of the variable, then in the other script you can access by typing
//origscriptname.variablename
