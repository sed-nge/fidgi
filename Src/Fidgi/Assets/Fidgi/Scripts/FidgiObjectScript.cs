using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.AI;
using UnityEngine.XR.Interaction.Toolkit;


[RequireComponent(typeof(Rigidbody))]
[AddComponentMenu("Fidgi Object Script")]

public class FidgiObjectScript : MonoBehaviour
{

    /// <summary>
    /// This script is the main script for objects
    /// </summary>

    public enum ObjectType : ushort
    {
        Empty = 0,
        Character = 1,
        Invention = 2,
        Combined = 3,
        Virus = 4,
    }


    [SerializeField, Tooltip("Color for Invention")]
    public Color colorInvention;
    [SerializeField, Tooltip("Color for Character")]
    public Color colorCharacter;
    [SerializeField, Tooltip("Color for Combined")]
    public Color colorCombined;
    [SerializeField, Tooltip("Color for Viruses")]
    public Color colorViruses;
    [SerializeField, Tooltip("Color for Other")]
    public Color colorOther;
    [SerializeField, Tooltip("Color for Specific")]
    public Color colorSpecific;
    [SerializeField, Tooltip("Use specific color")]
    public bool specificColor = false;


    [SerializeField, Tooltip("type of the object")]
    public ObjectType type = ObjectType.Invention;


    [SerializeField, Tooltip("if the inventor is a woman")]
    public bool isWoman = false;

    [SerializeField, Tooltip("Name of the current character / invention")]
    public string matchName = "Lee";

    [SerializeField, Tooltip("The year for the final order")]
    public int year = 0;


    [SerializeField, Range(0.0f, 10.0f), Tooltip("Minimum of time to stay in the reader to get infos")]
    public float minimumInfo = 3.0f;


    [SerializeField, Tooltip("Icon for the Fidgi object")]
    public GameObject icons;


    [SerializeField, Tooltip("List of videoclip to start when enter the videoreader")]
    public List<VideoClip> videoClips = new List<VideoClip>();

    private int currentClip = 0;


    [SerializeField, Tooltip("Virus maximum hunter time")]
    public float virusDelay = 30f;

    private NavMeshAgent agent;



    [SerializeField, Tooltip("List of holograms to display when enter the videoreader")]
    public List<GameObject> holograms = new List<GameObject>();
    [SerializeField, Range(0.0f, 10.0f), Tooltip("delay for each holograms")]
    public float hologramsShowDelay = 3.0f;
    private int currentHologram = 0;


    private Rigidbody rb;
    private GameObject Label;
    private GameObject Picture;
    private GameObject Icon;
    private GameObject Capsule;
    private GameObject Projector;
    private GameObject Question;
    private GameObject Rings;
    private GameObject Cylinder;
    private GameObject Mem;
    public bool InfosVisible = false;

    private Vector3 PositionOffset = new Vector3(0, 0.5f, 0);
    private Vector3 creationForce = new Vector3(0, 3f, 0);

    private IEnumerator scanRoutine = null;

    private Color glassEmissionColor;
    private GameObject other = null;

    private PairingVoice pairingVoice;


    void Start()
    {

        agent = GetComponent<NavMeshAgent>();

        rb = GetComponent<Rigidbody>();

        Label = FindSubObject("Mem/Label");
        if (!Label) Debug.LogError("No \"Label\" as a children of \"" + gameObject.name + "\"");

        Picture = FindSubObject("Mem/Pictures");
        Icon = FindSubObject("Mem/Icon");
        Capsule = FindSubObject("Mem/Icon/Capsule");
        Projector = FindSubObject("Mem/Projector");
        Question = FindSubObject("Mem/Question");
        Rings = FindSubObject("Rings");
        Cylinder = FindSubObject("Mem/Cylinder");
        Mem = FindSubObject("Mem");

        if (icons)
        {
            GameObject t = Instantiate(icons, Icon.transform);
            t.transform.localPosition = Capsule.transform.localPosition;
            t.SetActive(true);
            t.name = "Icons";
        }
        Capsule.SetActive(false);

        ChangeColor();
        OnEnable();

        Renderer render = Mem.GetComponent<Renderer>();

        // --- Store the emission color intensity of the glass
        Material[] materials = render.materials;
        if (materials.Length == 2)
        {
            glassEmissionColor = materials[0].GetColor("_EmissionColor");
        }

        PairingVoice[] wl = FindObjectsOfType(typeof(PairingVoice)) as PairingVoice[];
        if (wl.Length == 1)
        {
            pairingVoice = wl[0];
        }



    }



    public void ChangeColor()
    {
        if (specificColor == false)
        {
            switch (type)
            {
                case ObjectType.Character:
                    changeColor(colorCharacter);
                    break;
                case ObjectType.Invention:
                    changeColor(colorInvention);
                    break;
                case ObjectType.Combined:
                    changeColor(colorCombined);
                    break;
                case ObjectType.Virus:
                    if (InfosVisible)
                    {
                        changeColor(colorViruses);
                    }
                    else
                    {
                        changeColor((Random.Range(0, 2) == 0) ? colorCharacter : colorInvention);
                    }
                    break;
                default:
                    changeColor(colorOther);
                    break;
            }
        }
        else
        {
            changeColor(colorSpecific);
        }

    }


    public bool PairingInProgress()
    {
        return other != null ? true : false;
    }

    //    private void OnTriggerEnter(Collider collider)
    void OnCollisionEnter(Collision collider)
    {
        // --- Already in a collision with another object ---
        if (PairingInProgress()) return;

        if (!collider.gameObject) return;
        FidgiObjectScript script = collider.gameObject.GetComponent<FidgiObjectScript>();

        // this is not an Fidgi Game Script 
        if (!script) return;


        // --- The other is actually pairing with someone else
        if (script.PairingInProgress())
        {
            if (script.other.gameObject.GetInstanceID() != gameObject.GetInstanceID()) return;
        }

        // --- In case of a virus stop navigation and movement
        if (isVirus())
        {
            // --- Nothing append beettween virus ---
            if (script.isVirus()) return;

            /*
                    rb.isKinematic = true;
                    rb.velocity = Vector3.zero;
                    StopVirusAgent();
                    rb.isKinematic = false;
                    */
            Debug.Log("Virus paring " + gameObject.name);
        }


        other = collider.gameObject;
    }

    void OnCollisionExit(Collision collider)
    {

        if (!collider.gameObject) return;
        FidgiObjectScript script = collider.gameObject.GetComponent<FidgiObjectScript>();

        // this is not an Fidgi Game Script 
        if (!script) return;

        if (!PairingInProgress()) return;

        if (other.GetInstanceID() == collider.gameObject.GetInstanceID())
        {
            Debug.Log("Virus end paring" + gameObject.name);

            other = null;
            return;
        }

    }


    private void AbortParing()
    {
        if (scanRoutine != null)
        {
            Debug.Log(gameObject.name + " end scanRoutine ");
            StopCoroutine(scanRoutine);
            scanRoutine = null;
            Rings.SetActive(false);
            Cylinder.SetActive(false);

            // --- Reset the right color
            Renderer render = Mem.GetComponent<Renderer>();
            Material[] materials = render.materials;
            if (materials.Length == 2)
            {
                Color emi = materials[0].GetColor("_EmissionColor");
                materials[0].SetColor("_EmissionColor", glassEmissionColor);
            }

            // --- In case of a virus, restart the agent (stopped at the beginning of the paring process)
            if (isVirus())
            {
                VirusAI ai = GetComponent<VirusAI>();
                ai.resetTarget();
                Debug.Log("Virus abort paring" + gameObject.name);
            }

        }
        other = null;
    }

    private void Update()
    {
        // --- no other actually in collision (or just leave)
        if (!other)
        {
            if (scanRoutine != null)
            {
                //Debug.Log("Abort Pairing because " + gameObject.name + " has no other and a scan routine in progress");
                AbortParing();
            }
            return;
        }

        //        if (gameObject.GetComponent<Grab>().isSelected)
        if (gameObject.GetComponent<XRGrabInteractable>().isSelected)
        {
            //Debug.Log("Abort Pairing because " + gameObject.name + " is selected");
            //AbortParing();
            return;
        }
        if (other.GetComponent<XRGrabInteractable>().isSelected)
        {
            //Debug.Log("Abort Pairing because " + other.gameObject.name + " is selected");
            //AbortParing();
            return;
        }

        if (scanRoutine == null)
        {            
            if (isVirus())
            {
              
                VirusAI ai = GetComponent<VirusAI>();
                if (ai) ai.resetTarget();
            }

            scanRoutine = Scan(other.GetComponent<FidgiObjectScript>());
            StartCoroutine(scanRoutine);
        }
    }


    IEnumerator Scan(FidgiObjectScript otherScript)
    {
        // Debug.Log(gameObject.name + " start scanRoutine with " + other.gameObject.name);
        yield return new WaitForSeconds(0.5f);
        Cylinder.SetActive(true);
        yield return new WaitForSeconds(3f);
        Cylinder.SetActive(false);

        // --- I am a virus ---
        if (isVirus())
        {
            if (!otherScript.isVirus())
            {
                VirusAI ai = GetComponent<VirusAI>();
                Vector3 startPosition = other.transform.position;
                Quaternion startRotation = other.transform.rotation;
                otherScript.other = null;
                activateInfos();
                otherScript.Cylinder.SetActive(false);
                otherScript.gameObject.SetActive(false);
                other = null;
                ai.CloneMe(startPosition, startRotation);
                StopCoroutine(scanRoutine);
            }
        }

        if (mustExplode(otherScript))
        {
            Rings.SetActive(true);
            yield return burn(4f);

            if (mustSpawnCombined(otherScript))
            {
                // ---- check if the combined is already on scene
                // ---- check if the combined is already on scene
                FidgiObjectScript combined = GetRelatedObject(matchName);
                if ((combined) && (combined.gameObject.activeSelf == false))
                {
                    Debug.Log(" good one " + matchName + "/" + type + " -- " + otherScript.matchName + "/" + otherScript.type);
                    SpawnCombined();
                    if (pairingVoice) pairingVoice.GoodPair();
                }

            }
            else
            {
                if ((type == ObjectType.Character) && (pairingVoice)) pairingVoice.WrongPair();
            }
            InfosVisible = false;
            Rings.SetActive(false);
            gameObject.SetActive(false);
        }
        other = null;
        StopCoroutine(scanRoutine);
    }



    public void ThrowUp(GameObject o)
    {
        Rigidbody rc = o.GetComponent<Rigidbody>();
        if (rc)
        {
            rc.useGravity = true;
            //rc.isKinematic = false;
            rc.velocity = new Vector3(0f, 0f, 0f);

            o.gameObject.SetActive(true);
            // Add a vertical small force to make a jump of the new object
            rc.AddForce(creationForce, ForceMode.Impulse);
        }
        else
        {
            o.gameObject.SetActive(true);
        }

    }

    private void SpawnCombined()
    {
        FidgiObjectScript combined = GetRelatedObject(matchName);
        if (!combined) return;

        // set the position of the new Object relative to the previous one
        combined.transform.position = gameObject.transform.position + PositionOffset;
        combined.transform.rotation = Random.rotation;

        // -- Set values from character if not in combined
        // --- TODO ---


        // --- set info visible on the combined object
        combined.InfosVisible = false;
        if (other)
        {
            FidgiObjectScript ff = other.GetComponent<FidgiObjectScript>();
            combined.InfosVisible = InfosVisible || ff.InfosVisible;
        }

        ThrowUp(combined.gameObject);
    }



    IEnumerator burn(float delay)
    {
        Renderer render = Mem.GetComponent<Renderer>();
        Material[] materials = render.materials;
        if (materials.Length == 2)
        {
            float t = Time.time;
            while ((Time.time - t) <= delay)
            {
                float intensity = (Time.time - t) / delay * 600;

                materials[0].SetColor("_EmissionColor", glassEmissionColor * intensity);
                yield return new WaitForSeconds(0.2f);
            }
        }

    }


    private bool mustExplode(FidgiObjectScript other)
    {
        if (((isCharacter()) && (other.isInvention())) ||
        ((isInvention()) && (other.isCharacter()))) return true;
        return false;
    }

    private bool mustSpawnCombined(FidgiObjectScript other)
    {
        if ((isCharacter()) && (other.isInvention()))
        {
            if (matchName != other.matchName) return false;
            return true;
        }
        return false;
    }

    private bool PlayMessage(FidgiObjectScript other)
    {
        if (isCharacter())
        {

        }
        if ((isCharacter()) && (other.isInvention()))
        {
            if (matchName != other.matchName) return false;
            // ---- check if the combined is already on scene
            FidgiObjectScript combined = GetRelatedObject(matchName);
            if (!combined) return false;
            if (combined.gameObject.activeSelf == true) return false;
            return true;
        }
        return false;
    }


    private void OnEnable()
    {

        if (Label) Label.SetActive(InfosVisible ? true : false);
        if (Picture) Picture.SetActive(InfosVisible ? true : false);
        if (Question) Question.SetActive(InfosVisible ? false : true);
        if (Cylinder) Cylinder.SetActive(false);

        if (Icon) Icon.SetActive(false);
        if (Projector) Projector.SetActive(false);
        if (Rings) Rings.SetActive(false);

        other = null;
    }



    private void changeColor(Color c)
    {
        if (!Mem) Debug.LogError("ARG MEM no configured ");
        Renderer render = Mem.GetComponent<Renderer>();
        Material[] materials = render.materials;
        if (materials.Length == 2)
        {

            materials[1].SetColor("_BaseColor", c);
            Color emi = materials[1].GetColor("_EmissionColor");
            float intensity = Mathf.Max(emi.r, emi.g, emi.b);

            materials[1].SetColor("_EmissionColor", c * intensity);
        }
        changeColorSubObject(Question, c);
        changeColorSubObject(Cylinder, c);
    }

    private void changeColorSubObject(GameObject o, Color c)
    {
        if (!o) return;
        Renderer render = o.GetComponent<Renderer>();
        if (!render) return;

        render.material.SetColor("_BaseColor", c);
        Color emi = render.material.GetColor("_EmissionColor");
        float intensity = Mathf.Max(emi.r, emi.g, emi.b);
        render.material.SetColor("_EmissionColor", c * intensity);
    }



    public void InHand()
    {
        if (InfosVisible)
        {
            //if (Projector) Projector.SetActive(true);
            if (Icon) Icon.SetActive(true);
        }
    }

    public void LeaveHand()
    {
        //if (Projector) Projector.SetActive(false);
        if (Icon) Icon.SetActive(false);
    }



    private GameObject FindSubObject(string name)
    {
        Transform trans = gameObject.transform.Find(name);
        if (trans == null) return null;
        return trans.gameObject;
    }


    public VideoClip GetVideo()
    {
        if (videoClips.Count == 0) return null;
        if (currentClip >= videoClips.Count) currentClip = 0;
        return videoClips[currentClip];
    }


    public GameObject GetHologram()
    {
        if (holograms.Count == 0) return null;
        GameObject holo = holograms[currentHologram];
        currentHologram++;
        if (currentHologram >= holograms.Count) currentHologram = 0;
        return holo;
    }

    public void activateInfos(bool visible = true)
    {

        InfosVisible = visible;
        ChangeColor();

        if (Label) Label.SetActive(visible);
        if (Question) Question.SetActive(!visible);
        if (Picture) Picture.SetActive(visible);
    }


    public bool isCombined()
    {
        return type == ObjectType.Combined ? true : false;
    }
    public bool isCharacter()
    {
        return type == ObjectType.Character ? true : false;
    }
    public bool isInvention()
    {
        return type == ObjectType.Invention ? true : false;
    }
    public bool isVirus()
    {
        return type == ObjectType.Virus ? true : false;
    }
    public bool isEmpty()
    {
        return type == ObjectType.Empty ? true : false;
    }

    private FidgiObjectScript GetRelatedObject(string matchName)
    {
        var fidgiObjects = GameObject.Find("FidgiObjects");

        foreach (var fidgi in fidgiObjects.GetComponentsInChildren<FidgiObjectScript>(true))
        {
            if ((fidgi.matchName == matchName) && (fidgi.isCombined()))
            {
                return fidgi;
            }
        }

        return null;
    }

}
