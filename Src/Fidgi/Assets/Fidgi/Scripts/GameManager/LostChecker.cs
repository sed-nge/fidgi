using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{

    /// <summary>
    /// Check if the game is finished and decrease lights
    /// </summary>
    [AddComponentMenu("Fidgi lost and light checker")]

    public class LostChecker : ManagerOfGame
    {

        [SerializeField, Range(10f, 60.0f), Tooltip("Blink duration at the end of the game")]
        public float blinkDurationAtTheEnd = 15f;

        private GameObject[] blinkingObjects = null;


        // Start is called before the first frame update
        public override void Start()
        {
            base.Start();

            GameObject SlotText = GameObject.Find("SlotText");
            if (!SlotText) Debug.LogError("SlotText Object not found !");
            tabletText = SlotText.GetComponent<TextDelay>();
            if (!tabletText) Debug.LogError("SlotText Object has no TextDelay script !");


            WinAndLost[] wl = FindObjectsOfType(typeof(WinAndLost)) as WinAndLost[];
            if (wl.Length != 1)
            {
                Debug.LogError("More than one WinAndLost in the scene ;");

            }
            else
            {
                winAndLostSound = wl[0];
            }

            blinkingObjects = GameObject.FindGameObjectsWithTag("blinkingLight");
            if (blinkingObjects.Length == 0)
            {
                Debug.LogError("No blinking objects found (tag blinkingLight)");
            }

            StartCoroutine(DecreaseTime());
        }



        /// <summary>
        /// After half time, check every second if time finished.
        /// If finished... failed.
        /// start blinking lights 
        /// </summary>
        /// <returns></returns>
        IEnumerator DecreaseTime()
        {
            Debug.Log("Start checkin loose");

            bool blinkInProgress = false;
            yield return new WaitForSeconds(globalVariables.gameDuration / 2);

            while (true)
            {
                yield return new WaitForSeconds(1);
                float gameTime = Time.time - globalVariables.startTime;

                // --- Finish - you loose !!!
                if (gameTime > globalVariables.gameDuration)
                {
                    tabletText.write("Batterie\r\ndéchargée");
                    if (winAndLostSound) winAndLostSound.Lost();
                    if (sceneSwitcher) sceneSwitcher.GotoMenuScene();
                    yield break;
                }

                // -- start the blink (close to the end)
                if (!blinkInProgress)
                {
                    float remainingTime = globalVariables.gameDuration - gameTime;
                    if (remainingTime < blinkDurationAtTheEnd)
                    {
                        blinkInProgress = true;
                        for (int i = 0; i < blinkingObjects.Length; i++)
                        {
                            LightMaterials l = blinkingObjects[i].GetComponent<LightMaterials>();
                            if (l != null) l.blink(remainingTime);
                            LightManager lm = blinkingObjects[i].GetComponent<LightManager>();
                            if (lm != null) lm.blink(remainingTime);
                        }
                    }
                }
            }
        }

    }


}
