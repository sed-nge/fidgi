using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{

    public abstract class ManagerOfGame : MonoBehaviour
    {

        protected TextDelay tabletText;

        protected WinAndLost winAndLostSound;

        protected SceneSwitcher sceneSwitcher;


        // Start is called before the first frame update
        public virtual void Start()
        {
            // --- Select the tablet to add Text
            GameObject SlotText = GameObject.Find("SlotText");
            if (!SlotText) Debug.LogError("SlotText Object not found !");
            tabletText = SlotText.GetComponent<TextDelay>();
            if (!tabletText) Debug.LogError("SlotText Object has no TextDelay script !");

            // --- Select the WindAndLod sounds
            WinAndLost[] wl = FindObjectsOfType(typeof(WinAndLost)) as WinAndLost[];
            if (wl.Length != 1)
            {
                Debug.LogError("More than one WinAndLost in the scene ;");

            }
            else
            {
                winAndLostSound = wl[0];
            }

            // --- Select the sceneswitcher (to go back to the menu scene)
            GameObject sceneSwitcherGo = GameObject.Find("SceneSwitcherMain");
            if (!sceneSwitcherGo)
            {
                Debug.LogError("No SceneSwitcherMain object found !");
            }
            else
            {
                sceneSwitcher = sceneSwitcherGo.GetComponent<SceneSwitcher>();
                if (!sceneSwitcher) Debug.LogError("SceneSwitcherMain doesnt contain the script sceneSwitcher");
            }
        }



    }
}