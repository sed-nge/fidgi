using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{

    public class Probability
    {
        public GameObject g;
        public float startP = 0.0f;
        public float proba = 0.3f;

        public Probability(GameObject gg, float initialWeight)
        {
            g = gg;
            proba = initialWeight;
        }
    }


    [AddComponentMenu("Fidgi GameManager card spawner")]
    public class ObjectManager : SpawnManager
    {


        [SerializeField, Range(0f, 10f), Tooltip("Weight for probability for each object")]
        public float initialWeight = 1f;
        [SerializeField, Range(0f, 10f), Tooltip("Weight for probability to have woman on scene")]
        public float weightOfWomens = 3f;
        [SerializeField, Range(0f, 10f), Tooltip("Weight for probability to have combinable on scene")]
        public float weightOfCombineds = 3f;


        [SerializeField, Range(1f, 10.0f), Tooltip("Delay to wait before the first help message")]
        public float delayFirstHelpMessage = 2f;

        [SerializeField, Tooltip("Delay beetween 2 help messages")]
        public float delayHelpMessageInterval = 5f;

        private ExcelCell[] excelCells;

        protected bool combinedOnScene = false;
        protected bool pairableOnScene = false;

        // Start is called before the first frame update
        public override void Start()
        {

            base.Start();

            excelCells = FindObjectsOfType(typeof(ExcelCell)) as ExcelCell[];


            StartCoroutine(HelpMessage());
        }


        private bool SSDEmpty()
        {
            for (int i = 0; i < excelCells.Length; i++)
            {
                int y = excelCells[i].getObjectDate();
                if (y != -1) return false;
            }
            return true;
        }


        protected override void CheckIfNeedToSpawn()
        {

            List<GameObject> objects = new List<GameObject>();
            Probability lastWeight;

            List<Probability> spawnAbleListWithProba = new List<Probability>();

            combinedOnScene = false;
            pairableOnScene = false;

            FidgiObjectScript[] items = FindObjectsOfType<FidgiObjectScript>(true) as FidgiObjectScript[];

            for (int i = 0; i < items.Length; i++)
            {

                GameObject g = items[i].gameObject;
                FidgiObjectScript fidgi = g.GetComponent<FidgiObjectScript>();
                if (fidgi == null) continue;

                // Dont care about objects curently on scene
                if (g.activeSelf == true) continue;

                // --- Don't care about viruses
                if (fidgi.type == FidgiObjectScript.ObjectType.Virus) continue;

                // --- Don't care about combined
                if (fidgi.type == FidgiObjectScript.ObjectType.Combined) continue;



                Probability weight = new Probability(g, initialWeight);

                if (fidgi.isWoman == true)
                {
                    weight.proba += weightOfWomens;
                }

                for (int j = 0; j < items.Length; j++)
                {
                    if ( i == j) continue;

                    GameObject g1 = items[j].gameObject;
                    FidgiObjectScript fidgi1 = g1.GetComponent<FidgiObjectScript>();

                    if (g1.activeSelf == false) continue;
                    if (fidgi1.type == FidgiObjectScript.ObjectType.Virus) continue;

                    //Debug.Log("Testing " + g.name + " with " + g1.name);


                    if (fidgi.matchName == fidgi1.matchName)
                    {
                    //Debug.Log("Same " + g.name + " with " + g1.name);


                        // if a clone is on scene -> ignore ( proba = 0 )
                        if (fidgi1.type == fidgi.type)
                        {
                            weight.proba = 0;
                            break;
                        }

                        // if the combined is on scene -> ignore ( proba = 0 )
                        if (fidgi1.type == FidgiObjectScript.ObjectType.Combined)
                        {
                            weight.proba = 0;
                            break;
                        }

                        // The combinable is on scene
                        // Debug.Log("Adding combined proba for " + g.name);
                        weight.proba += weightOfCombineds;
                        break;
                    }

                }

                if (spawnAbleListWithProba.Count > 0)
                {
                    lastWeight = spawnAbleListWithProba[spawnAbleListWithProba.Count - 1];
                    weight.startP = lastWeight.startP + lastWeight.proba;
                }

                spawnAbleListWithProba.Add(weight);
            }

            // --- Select the Object to spawn ---
            if (spawnAbleListWithProba.Count == 0) return;

            lastWeight = spawnAbleListWithProba[spawnAbleListWithProba.Count - 1];
            float max = lastWeight.startP + lastWeight.proba;
            float f = Random.Range(0.0f, max);

            for (int i = 0; i < spawnAbleListWithProba.Count; i++)
            {
                Probability weight = spawnAbleListWithProba[i];

                //Debug.Log("Spawn proba " + weight.startP + " -> " + (weight.startP + weight.proba) + " : " + weight.g.name);

                if ((f > weight.startP) && (f <= (weight.startP + weight.proba)))
                {
                    //Debug.Log("< --- Spawn f " + f);
                    Spawn(weight.g, false);
                    //break;
                }
            }

        }


        /// <summary>
        /// Check if there is at least one pair available on scene.
        /// </summary>
        /// <returns></returns>
        private bool canMakeAPair()
        {

            FidgiObjectScript[] items = FindObjectsOfType<FidgiObjectScript>(true) as FidgiObjectScript[];

            for (int i = 0; i < items.Length; i++)
            {
                GameObject g = items[i].gameObject;
                FidgiObjectScript fidgi = g.GetComponent<FidgiObjectScript>();
                if (fidgi == null) continue;

                // Dont care about objects not on scene
                if (g.activeSelf == false) continue;

                // --- Don't care about viruses
                if (fidgi.type == FidgiObjectScript.ObjectType.Virus) continue;
                if (fidgi.type == FidgiObjectScript.ObjectType.Combined) continue;

                for (int j = i + 1; j < items.Length; j++)
                {
                    GameObject g1 = items[j].gameObject;
                    FidgiObjectScript fidgi1 = g1.GetComponent<FidgiObjectScript>();

                    if (g1.activeSelf == false) continue;
                    if (fidgi1.type == FidgiObjectScript.ObjectType.Virus) continue;
                    if (fidgi1.type == FidgiObjectScript.ObjectType.Combined) continue;

                    if (fidgi.matchName == fidgi1.matchName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Send an audio Help message
        /// </summary>
        /// <returns></returns>
        private IEnumerator HelpMessage()
        {
            yield return new WaitForSeconds(delayFirstHelpMessage + rand());

            while (true)
            {
                yield return new WaitForSeconds(delayHelpMessageInterval + rand());

                // check give help on "you can put on SSD"
                if ((combinedOnScene == true) && (SSDEmpty()))
                {
                    ambiantVoice.HelpSSD();
                }


                // check give help on "you can make a pair"
                if ((combinedOnScene == false) && (canMakeAPair()))
                {
                    // Debug.Log("Voice help can make a pair");
                    ambiantVoice.HelpPairing();
                }

            }

        }

    }
}