using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace GameManager
{

    [AddComponentMenu("Fidgi GameManager virus spawner")]
    public class VirusManager : SpawnManager
    {

        [SerializeField, Tooltip("The virus Gameobject")]
        public GameObject virusSource = null;

        // Start is called before the first frame update
        public override void Start()
        {

            if (virusSource == null)
            {
                Debug.LogError("Need a virus object");
            }

            base.Start();
        }


        protected override void CheckIfNeedToSpawn()
        {
            Debug.Log("Check Spawn virus");

            // --- Get all virus on scene
            VirusAI[] items = FindObjectsOfType<VirusAI>(true) as VirusAI[];

            List<GameObject> viruses = new List<GameObject>();
            for (int i = 0; i < items.Length; i++)
            {
                GameObject g = items[i].gameObject;
                if (g.activeSelf == true) viruses.Add(g);
            }

            // --- Too much viruses ---
            if (viruses.Count >= globalVariables.maxNumberOfViruses) return;

            Spawn( virusSource );

        }

    }
}
