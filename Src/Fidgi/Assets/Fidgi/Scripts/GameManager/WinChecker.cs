using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameManager
{
    /// <summary>
    /// Check if the the user win and manage tablet
    /// </summary>
    [AddComponentMenu("Fidgi win checker")]

    public class WinChecker : ManagerOfGame
    {


        [SerializeField, Range(1f, 5.0f), Tooltip("Delay beetween 2 check of success")]
        public float checkDelay = 1.3f;

        private ExcelCell[] excelCells = new ExcelCell[8];


        public override void Start()
        {
            base.Start();

            // --- Select excel cells to check if win --
            for (int i = 0; i < 8; i++)
            {
                string cellName = "Cell" + i;
                GameObject cell = GameObject.Find(cellName);
                if (!cell)
                {
                    Debug.LogError("No excel cell " + cellName + " found !");
                }
                ExcelCell c = cell.GetComponent<ExcelCell>();
                if (!c) Debug.LogError("Excel cell " + cellName + " is not excel ??");
                excelCells[i] = c;

            }

            StartCoroutine(CheckIfSucceed());
            globalVariables.startGame();

            tabletText.write(globalVariables.numberOfCellToFill + "\r\ncombinaisons\r\nà\r\nsauvegarder");

        }


        IEnumerator CheckIfSucceed()
        {
            // --- sleep close to 2 min before starting
            // yield return new WaitForSeconds(202);

            while (true)
            {
                yield return new WaitForSeconds(checkDelay);
                int count = 0;
                int year = 0;
                for (int i = 0; i < globalVariables.numberOfCellToFill; i++)
                {
                    int y = excelCells[i].getObjectDate();
                    if (y == -1) continue;

                    if (y >= year)
                    {
                        count++;
                        year = y;
                        if (count >= globalVariables.numberOfCellToFill)
                        {

                            tabletText.write("Sauvegarde\r\nen cours\r\n...");

                            float gameTime = Time.time - globalVariables.startTime;
                            float remainingTime = globalVariables.gameDuration - gameTime;
                            Scores.addToScore(100);
                            Scores.addToScore(Mathf.RoundToInt(remainingTime));

                            // Animation for cells
                            for (int j = 0; j < globalVariables.numberOfCellToFill; j++)
                            {
                                excelCells[j].AnimCell();
                            }

                            Scores.currentUserPair = count;
                            if (winAndLostSound) winAndLostSound.Win();

                            yield return new WaitForSeconds(2f);
                            if (sceneSwitcher) sceneSwitcher.GotoMenuScene();
                            yield break;
                        }
                        Scores.currentUserPair = count;
                        tabletText.write((globalVariables.numberOfCellToFill - count) + "\r\ncombinaisons\r\nà\r\nsauvegarder");
                    }
                    else
                    {
                        tabletText.write("Erreur\r\nordre\r\nchronologique");
                        break;
                    }
                }
            }
        }





    }
}