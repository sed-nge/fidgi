using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace GameManager
{

    public abstract class SpawnManager : MonoBehaviour
    {

        [SerializeField, Range(1f, 10.0f), Tooltip("Delay beetween 2 checks")]
        public float checkDelay = 2f;

        [SerializeField, Tooltip("Delay beetween 2 spawn")]
        public float spawnInterval = 3f;

        // The delay to wait before starting to check
        [SerializeField, Tooltip("Delay before the first check")]
        public float startDelay = 5f;

        [SerializeField, Range(0, 7), Tooltip("Number of slots to keep free")]
        public int numberOfSlotsToKeepFree = 3;

        private float lastSpawnTime = 0f;
        protected float randomMax = 10f;

        private GameObject[] respawnAreas = null;


        protected AmbiantVoice ambiantVoice;


        // Start is called before the first frame update
        public virtual void Start()
        {
            lastSpawnTime = 0f;
            respawnAreas = GameObject.FindGameObjectsWithTag("Respawn");
            if (respawnAreas.Length == 0)
            {
                Debug.LogError("No respawn area available (tag Respawn)");
            }


            AmbiantVoice[] s = FindObjectsOfType(typeof(AmbiantVoice)) as AmbiantVoice[];
            if (s.Length != 1)
            {
                Debug.LogError("More than one AmbiantVoice in the scene ;");

            }
            else
            {
                ambiantVoice = s[0];
            }

            StartCoroutine(CheckAndSpawn());



        }

    protected float rand() {
        return Random.Range(0f, randomMax);
    }

        protected virtual IEnumerator CheckAndSpawn()
        {
            yield return new WaitForSeconds(startDelay + rand());
            while (true)
            {
                yield return new WaitForSeconds(checkDelay + rand());

                // --- some virus have been spawned not so far
                if ((Time.time - lastSpawnTime) < spawnInterval) continue;

                CheckIfNeedToSpawn();
            }
        }

        protected virtual void CheckIfNeedToSpawn()
        {

        }



        /// <summary>
        /// Select a randomized free spawn Area
        /// </summary>
        /// <returns>GameObject or null</returns>
        private GameObject GetAvailableSpawnArea()
        {
            List<GameObject> availableRespawnAreas = new List<GameObject>();

            // find an non empty spawn area
            for (int i = 0; i < respawnAreas.Length; i++)
            {
                GameObject r = respawnAreas[i];
                XRSocketInteractor sp = r.GetComponent<XRSocketInteractor>();
                if (sp == null) continue;
                if (sp.selectTarget != null) continue;
                availableRespawnAreas.Add(r);
            }

            if (availableRespawnAreas.Count <= numberOfSlotsToKeepFree) return null;
            return availableRespawnAreas[Random.Range(0, availableRespawnAreas.Count)];
        }



        protected void Spawn(GameObject src, bool instanciate = true)
        {

            if (src == null) return;
            GameObject spawnArea = GetAvailableSpawnArea();
            if (spawnArea == null) return;

            // Debug.Log(">>> Spawn " + src.name);
            GameObject newObj;

            lastSpawnTime = Time.time;

            if (instanciate == true)
            {
                newObj = Instantiate(src.gameObject);
                newObj.name = src.gameObject.name + "+";
            }
            else
            {
                newObj = src;
            }

            FidgiObjectScript f = newObj.GetComponent<FidgiObjectScript>();
            f.InfosVisible = false;

            // --- Place the object in the center of the area ---
            newObj.transform.position = spawnArea.transform.position;
            newObj.transform.rotation = spawnArea.transform.rotation;
            newObj.SetActive(true);
        }

    }
}