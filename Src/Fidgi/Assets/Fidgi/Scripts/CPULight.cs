using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


[AddComponentMenu("Fidgi CPU Light Management")]

/// <summary>
/// Manage the display of a video / gameobject / Slideshow
/// </summary>
public class CPULight : MonoBehaviour
{


    [SerializeField, Tooltip("Lights elements in order")]
    public List<GameObject> lights = new List<GameObject>();


    private IEnumerator anim;

    private void Start()
    {

        Abort();
    }



    public void rotate(float speed)
    {
        Abort();
        anim = Rotation(speed);
        StartCoroutine(anim);
    }


    public void yeah(float speed)
    {
        Abort();
        anim = yeahGo(speed);
        StartCoroutine(anim);

    }


    public void serpent(int count, float speed)
    {
        Abort();
        anim = Serpent(count, speed);
        StartCoroutine(anim);
    }

    public IEnumerator Serpent(int count, float speed)
    {
        for (int i = 0; i < count; i++)
        {
            yield return OneRotate( speed );
        }
    }


    public IEnumerator OneRotate(float speed)
    {
        for (int i = 0; i < lights.Count; i++)
        {
            SwitchLightOn(lights[i]);
            if (i > 0) SwitchLightOff(lights[i - 1]);
            yield return new WaitForSeconds(speed);
        }
        SwitchLightOff(lights[lights.Count - 1]);
        yield return new WaitForSeconds(speed);
    }


    public IEnumerator yeahGo(float speed)
    {
        yield return Rotation(speed);
        yield return blinkAll(speed);
        yield return blinkAll(speed);
        Abort();
    }


    public IEnumerator Rotation(float speed)
    {
        for (int i = 0; i < lights.Count; i++)
        {
            SwitchLightOn(lights[i]);
            yield return new WaitForSeconds(speed);
        }
        yield return new WaitForSeconds(speed * 2);
        for (int i = 0; i < lights.Count; i++)
        {
            SwitchLightOff(lights[i]);
            yield return new WaitForSeconds(speed);
        }
    }


    public void Abort()
    {

        if (anim != null)
        {
            StopCoroutine(anim);
            anim = null;
        }

        for (int i = 0; i < lights.Count; i++)
        {
            SwitchLightOff(lights[i]);
        }

    }

    IEnumerator blinkAll(float speed)
    {
        Debug.Log("Blink On");
        for (int i = 0; i < lights.Count; i++)
        {
            SwitchLightOn(lights[i]);
        }
        yield return new WaitForSeconds(speed);
        Debug.Log("Blink Off");
        for (int i = 0; i < lights.Count; i++)
        {
            SwitchLightOff(lights[i]);
        }
    }

    IEnumerator blinkDelay(float delay)
    {
        float t = Time.time;
        while ((Time.time - t) < delay)
        {
            yield return blinkAll(0.2f);
            yield return new WaitForSeconds(0.2f);
        }
    }


    private void SwitchLightOff(GameObject o)
    {
        Renderer render = o.GetComponent<Renderer>();
        if (!render) return;

        Material[] mats = render.materials;
        foreach (Material m in mats)
        {
            m.DisableKeyword("_EMISSION");
        }
    }


    private void SwitchLightOn(GameObject o)
    {
        Renderer render = o.GetComponent<Renderer>();
        if (!render) return;

        Material[] mats = render.materials;
        foreach (Material m in mats)
        {
            m.EnableKeyword("_EMISSION");
        }
    }


}
