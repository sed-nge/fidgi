using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateItem : MonoBehaviour
{
    //Vector3 fanSpeed;
    //public Vector3 rotationSpeed = new Vector3(0, 0, 5);
    // Update is called once per frame

    private static Vector3 itemRotationSpeed = new Vector3(0, 5, 0);
    private static Vector3 itemRotation = new Vector3(0, 0, 0);


    void Update()
    {
        // Change the speed according to the cpuUsage
        itemRotationSpeed = new Vector3(0, globalVariables.cpuUsage / 100f * 5.0f, 0);

        // Set the transformation
        itemRotation += itemRotationSpeed;
        transform.localEulerAngles = itemRotation;
    }
}