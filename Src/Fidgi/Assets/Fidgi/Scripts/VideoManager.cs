using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

// This script won't work without a VideoPlayer present,
// so let's ask Unity to enforce that relationship for us.
[RequireComponent(typeof(VideoPlayer))]

// The C# convention for class names is PascalCase.
public class VideoManager : MonoBehaviour
{

    // Don't create/size the Array in Start() - that makes an empty
    // array, discarding the clips you assigned in the Inspector.
    public VideoClip[] videoList = new VideoClip[8];

    private VideoPlayer vp;

    void Start()
    {
        vp = gameObject.GetComponent<VideoPlayer>();
    }

    // Call this method when it's time to play a particular video.
    // Pass a number from 0 to n-1 inclusive to choose which video.
    public VideoManager(int id)
    {
        // To be safe, let's bounds-check the ID 
        // and throw a descriptive error to catch bugs.
        if (id < 0 || id >= videoList.Length)
        {
            Debug.LogErrorFormat(
               "Cannot play video #{0}. The array contains {1} video(s)",
                                   id, videoList.Length);
            return;
        }

        // If we get here, we know the ID is safe.
        // So we assign the (id+1)th entry of the vids array as our clip.
        vp.clip = videoList[id];

        vp.Play();
    }
}