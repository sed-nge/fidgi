﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[AddComponentMenu("Fidgi Bravo 3D Text")]
public class Bravo3DText : MonoBehaviour
{

    [SerializeField, Tooltip("List 3D Text for Not so bad")]
    public List<GameObject> level0 = new List<GameObject>();

    [SerializeField, Tooltip("List 3D Text for Good")]
    public List<GameObject> level1 = new List<GameObject>();

    [SerializeField, Tooltip("List 3D Text for Very Good")]
    public List<GameObject> level2 = new List<GameObject>();
    [SerializeField, Tooltip("List 3D Text for Perfect")]
    public List<GameObject> level3 = new List<GameObject>();

    [SerializeField, Tooltip("minimum Duration")]
    public float minimumDuration = 10f;

    [SerializeField, Tooltip("Distance from the player")]
    public float distance = 3f;
    [SerializeField, Tooltip("Height")]
    public float height = 4f;

    [SerializeField, Tooltip("Speed ")]
    public float speed = 1f;

    private IEnumerator currentShow = null;

    private List<GameObject> level;
    private GameObject word = null;
    private float showDuration = 0f;

    private void Start()
    {
        // --- Not comming back from the XRROom scene
        if (Scores.currentUserScore == -1) return;

        // --- compute the show duration from the player result
        if (Scores.currentUserPair > 0)
        {
            showDuration = ((float)Scores.currentUserPair / (float)globalVariables.numberOfCellToFill) * 30f + minimumDuration;
            int l = Mathf.CeilToInt(((float)Scores.currentUserPair / (float)globalVariables.numberOfCellToFill) * 4f);

            switch (l)
            {
                case 0:
                    level = level0;
                    break;
                case 1:
                    level = level1;
                    break;
                case 2:
                    level = level2;
                    break;
                default:
                    level = level3;
                    break;
            }
            currentShow = show(showDuration);
            StartCoroutine(currentShow);
        }


    }

    IEnumerator show(float duration)
    {
        showWord();
        yield return new WaitForSeconds(duration);
        stopShowWord();
        StopCoroutine(currentShow);
    }


    private void Update()
    {
        if (!word) return;

        Vector3 destination = Camera.main.transform.position + Camera.main.transform.forward * distance;
        if (destination.y < height) destination.y = height;

        float step = speed * Time.deltaTime; // calculate distance to move

        transform.position = Vector3.MoveTowards(transform.position, destination, step);

        Vector3 lookat = Camera.main.transform.position;
        if (lookat.y < height) lookat.y = height;

        transform.LookAt(lookat);
    }

    public void showWord()
    {
        // Set the word in front of the camera
        // --- Check player direction related to the object
        Vector3 destination = Camera.main.transform.position + Camera.main.transform.forward * distance;
        if (destination.y < height) destination.y = height;
        transform.position = destination;

        Vector3 lookat = Camera.main.transform.position;
        if (lookat.y < height) lookat.y = height;

        transform.LookAt(lookat);

        word = level[Random.Range(0, level.Count)];
        if (!word) return;
        word.SetActive(true);
    }


    public void stopShowWord()
    {
        if (word) word.SetActive(false);
        word = null;
    }


}