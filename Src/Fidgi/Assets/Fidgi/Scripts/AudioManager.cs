using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

// This script won't work without a AudioSource present,
// so let's ask Unity to enforce that relationship for us.
[RequireComponent(typeof(AudioSource))]

// The C# convention for class names is PascalCase.
public class AudioManager : MonoBehaviour
{

    // Don't create/size the Array in Start() - that makes an empty
    // array, discarding the clips you assigned in the Inspector.
    public AudioClip[] audioList = new AudioClip[19];

    private AudioSource audioPlayer;

    void Start()
    {
        audioPlayer = gameObject.GetComponent<AudioSource>();
    }

    // Call this method when it's time to play a particular video.
    // Pass a number from 0 to n-1 inclusive to choose which video.
    public AudioManager(int id)
    {
        // To be safe, let's bounds-check the ID 
        // and throw a descriptive error to catch bugs.
        if (id < 0 || id >= audioList.Length)
        {
            Debug.LogErrorFormat(
               "Cannot play video #{0}. The array contains {1} video(s)",
                                   id, audioList.Length);
            return;
        }

        // If we get here, we know the ID is safe.
        // So we assign the (id+1)th entry of the vids array as our clip.
        audioPlayer.clip = audioList[id];

        audioPlayer.Play();
    }
}