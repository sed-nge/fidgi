using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class endCheckContent : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.Log("Anim Finish");
        GameObject cell = animator.gameObject;

        // --- check if the object is acceptable (is a fidgi object and is a combined) 
        // --- otherwise drop it
        GameObject target = cell.GetComponent<XRSocketInteractor>().selectTarget.gameObject;
        if (!target) return;
        FidgiObjectScript fidgi = target.GetComponent<FidgiObjectScript>();
        if (!fidgi)
        {
                    Debug.Log("Not a good object");
//            ThrowObject(target);
            return;
        }
        if ( ! fidgi.isCombined() ) {
                                Debug.Log("Not a combined object");

        }


    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
