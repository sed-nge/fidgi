﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Transform objects into a new one
*/


public class transformObject : MonoBehaviour
{
    public bool transformationInProgress = false;

    public GameObject stickTemplate;
    public GameObject fireWork;   // Video to play when things matching
    private Animator iconAnimator;

    // Start is called before the first frame update
    void Start()
    {
        foreach (UnityEngine.Object go in Resources.FindObjectsOfTypeAll(typeof(UnityEngine.Object)) as UnityEngine.Object[])
        {
            GameObject cGO = go as GameObject;
            if (cGO != null && cGO.name == "stick")
            {
                stickTemplate = cGO;
                // break;
            }
            
            if (cGO != null && cGO.name == "FireWork")
            {
                fireWork = cGO;
                // break;
            }
        }

        iconAnimator = this.getObjectIconAnimator(this.gameObject);
    }

    private GameObject getIconGameObject(GameObject g)
    {
        for (int i = 0; i < g.transform.childCount; i++)
        {
            GameObject child = g.transform.GetChild(i).gameObject;
            if (child.tag == "icon")
            {
                return child;
            }
        }
        return null;

    }

    private Animator getObjectIconAnimator(GameObject g)
    {
        GameObject child = this.getIconGameObject(g);
        if (child == null) return null;
        return child.GetComponent<Animator>();
    }


    // Update is called once per frame
    void Update()
    {

    }


    private void OnCollisionEnter(Collision other)
    {
        Debug.Log("Collision with " + other.gameObject.name);

        // check if the collision is with a invention
        // if (other.gameObject.tag != "invention") return;
        if (this.gameObject.name+"_invention" != other.gameObject.name) return;
        
        // check if the collision is above
        if (other.contacts[0].normal.y != -1) return;

        // Check if both invention object is released (not in and or mouse down)
        DragDropMouse ddscript = other.gameObject.GetComponent<DragDropMouse>();
        if (ddscript && ddscript.isSelected == true) return;

        // Stop the iconAnimation if needed
        iconAnimator.SetBool("rotate", false);

        Animator otherIconAnimator = this.getObjectIconAnimator(other.gameObject);
        if (otherIconAnimator == null)
        {
            Debug.LogError("cannot found other Animator");
            return;
        }
        otherIconAnimator.SetBool("rotate", false);

        // Set transform script for both gameObject
        transformationInProgress = true;
        //transformObject transformScript = other.gameObject.GetComponent<transformObject>();
        //transformScript.transformationInProgress = true;

        GameObject otherIcon = this.getIconGameObject( other.gameObject );
        GameObject myIcon = this.getIconGameObject( this.gameObject );
        

        other.gameObject.SetActive(false);
        this.gameObject.SetActive(false);

        // Create the new Object, add icon objects.
        GameObject stick = Instantiate(stickTemplate, transform.position, Quaternion.identity);

        otherIcon.transform.SetParent( stick.transform );
        myIcon.transform.SetParent( stick.transform );
        otherIcon.transform.position = myIcon.transform.position;
        stick.SetActive(true);

        //otherIconAnimator.SetBool("goToCharacter", true);
        
        // Awarding with a nice firework on the Egg

        PlayFireworkOn(otherIcon);

    }


    public void PrintEvent(string s)
    {
        Debug.Log("PrintEvent: " + s + " called at: " + Time.time);
    }

    private void PlayFireworkOn(GameObject gameObject)
    {
        var videoPlayer = fireWork.GetComponent<UnityEngine.Video.VideoPlayer>();

        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.MaterialOverride;
        videoPlayer.targetMaterialRenderer = gameObject.GetComponent<Renderer>();
        videoPlayer.targetMaterialProperty = "_MainTex";

        fireWork.SetActive(true);
        videoPlayer.Play();

    }
}
