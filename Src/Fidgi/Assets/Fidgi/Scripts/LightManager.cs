using UnityEngine;
using System.Collections.Generic;
using System.Collections;


[RequireComponent(typeof(Light))]
[AddComponentMenu("Fidgi Light manager")]




/// <summary>
/// Manage light emission and color.
/// modify the light
/// </summary>
public class LightManager : MonoBehaviour
{


    [SerializeField, Tooltip("Color below 10%")]
    public Color colorAlert = Color.red;

    [SerializeField, Tooltip("Audio Sound for switch")]
    public AudioClip clicNoise = null;


    private IEnumerator constantBlinking = null;

    private AudioSource source = null;

    private Light l;
    float originalIntensity = 0f;
    Color originalColor;

    public virtual void Start()
    {
        l = gameObject.GetComponent<Light>();
        originalIntensity = l.intensity;
        originalColor = l.color;

        constantBlinking = UpdateColor();
        StartCoroutine(constantBlinking);

        source = gameObject.GetComponent<AudioSource>();
        if ( ! source ) {
            Debug.Log("Light "+gameObject.name+" doesnt have a audio Source for click sound");
        }
    }

    private void playSound() {
        if ( ! source ) return;
        if ( ! clicNoise ) return;
        source.PlayOneShot( clicNoise );
    }

    public void stopEmiting()
    {
        l.intensity = 0;
        playSound();
    }
    public void startEmiting()
    {
        l.intensity = originalIntensity;
        playSound();
    }
    public void blink(float duration)
    {
        StopCoroutine(constantBlinking);
        startEmiting();
        StartCoroutine(Blink(duration));
    }


    public void ChangeColor(Color c)
    {
        l.color = c;
    }





    IEnumerator Blink(float duration)
    {
        Debug.Log("Start Blink for " + duration + " seconds");
        float t = Time.time;
        float maxBlickOff = 0.2f;
        float maxBlickOOn = 1f;
        float rtime = Time.time - t;
        while (rtime < duration)
        {
            stopEmiting();
            yield return new WaitForSeconds(Random.Range(0.1f, maxBlickOff));
            startEmiting();
            yield return new WaitForSeconds(Random.Range(0.1f, maxBlickOOn));
            rtime = Time.time - t;
            maxBlickOff = (rtime / duration) + 0.2f;
            maxBlickOOn = ((duration - rtime) / duration) + 0.2f;

        }
        stopEmiting();

    }


    IEnumerator UpdateColor()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            float batteryLevel = globalVariables.getBatteryLevel();
            float t = (globalVariables.maxBatteryLevel - batteryLevel) / globalVariables.maxBatteryLevel;
            l.color = Color.Lerp( originalColor, colorAlert, t);
        }
    }


}
