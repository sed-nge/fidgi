using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.Interaction.Toolkit;

[AddComponentMenu("Fidgi disable objects")]


public class DisableUse : MonoBehaviour
{

    [SerializeField, Tooltip("Shader to use when disabled")]

    public Shader disabledShader = null;

    [SerializeField, Tooltip("Layer when disabled")]

    public string LayerName = "NoInteraction";


    private List<Shader> materialShader = new List<Shader>();

    private int normalLayer;
    private int disabledLayer;

    private bool particulesRunning = false;
    private bool isDisabled = false;

    void Start()
    {

        disabledLayer = LayerMask.NameToLayer(LayerName);

        // --- Save the current Layer to get it back when re-enable
        // normalLayer = gameObject.layer;
    }

    public void Enable()
    {

        if (isDisabled == true)
        {
            gameObject.layer = normalLayer;

            // --- Re enable the material ---
            Renderer ObjectRenderer = GetComponent<Renderer>();
            if (ObjectRenderer)
            {
                Material[] mats = ObjectRenderer.materials;
                if (materialShader.Count == mats.Length)
                {
                    for (int i = 0; i < mats.Length; i++)
                    {
                        mats[i].shader = materialShader[i];
                    }
                }
                else
                {
                    Debug.Log("Some error count of materials for " + gameObject.name);
                }
            }
            isDisabled = false;
        }

        // --- restart particules if exists
        ParticleSystem p = GetComponent<ParticleSystem>();
        if (p)
        {
            if (particulesRunning) p.Play();
        }

        // --- enable scripts
        SetEnableScripts(true);

        // --- Do it for childrens
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform t = transform.GetChild(i);
            GameObject sub = t.gameObject;
            DisableUse di = sub.GetComponent<DisableUse>();
            if (di) di.Enable();
        }

    }


    private void SetEnableScripts(bool enabled)
    {
        // --- Scripts to enable 
        FidgiObjectScript f = GetComponent<FidgiObjectScript>();
        if (f) f.enabled = enabled;
        XRSocketInteractor s = GetComponent<XRSocketInteractor>();
        if (s) s.enabled = enabled;
        ExcelCell e = GetComponent<ExcelCell>();
        if (e) e.enabled = enabled;
        DisplayScript d = GetComponent<DisplayScript>();
        if (d) d.enabled = enabled;
        Copy c = GetComponent<Copy>();
        if (c) c.enabled = enabled;

    }

    private bool isinException(string[] exeptionNames)
    {
        for (int i = 0; i < exeptionNames.Length; i++)
        {
            if (exeptionNames[i] == gameObject.name) return true;
        }
        return false;
    }

    public void Disable(string[] exeptionNames)
    {

        if (disabledShader == null) return;

        if (isinException(exeptionNames) == true)
        {
            Enable();
            return;
        }

        if (isDisabled == false)
        {
            // --- Save the current Layer to get it back when re-enable
            normalLayer = gameObject.layer;

            // --- change the layer (to make grabable objects ungrabable)
            gameObject.layer = disabledLayer;

            // --- Set grey ---
            Renderer ObjectRenderer = GetComponent<Renderer>();
            if (ObjectRenderer)
            {
                Material[] mats = ObjectRenderer.materials;
                materialShader.Clear();
                for (int i = 0; i < mats.Length; i++)
                {
                    materialShader.Add(mats[i].shader);
                    mats[i].shader = disabledShader;
                }
            }

            // --- pause particules if exists
            ParticleSystem p = GetComponent<ParticleSystem>();
            if (p)
            {
                particulesRunning = p.isPlaying;
                p.Stop();
            }

            isDisabled = true;
        }

        // --- Scripts to disable 
        SetEnableScripts(false);



        // --- Do it for childrens
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform t = transform.GetChild(i);
            GameObject sub = t.gameObject;
            DisableUse di = sub.GetComponent<DisableUse>();
            if (!di) sub.AddComponent<DisableUse>();
            di = sub.GetComponent<DisableUse>();
            di.disabledShader = disabledShader;
            di.LayerName = LayerName;
            di.Disable(exeptionNames);
        }

    }

}