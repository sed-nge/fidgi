using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class UserScore
{
    public string name;
    public int score;
    public DateTime date;
    public UserScore(string n, int s)
    {
        name = n;
        score = s;
        date = DateTime.Now;
    }
}


public class Scores : MonoBehaviour
{

    public static List<UserScore> hallOfFame = new List<UserScore>();

    public static int maxUsers = 10;
    public static int currentUserScore =  -1;
    public static int currentUserPair = -1;


    public static void addToHallOfFame(string name, int score)
    {
        List<UserScore> hall = new List<UserScore>();

        UserScore n = new UserScore(name, score);

        for (int i = 0; i < hallOfFame.Count; i++)
        {
            hall.Add(hallOfFame[i]);
        }

        hallOfFame.Clear();
        bool added = false;
        int count=0;
        for (int i = 0; i < hallOfFame.Count; i++)
        {
            if ( count >= maxUsers ) continue;
            if ((hall[i].score < score) && (!added))
            {
                hallOfFame.Add(n);
                count++;
                added = true;
            }            
            hallOfFame.Add(hall[i]);
            count++;
        }
    }


    public static void addToScore( int num ) {
        currentUserScore+=num;
//        Debug.Log("New score "+currentUserScore);
    }
    public static void resetScore() {
        currentUserScore=0;
        currentUserPair = 0;
    }
    public static void saveScore( string name ) {
        addToHallOfFame( name, currentUserScore );
    }

    public static void clear() {
        resetScore();
        hallOfFame.Clear();
    }

}

//write static in front of the variable, then in the other script you can access by typing
//origscriptname.variablename
