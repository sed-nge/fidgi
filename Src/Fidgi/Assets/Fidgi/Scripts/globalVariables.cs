using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class globalVariables : MonoBehaviour
{
    // cpu level -- minimum = 0 , maximum = 100;
    public static float cpuUsage = 5f;
    public static float maxBatteryLevel = 0.4f;
    public static float startTime;

    public static float gameDuration = 12 * 60; // maximal duration of the game, in seconds
    public static int minimumCompletableObjects = 3;
    public static int numberOfCellToFill = 4;
    public static int maxNumberOfViruses = 3;


    public static void startGame() {
        startTime = Time.time;
    }

    /**
     * getters / setters
     */
    public static float getCpuUsage()
    {
        return cpuUsage;
    }
    public static void setCpuUsage(float newValue)
    {
        if (newValue > 100f)
        {
            cpuUsage = 100.0f;
        }
        else
        {
            if (newValue < 1f)
            {
                cpuUsage = 5f;
            }
            else
            {
                cpuUsage = newValue;
            }
        }
    }
    public static void incrementCpuUsage(float increment)
    {
        setCpuUsage(cpuUsage + increment);
    }
    public static void decrementCpuUsage(float increment)
    {
        setCpuUsage(cpuUsage - increment);
    }

/// <summary>
/// Return the battery level between 0f - 1f according to the time
/// </summary>
/// <returns></returns>
    public static float getBatteryLevel()
    {
            float b =  (gameDuration - (Time.time - startTime)) / gameDuration * maxBatteryLevel ;
            if ( b < 0 ) b = 0;
            return b;
    }

    public static void decrementTimeLevel(float seconds)
    {
        gameDuration = gameDuration - seconds;
    }




}

//write static in front of the variable, then in the other script you can access by typing
//origscriptname.variablename
