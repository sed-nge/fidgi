using UnityEngine;
using System.Collections.Generic;
using System.Collections;


[AddComponentMenu("Fidgi Color Move")]

/// <summary>
/// change slowly color
/// </summary>
public class ColorMove : MonoBehaviour
{


    [SerializeField, Tooltip("Colors")]
    public List<Color> colors = new List<Color>();

    [SerializeField, Tooltip("duration")]
    public float duration = 60f;

    private Material mat;

    public virtual void Start()
    {


        Renderer render = gameObject.GetComponent<Renderer>();
        mat = render.material;
        colors.Add( mat.GetColor("_BaseColor") );


        if (colors.Count > 1) StartCoroutine(UpdateColor());
    }

    IEnumerator UpdateColor()
    {
        int index = 0;
        while (true)
        {
            if (index == colors.Count) index = 0;
            Color dst = colors[index];
            Color startColor = mat.GetColor("_BaseColor");

            float startTime = Time.time;
            float t = startTime;
            while ((t - startTime) < duration)
            {
                t = Time.time;
                mat.SetColor("_BaseColor", Color.Lerp(startColor, dst, (t-startTime)/duration ));
                yield return new WaitForSeconds(0.1f);
            }
            index++;
            yield return new WaitForSeconds(1f);
        }
    }


}
