using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

[AddComponentMenu("Fidgi disable scene")]


public class DisableScene : MonoBehaviour
{

    [SerializeField, Tooltip("Shader to use when disabled")]

    public Shader disabledShader = null;

    [SerializeField, Tooltip("Layer when disabled")]

    public String LayerName = "NoInteraction";


    private List<Shader> materialShader = new List<Shader>();

    private int normalLayer;
    private int disabledLayer;

    void Start()
    {

        disabledLayer = LayerMask.NameToLayer(LayerName);

        // --- Save the current Layer to get it back when re-enable
        normalLayer = gameObject.layer;
    }
/* 
    IEnumerator waitAndStart()
    {
        while (true)
        {
            yield return new WaitForSeconds(2f);
            DisableAll(new string[] { "Cell3P" } );
            yield return new WaitForSeconds(2f);
            DisableAll(new string[] { "mug" } );
            yield return new WaitForSeconds(2f);
            EnableAll();
        }
    }
 */

    public void EnableAll()
    {

        DisableUse[] disableabe = FindObjectsOfType(typeof(DisableUse)) as DisableUse[];

        for (int i = 0; i < disableabe.Length; i++)
        {
            DisableUse di = disableabe[i];
            di.Enable();
        }
    }

    public void DisableAll( string[] exeptionNames )
    {
        DisableUse[] disableabe = FindObjectsOfType(typeof(DisableUse)) as DisableUse[];

        for (int i = 0; i < disableabe.Length; i++)
        {
            DisableUse di = disableabe[i];

            if (!di.disabledShader) di.disabledShader = disabledShader;
            di.LayerName = LayerName;
            di.Disable( exeptionNames );
        }
    }

}