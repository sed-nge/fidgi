using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(VoicePlayer))]
[AddComponentMenu("Fidgi Win and lost voices")]

public class WinAndLost : MonoBehaviour
{

    [SerializeField, Tooltip("Win messages")]
    public List<AudioClip> WinMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Lost messages")]
    public List<AudioClip> LostMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Abort messages")]
    public List<AudioClip> AbortMessages = new List<AudioClip>();

    private VoicePlayer player;

    // Start is called before the first frame update
    void Start()
    {
        player = gameObject.GetComponent<VoicePlayer>();

    }


    public void Lost() { player.abortCurrentSound(); player.PlayRandom(LostMessages, 0, 1); }
    public void Abort() { player.abortCurrentSound(); player.PlayRandom(AbortMessages, 0, 1); }
    public void Win() { player.abortCurrentSound(); player.PlayRandom(WinMessages, 0, 1); }



}
