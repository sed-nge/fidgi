using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(VoicePlayer))]
[AddComponentMenu("Fidgi Battery voices")]

public class BatteryVoice : MonoBehaviour
{

    [SerializeField, Tooltip("Message Battery per percent")]
    public List<AudioClip> BatteryMessage = new List<AudioClip>();


    private VoicePlayer player;

    // Start is called before the first frame update
    void Start()
    {
        player = gameObject.GetComponent<VoicePlayer>();
    }

    public void BatteryPercentVoice( int percent) {
        if ( percent >= BatteryMessage.Count ) return;
        AudioClip Sound = BatteryMessage[percent];
        if ( ! Sound ) return;
         player.abortCurrentSound(); 
         player.Play(Sound, 1); 
    }




}
