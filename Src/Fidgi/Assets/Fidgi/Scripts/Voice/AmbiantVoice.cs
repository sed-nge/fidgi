using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System.Collections;
using UnityEngine.Video;
using UnityEngine.AI;


[RequireComponent(typeof(VoicePlayer))]
[AddComponentMenu("Fidgi Ambiant voices")]
public class AmbiantVoice : MonoBehaviour
{

    [SerializeField, Tooltip("Welcome messages")]
    public List<AudioClip> Welcome = new List<AudioClip>();
    [SerializeField, Tooltip("Delay before start welcome messages")]
    public float WelcomeDelay = 5f;


    [SerializeField, Tooltip("Back from XRRoom messages")]
    public List<AudioClip> BackMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Delay before start back messages")]
    public float BackDelay = 2f;


    [SerializeField, Tooltip("Delay before another help messages")]
    public float HelpMessageDelay = 45f;

    [SerializeField, Tooltip("Help Paring Items")]
    public List<AudioClip> HelpPairingMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Help user reader Items")]
    public List<AudioClip> HelpReaderMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Help user use SSD")]
    public List<AudioClip> HelpSSDMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Help copy machine message")]
    public List<AudioClip> HelpCPUMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Help buttons machine message")]
    public List<AudioClip> HelpButtonMessages = new List<AudioClip>();


    private VoicePlayer player;
    private float lastHelpMessageTime = -1f;
    private bool mute = false;

    private GameObject cardReader = null;

    // Start is called before the first frame update
    void Start()
    {


        DisplayScript[] s = FindObjectsOfType(typeof(DisplayScript)) as DisplayScript[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one DisplayScript in the scene ;");
        }
        else
        {
            cardReader = s[0].gameObject;
        }


        player = gameObject.GetComponent<VoicePlayer>();
        Scene currentScene = SceneManager.GetActiveScene();

        if (cardReader != null) StartCoroutine(ModulateSoundVolume());

        if (currentScene.name == "MenuScene")
        {
            StartVoiceMenuSceenRoom();
        }
        else
        {
            StartVoiceXRroom();
        }


    }

    private void StartVoiceMenuSceenRoom()
    {

        if (Scores.currentUserScore == -1)
        {
            player.Play(Welcome, 0, WelcomeDelay);
        }
        else
        {
            int index = Mathf.CeilToInt(((float)Scores.currentUserPair / (float)globalVariables.numberOfCellToFill) * 4f);
            if (index >= BackMessages.Count) index = BackMessages.Count - 1;

            if ((index >= 0) && (BackMessages[index])) player.Play(BackMessages[index], BackDelay);
        }
    }
    private void StartVoiceXRroom()
    {
        player.Play(Welcome, 0, WelcomeDelay);
    }

    IEnumerator ModulateSoundVolume()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            XRSocketInteractor sp = cardReader.GetComponent<XRSocketInteractor>();
            if (sp == null)
            {
                mute = false;
                continue;
            }
            if (sp.selectTarget != null)
            {
                mute = true;
                continue;
            }
            // --- Mute ---
            mute = false;
        }
    }


    public void MuteOn()
    {
        //mute = true;
    }
    public void MuteOff()
    {
        //mute = false;
    }

    public bool isMuted()
    {
        return (mute || player.isPlaying());
    }


    public void PlayRandom(List<AudioClip> Sounds, float delay, float startDelay)
    {
        if (mute == true) return;
        //float t = Time.time;
        //if ((t - lastHelpMessageTime) <= HelpMessageDelay) return;
        player.PlayRandom(Sounds, delay, startDelay);
        //lastHelpMessageTime = t;
    }


    public void Play(AudioClip Sound, float startDelay)
    {
        if (mute == true) return;
        //float t = Time.time;
        //if ((t - lastHelpMessageTime) <= HelpMessageDelay) return;
        player.Play(Sound, startDelay);
        //lastHelpMessageTime = t;
    }


    public bool HelpPairing()
    {
        if (isMuted()) return false;
        player.PlayRandom(HelpPairingMessages, 0, 0);
        return true;
    }
    public void HelpReader()
    {
        if (mute == true) return;
        player.PlayRandom(HelpReaderMessages, 0, 0);
    }
    public bool HelpSSD()
    {
        if (isMuted()) return false;
        player.PlayRandom(HelpSSDMessages, 0, 0);
        return true;
    }
    public void HelpCPU()
    {
        if (mute == true) return;
        player.PlayRandom(HelpCPUMessages, 0, 0);
    }
    public void HelpGPU()
    {
        if (mute == true) return;
        player.PlayRandom(HelpReaderMessages, 0, 0);
    }
    public void HelpButton()
    {
        if (mute == true) return;
        player.Play(HelpButtonMessages, 0, 0);
    }




}
