using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(VoicePlayer))]
[AddComponentMenu("Fidgi Pairing voices")]

public class PairingVoice : MonoBehaviour
{

    [SerializeField, Tooltip("Wrong pair messages")]
    public List<AudioClip> WrongPairMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Good pair messages")]
    public List<AudioClip> GoodPairMessages = new List<AudioClip>();

    private VoicePlayer player;

    // Start is called before the first frame update
    void Start()
    {
        player = gameObject.GetComponent<VoicePlayer>();

    }


    public void WrongPair() { player.abortCurrentSound(); player.PlayRandom(WrongPairMessages, 0, 1); }
    public void GoodPair() { player.abortCurrentSound(); player.PlayRandom(GoodPairMessages, 0, 1); }


}
