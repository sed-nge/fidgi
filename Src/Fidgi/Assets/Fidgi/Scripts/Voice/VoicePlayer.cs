using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Fidgi Voice player")]

public class VoicePlayer : MonoBehaviour
{

    [SerializeField, Tooltip("Minimum pause time beetween messages")]
    public float PauseDelay = 1f;

    private AudioSource audioPlayer;
    private IEnumerator playSound = null;

    private bool mute = false;

    // Start is called before the first frame update
    void Start()
    {
        audioPlayer = gameObject.GetComponent<AudioSource>();

    }

    public void MuteOn() {
        mute = true;
    }
    public void MuteOff() {
        mute = false;
    }

    public bool isPlaying() {
        return(playSound != null);
    }


    public IEnumerator waitForSoundToFinish()
    {
        while (playSound != null)
        {
            yield return new WaitForSeconds(0.2f);
        }
    }

    public void abortCurrentSound()
    {
        if (playSound == null) return;
        StopCoroutine(playSound);
        playSound = null;
    }

    public void Play(List<AudioClip> Sounds, float delay, float startDelay)
    {

        // --- check if currently talking ---
        if (playSound != null)
        {
            Debug.Log("Cannot talk, a voice is currently talking");
            return;
        }
        if ( mute == true ) return;
        playSound = PlaySequencialSoundDelay(Sounds, delay, startDelay);
        StartCoroutine(playSound);
    }

    public void Play(AudioClip sound, float startDelay)
    {
        // --- check if currently talking ---
        if (playSound != null)
        {
            Debug.Log("Cannot talk, a voice is currently talking");
            return;
        }

        if ( mute == true ) return;
        playSound = PlaySoundRoutine(sound, startDelay);
        StartCoroutine(playSound);
    }


    public void PlayRandom(List<AudioClip> Sounds, float delay, float startDelay)
    {
        AudioClip sound = selectRandomSound(Sounds);
        if (!sound) return;

        // --- check if currently talking ---
        if (playSound != null)
        {
            Debug.Log("Cannot talk, a voice is currently talking");
            return;
        }
        if ( mute == true ) return;
        playSound = PlaySoundRoutine(sound, startDelay);
        StartCoroutine(playSound);
    }


    private AudioClip selectRandomSound(List<AudioClip> Sounds)
    {
        if (Sounds.Count == 0) return null;
        AudioClip sound = null;
        int count = 0;
        while ((sound == null) || (count > 10))
        {
            int i = Random.Range(0, Sounds.Count);
            sound = Sounds[i];
            count++;
        }
        if (!sound)
        {
            Debug.Log("No sound found in list");
            return null;
        }
        return sound;
    }

    public IEnumerator PlayRandomSoundRoutine(List<AudioClip> Sounds, float startDelay)
    {
        AudioClip sound = selectRandomSound(Sounds);
        yield return PlaySoundRoutine(sound, startDelay);
    }


    public IEnumerator PlaySoundRoutine(AudioClip sound, float startDelay)
    {
        if (sound)
        {
            Debug.Log("LP: VoicePlayer.PlaySound(" + sound.name + ")");
            yield return new WaitForSeconds(startDelay);
            audioPlayer.Stop();
            audioPlayer.clip = sound;
            audioPlayer.loop = false;
            audioPlayer.Play();
            yield return new WaitForSeconds(sound.length + PauseDelay);

            // --- ugly ? stop and reset the coroutine --
            if (playSound != null) StopCoroutine(playSound);
            playSound = null;
        }
    }


    public IEnumerator PlaySequencialSoundDelay(List<AudioClip> Sounds, float delay, float startDelay)
    {
        yield return new WaitForSeconds(startDelay);
        for (int i = 0; i < Sounds.Count; i++)
        {
            AudioClip sound = Sounds[i];
            audioPlayer.Stop();
            audioPlayer.clip = sound;
            audioPlayer.loop = false;
            audioPlayer.Play();
            yield return new WaitForSeconds(sound.length + delay);
        }

        yield return new WaitForSeconds(PauseDelay);

        // --- ugly ? stop and reset the coroutine --
        if (playSound != null) StopCoroutine(playSound);
        playSound = null;
    }


}
