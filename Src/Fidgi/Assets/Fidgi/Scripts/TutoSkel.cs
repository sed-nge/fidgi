using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutoSequence
{

    public string title = null;
    public string text = null;
    public GameObject objectToFollow = null;
    public string objectToFollowName = null;
    public string[] HighlightObjectsName = null;

    public GameObject hologram = null;
    public float hologramDelay = 4f;
    public float turnArrowDelay = 2f;

    public List<AudioClip> audios = new List<AudioClip>();
    public List<AudioClip> endAudios = new List<AudioClip>();
    public List<AudioClip> randomEndAudios = new List<AudioClip>();
    public List<AudioClip> tryAgainAudios = new List<AudioClip>();
    public float tryAgainDelay = 10f;

    public float endDelay = 0.2f;
    private Helper h;

    public bool ended = false;

    public IEnumerator currentSequence = null;

    public TutoSequence(GameObject ObjectToFollow, string Title, string Text)
    {
        objectToFollow = ObjectToFollow;
        HighlightObjectsName = new string[] { ObjectToFollow.name };
        title = Title;
        text = Text;
    }
    public TutoSequence(string ObjectToFollowName, string Title, string Text)
    {
        HighlightObjectsName = new string[] { ObjectToFollowName };
        title = Title;
        text = Text;
    }
    public TutoSequence(string ObjectToFollowName)
    {
        HighlightObjectsName = new string[] { ObjectToFollowName };
    }
    public TutoSequence(GameObject ObjectToFollow)
    {
        objectToFollow = ObjectToFollow;
        HighlightObjectsName = new string[] { ObjectToFollow.name };
    }

    private GameObject GetGameObject()
    {
        if (objectToFollow) return objectToFollow;
        if (objectToFollowName != null) return GameObject.Find(objectToFollowName);
        return null;
    }

    /// <summary>
    /// Start the help the this step
    /// </summary>
    public void Start(Helper helperObject)
    {
        h = helperObject;
        objectToFollow = GetGameObject();
        ended = false;
        h.createHelper(objectToFollow, title, text);



        //currentSequence = waitForEnd();
    }

    private void playAudio(AudioClip c)
    {
        if (c == null) return;
        Vector3 position = objectToFollow.transform.position;
        AudioSource.PlayClipAtPoint(c, position);
    }

    public IEnumerator waitForEnd()
    {
        while (true)
        {
            for (int i = 0; i < audios.Count; i++)
            {
                if (audios[i])
                {
                    playAudio(audios[i]);
                    yield return new WaitForSeconds(audios[i].length + 1);
                }
            }
            yield return new WaitForSeconds(30);
        }
    }

    public IEnumerator playAudios(List<AudioClip> audios)
    {
        for (int i = 0; i < audios.Count; i++)
        {
            if (audios[i])
            {
                playAudio(audios[i]);
                yield return new WaitForSeconds(audios[i].length + 1);
            }
        }
        yield return new WaitForSeconds(30);
    }
    public IEnumerator playRandomAudio(List<AudioClip> audios)
    {
        int i = Random.Range(0, audios.Count);
        if (audios[i])
        {
            playAudio(audios[i]);
            yield return new WaitForSeconds(audios[i].length + 1);
        }
    }


    public IEnumerator playEnd()
    {
        if (endAudios.Count > 0)
        {
            for (int i = 0; i < endAudios.Count; i++)
            {
                if (endAudios[i])
                {
                    playAudio(endAudios[i]);
                    yield return new WaitForSeconds(endAudios[i].length + 1);
                }
            }
            yield return new WaitForSeconds(2);

        }
        else
        {
            int i = Random.Range(0, randomEndAudios.Count);
            if (randomEndAudios[i])
            {
                playAudio(randomEndAudios[i]);
                yield return new WaitForSeconds(randomEndAudios[i].length + 1);

            }
        }

    }


}



/**********************************************


████████╗██╗   ██╗████████╗ ██████╗ ██████╗ ██╗ █████╗ ██╗     
╚══██╔══╝██║   ██║╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗██║     
   ██║   ██║   ██║   ██║   ██║   ██║██████╔╝██║███████║██║     
   ██║   ██║   ██║   ██║   ██║   ██║██╔══██╗██║██╔══██║██║     
   ██║   ╚██████╔╝   ██║   ╚██████╔╝██║  ██║██║██║  ██║███████╗
   ╚═╝    ╚═════╝    ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚══════╝
                                                               

***********************************************/
[RequireComponent(typeof(Helper))]
[RequireComponent(typeof(DisableScene))]

public abstract class TutoSkel : MonoBehaviour
{
    [SerializeField, Tooltip("Audio Sound at start")]
    public List<AudioClip> startAudios = new List<AudioClip>();

    [SerializeField, Tooltip("Audio Sound at end")]
    public List<AudioClip> endAudios = new List<AudioClip>();

    [SerializeField, Tooltip("Random ends for each sequence")]
    public List<AudioClip> randomEndAudios = new List<AudioClip>();

    [SerializeField, Tooltip("Random abort audio")]
    public List<AudioClip> randomAbortAudios = new List<AudioClip>();

    [SerializeField, Tooltip("objects to follow")]
    public List<GameObject> objects = new List<GameObject>();

    [SerializeField, Tooltip("holograms to help")]
    public List<GameObject> holograms = new List<GameObject>();

    [SerializeField, Tooltip("Delay at the end")]
    public float endDelay = 2f;

    [SerializeField, Tooltip("The turn left arrow")]
    public GameObject turnLeft;

    [SerializeField, Tooltip("The turn right arrow")]
    public GameObject turnRight;



    private VoicePlayer voicePlayer;

    protected IEnumerator TutoDo = null;
    private IEnumerator hologramDisplayCoroutine = null;
    private IEnumerator turnDisplayCoroutine = null;

    protected List<TutoSequence> sequences = new List<TutoSequence>();
    protected Helper h = null;

    protected DisableScene disableScene;
    protected int index = 0;

    // Start is called before the first frame update
    public virtual void Start()
    {
        h = gameObject.GetComponent<Helper>();
        if (!h) Debug.LogError("Helper not found ??");


        disableScene = GetComponent<DisableScene>();

        VoicePlayer[] s = FindObjectsOfType(typeof(VoicePlayer)) as VoicePlayer[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one VoicePlayer in the scene ;");

        }
        else
        {
            voicePlayer = s[0];
        }
    }



    public virtual void Update()
    {
    }


    public void Abort()
    {
        StartCoroutine(abortProcedure());
        disableScene.EnableAll();
    }

    IEnumerator abortProcedure()
    {
        if (hologramDisplayCoroutine != null)
        {
            StopCoroutine(hologramDisplayCoroutine);
            hologramDisplayCoroutine = null;
        }
        if (turnDisplayCoroutine != null)
        {
            StopCoroutine(turnDisplayCoroutine);
            turnDisplayCoroutine = null;
        }



        if (TutoDo != null)
        {
            StopCoroutine(TutoDo);
            TutoDo = null;
            voicePlayer.abortCurrentSound();
            voicePlayer.PlayRandom(randomAbortAudios, 0, 0.5f);
            yield return voicePlayer.waitForSoundToFinish();
        }

        onAbort();

        // Erase all holograms and reset
        clean();



    }


    private void clean()
    {
        disableScene.EnableAll();
        h.endHelper();

        if (turnLeft) turnLeft.SetActive(false);
        if (turnRight) turnRight.SetActive(false);

        index = 0;
        for (int i = 0; i < sequences.Count; i++)
        {
            if (sequences[i].hologram) sequences[i].hologram.SetActive(false);
            sequences[i].ended = false;
        }
    }

    public void endSequence(int i)
    {
        // Debug.Log("Try to end sequence "+i+" actually "+index);
        if (i == index)
        {
            sequences[i].ended = true;
        }
    }

    public void dropHelper(int i)
    {
        if (i == index)
        {
            h.endHelper();
        }
    }

    public void doIt()
    {
        clean();

        onStart();

        TutoDo = DoIt();
        StartCoroutine(TutoDo);
    }


    protected virtual void onStart()
    {

    }

    protected virtual void onAbort()
    {

    }
    protected virtual void onEnd()
    {

    }

    protected IEnumerator DoIt()
    {

        voicePlayer.abortCurrentSound();
        // --- Play the start sounds ---
        // disableScene.DisableAll(new string[] { });

        voicePlayer.Play(startAudios, 0.3f, 0f);
        yield return voicePlayer.waitForSoundToFinish();
        index = 0;
        while ((index > -1) && (index < sequences.Count))
        {
            Debug.Log("Do sequence " + index);
            yield return DoSequence(sequences[index]);
            index++;

        }

        onEnd();

        voicePlayer.Play(endAudios, 0.3f, 0.5f);
        yield return voicePlayer.waitForSoundToFinish();
        StopCoroutine(TutoDo);
        TutoDo = null;
        clean();

    }



    IEnumerator DoSequence(TutoSequence sequence)
    {
        h.endHelper();

        disableScene.DisableAll(sequence.HighlightObjectsName);

        sequence.Start(h);

        // ---- Start the sound ----
        voicePlayer.abortCurrentSound();
        voicePlayer.Play(sequence.audios, 0.5f, 0);


        // --- Start the hologram
        if (hologramDisplayCoroutine != null)
        {
            StopCoroutine(hologramDisplayCoroutine);
            hologramDisplayCoroutine = null;
        }
        hologramDisplayCoroutine = showHologram(sequence);
        StartCoroutine(hologramDisplayCoroutine);

        // --- Start the turn arrow
        if (turnDisplayCoroutine != null)
        {
            StopCoroutine(turnDisplayCoroutine);
            turnDisplayCoroutine = null;
        }
        turnDisplayCoroutine = showTurnArrow(sequence);
        StartCoroutine(turnDisplayCoroutine);




        // -- Wait for end of the sequence --
        float time = Time.time;
        while (sequence.ended == false)
        {
            yield return new WaitForSeconds(1);

            // --- Send a try Again
            if ((Time.time > (time + sequence.tryAgainDelay)) && (sequence.tryAgainAudios.Count > 0))
            {
                // voicePlayer.abortCurrentSound();
                voicePlayer.Play(sequence.tryAgainAudios, 1, 0);
                time = Time.time;
            }
        }


        if (hologramDisplayCoroutine != null)
        {
            StopCoroutine(hologramDisplayCoroutine);
            hologramDisplayCoroutine = null;
        }
        if (sequence.hologram) sequence.hologram.SetActive(false);

        if (turnDisplayCoroutine != null)
        {
            StopCoroutine(turnDisplayCoroutine);
            turnDisplayCoroutine = null;
        }
        if (turnLeft) turnLeft.SetActive(false);
        if (turnRight) turnRight.SetActive(false);


        h.endHelper();
        voicePlayer.abortCurrentSound();
        if (sequence.endAudios.Count > 0)
        {
            voicePlayer.Play(sequence.endAudios, 0.5f, 0);
            yield return voicePlayer.waitForSoundToFinish();
        }
        else
        {
            voicePlayer.PlayRandom(sequence.randomEndAudios, 0, 0);
            yield return voicePlayer.waitForSoundToFinish();
        }
        yield return new WaitForSeconds(endDelay);

    }


    public IEnumerator showHologram(TutoSequence sequence)
    {
        if (sequence.hologram != null)
        {
            yield return new WaitForSeconds(sequence.hologramDelay);
            sequence.hologram.SetActive(true);
        }
    }

    public IEnumerator showTurnArrow(TutoSequence sequence)
    {
        yield return new WaitForSeconds(sequence.turnArrowDelay);


        while ((sequence.objectToFollow != null) && (turnLeft) && (turnRight))
        {
            yield return new WaitForSeconds(0.05f);


            // --- Check player direction related to the object
            Vector3 targetDir = sequence.objectToFollow.transform.position - Camera.main.transform.position;
            float angle = Vector3.SignedAngle(targetDir, Camera.main.transform.forward, Vector3.up);

            if (angle > 50f)
            {
                turnLeft.SetActive(true);
                turnLeft.transform.LookAt(sequence.objectToFollow.transform.position);
                turnRight.SetActive(false);
            }
            else
            {
                if (angle < -50f)
                {
                    turnLeft.SetActive(false);
                    turnRight.SetActive(true);
                    turnRight.transform.LookAt(sequence.objectToFollow.transform.position);
                }
                else
                {
                    turnLeft.SetActive(false);
                    turnRight.SetActive(false);

                }
            }


        }
        if (sequence.hologram != null)
        {
            yield return new WaitForSeconds(sequence.hologramDelay);
            sequence.hologram.SetActive(true);
        }
    }




    public void dropHelper0() { dropHelper(0); }
    public void dropHelper1() { dropHelper(1); }
    public void dropHelper2() { dropHelper(2); }
    public void dropHelper3() { dropHelper(3); }
    public void dropHelper4() { dropHelper(4); }
    public void dropHelper5() { dropHelper(5); }
    public void dropHelper6() { dropHelper(6); }
    public void dropHelper7() { dropHelper(7); }
    public void dropHelper8() { dropHelper(8); }
    public void dropHelper9() { dropHelper(9); }

    public void end0() { endSequence(0); }
    public void end1() { endSequence(1); }
    public void end2() { endSequence(2); }
    public void end3() { endSequence(3); }
    public void end4() { endSequence(4); }
    public void end5() { endSequence(5); }
    public void end6() { endSequence(6); }
    public void end7() { endSequence(7); }
    public void end8() { endSequence(8); }
    public void end9() { endSequence(9); }

}
