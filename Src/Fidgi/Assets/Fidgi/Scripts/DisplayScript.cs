using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.Video;


[RequireComponent(typeof(XRSocketInteractor))]
[AddComponentMenu("Fidgi Display")]

/// <summary>
/// Manage the display of a video / gameobject / Slideshow
/// </summary>
public class DisplayScript : Exe
{

    [SerializeField, Tooltip("Related videoscreen")]
    public GameObject videoScreen;
    [SerializeField, Tooltip("Related hologram projector")]
    public GameObject hologram;
    [SerializeField, Tooltip("Position of the hologram")]
    public GameObject hologramPosition;
    [SerializeField, Tooltip("Light of the hologram")]
    public GameObject hologramLight;
    [SerializeField, Tooltip("Delay help")]
    public float delayBeetweenHelp = 100f;

    [SerializeField, Tooltip("Video to play when win")]
    public VideoClip winVideo;
    private VideoPlayer vp;
    private Animator screenAnimator;
    private Animator holoAnimator = null;

    private GameObject currentHolo;

    private bool readerUsed = false;

    private IEnumerator coroutine = null;

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        if (!videoScreen)
            Debug.LogError("No  VideoScreen given");

        vp = videoScreen.GetComponent<VideoPlayer>();
        if (!vp)
            Debug.LogError("No VideoPlayer for the screen");

        screenAnimator = videoScreen.GetComponent<Animator>();
        if (!screenAnimator)
            Debug.LogError("No screenAnimator for the screen");

        if (!hologram)
        {
            Debug.LogError("No hologram given");
        }
        else
        {
            holoAnimator = hologram.GetComponent<Animator>();
            if (!holoAnimator)
                Debug.LogError("No Animator for the hologram");

        }

        if (hologramLight)
        {
            hologramLight.SetActive(false);
        }

        if (ambiantVoice) StartCoroutine(CheckIfUsedToGiveClue());

    }

    /// <summary>
    /// Give clue to the user if not used
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckIfUsedToGiveClue()
    {
        while (readerUsed == false)
        {
            yield return new WaitForSeconds(delayBeetweenHelp + Random.Range(10, 50));
            yield return new WaitWhile(() => ambiantVoice.isMuted());
            yield return new WaitForSeconds(Random.Range(10,50) );
            if (readerUsed == false) ambiantVoice.HelpGPU();
        }
    }

    // Update is called once per frame
    public override void Update()
    {

    }



    public override void OnSelectEnter()
    {

        // Debug.Log("Enter yeah !!");

        base.OnSelectEnter();
        if (!fidgi) return;

        // --- Adding some small score because read an object 
        Scores.addToScore(10);

        readerUsed = true;

        VideoClip v = fidgi.GetVideo();
        if (v)
        {
            if (ambiantVoice) ambiantVoice.MuteOn();
            VideoPlayer vp = videoScreen.GetComponent<VideoPlayer>();
            vp.isLooping = true;
            vp.clip = v;
            screenAnimator.SetBool("On", true);
            videoScreen.SetActive(true);
            vp.Play();
        }

        // Play a hologram show
        if ((holoAnimator) && (fidgi.holograms.Count > 0))
        {
            Debug.Log("open Animator hologram");
            holoAnimator.SetBool("open", true);
            coroutine = playHolograms();
            StartCoroutine(coroutine);
        }

        if (hologramLight)
        {
            hologramLight.SetActive(false);
        }


        // Play a slide show
        // ... soon ...

    }


    IEnumerator playHolograms()
    {
        yield return new WaitForSeconds(3f);
        while (true)
        {
            // place the hologram above the projector and
            if (!fidgi) yield return (null);

            currentHolo = fidgi.GetHologram();
            if (currentHolo != null)
            {
                if (hologramPosition)
                {
                    currentHolo.transform.SetParent(hologramPosition.transform);
                    currentHolo.transform.position = hologramPosition.transform.position;
                }
                if (hologramLight)
                {
                    hologramLight.SetActive(true);
                }
                currentHolo.SetActive(true);
                yield return new WaitForSeconds(fidgi.hologramsShowDelay);
                currentHolo.SetActive(false);
                currentHolo = null;

                if (hologramLight)
                {
                    hologramLight.SetActive(false);
                }
            }
            yield return new WaitForSeconds(0.2f);
        }
    }


    public override void OnSelectExit()
    {
        if (!fidgi) return;

        // Stay enough in the reader machine. Give informations
        if ((Time.time - startTime) > fidgi.minimumInfo)
        {
            fidgi.activateInfos();
            // --- Adding some small score because read during minimumLabelTime 
            Scores.addToScore(5);

        }

        if ((vp) && (vp.isPlaying))
        {
            screenAnimator.SetBool("On", false);
            // The stop of playing the video is done at the end
            // of the "stop" animation by a behaviour script

        }


        if (holoAnimator)
        {

            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }

            // --- disable all holograms ---
            for (int i = 0; i < fidgi.holograms.Count; i++)
            {
                if (fidgi.holograms[i]) fidgi.holograms[i].SetActive(false);
            }

            if (hologramLight)
            {
                hologramLight.SetActive(false);
            }
            holoAnimator.SetBool("open", false);

        }

        if (ambiantVoice) ambiantVoice.MuteOff();

        base.OnSelectExit();

    }


    public void PlayWinVideo()
    {
        if ( winVideo == null ) return;
        
        VideoPlayer vp = videoScreen.GetComponent<VideoPlayer>();
        vp.isLooping = true;
        vp.clip = winVideo;
        screenAnimator.SetBool("On", true);
        videoScreen.SetActive(true);
        vp.Play();
    }


}
