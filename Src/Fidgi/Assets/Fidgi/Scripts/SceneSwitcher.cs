using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[AddComponentMenu("Fidgi Scene switcher")]

public class SceneSwitcher : MonoBehaviour
{

    [SerializeField, Tooltip("Exit Fade image")]
    public RawImage img;



    private AsyncOperation loadingOperation = null;

    private Scene currentScene;

    private FadeScript fade = null;

    public void GotoMainScene()
    {
        loadScene("XRRoom");
    }

    public void GotoMenuScene()
    {
        loadScene("MenuScene");
    }

    public void Exit()
    {
        StartCoroutine(ExitApplication());
    }

    void Start()
    {
        currentScene = SceneManager.GetActiveScene();


        if (img)
        {
            img.gameObject.SetActive(true);
            fade = img.GetComponent<FadeScript>();
            if (!fade) Debug.LogError("Warning, rawImage doesnt have the FadeScript.");
        }

        /*
        if (currentScene.name == "LoadingScene")
        {
            StartCoroutine(loadMenuScene());
        }
        */
    }


    IEnumerator loadMenuScene()
    {
        yield return new WaitForSeconds(5f);
        if (fade) fade.ExitFade();
        loadScene("MenuScene");
    }

    /// <summary>
    /// Load a scene asynchronous
    /// and switch to the scene at the end of loading
    /// </summary>
    /// <param name="sceneName"></param>
    private void loadScene(string sceneName)
    {
        if (fade)
        {
            fade.ExitFade();
        }
        StartCoroutine(Load(sceneName));
    }


    IEnumerator ExitApplication()
    {
        if (fade) fade.ExitFade();
        float delay = fade ? fade.SceneExitDelay : 2f;
        yield return new WaitForSeconds(delay);
        Application.Quit();
    }

    IEnumerator Load(string sceneName)
    {
        loadingOperation = SceneManager.LoadSceneAsync(sceneName);
        while ((loadingOperation != null) && (!loadingOperation.isDone))
        {
            if (fade) fade.ExitPrecent = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            yield return null;
        }
    }
}