using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[AddComponentMenu("Fidgi Tutorial")]
public class Tuto : TutoSkel
{

    [SerializeField, Tooltip("Fidgi object list")]
    public GameObject FidjiObjects;
    [SerializeField, Tooltip("Character Starting point")]
    public GameObject characterStartingPoint;
    [SerializeField, Tooltip("Invention Starting point")]
    public GameObject inventionStartingPoint;


    [SerializeField, Tooltip("The Reader")]
    public GameObject reader;

    [SerializeField, Tooltip("The Character")]
    public GameObject character;

    [SerializeField, Tooltip("The invention")]
    public GameObject invention;

    [SerializeField, Tooltip("The SSD Table")]
    public GameObject ssd;

    [SerializeField, Tooltip("The combined")]
    public GameObject combined;
    [SerializeField, Tooltip("SSD Slot")]
    public GameObject slot;
    [SerializeField, Tooltip("Tuttorial button")]
    public GameObject tutoButton;
    [SerializeField, Tooltip("Tuttorial button abort")]
    public GameObject tutoButtonAbort;

    [SerializeField, Tooltip("Step 1")]
    public List<AudioClip> step1 = new List<AudioClip>();

    [SerializeField, Tooltip("Step 2")]
    public List<AudioClip> step2 = new List<AudioClip>();
    [SerializeField, Tooltip("Step 3")]
    public List<AudioClip> step3 = new List<AudioClip>();
    [SerializeField, Tooltip("Step 4")]
    public List<AudioClip> step4 = new List<AudioClip>();
    [SerializeField, Tooltip("Step 5")]
    public List<AudioClip> step5 = new List<AudioClip>();

    [SerializeField, Tooltip("Step 5 end")]
    public List<AudioClip> step5end = new List<AudioClip>();

    [SerializeField, Tooltip("Step 6")]
    public List<AudioClip> step6 = new List<AudioClip>();


    public UnityEvent m_MyEvent = new UnityEvent();

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        // 0 --- Take the memory character
        TutoSequence sequence = new TutoSequence(character, null, "Prends cette mémoire");
        sequence.audios = step1;
        sequence.HighlightObjectsName = new string[] { character.name, "Collider", "ButtonAbortTutorial" };
        sequence.randomEndAudios = randomEndAudios;
        sequence.hologram = holograms[0];
        sequence.hologramDelay = 11f;
        sequences.Add(sequence);

        // 1 --- put the memory character in the reader
        sequence = new TutoSequence(reader);
        sequence.audios = step2;
        sequence.HighlightObjectsName = new string[] { character.name, "Collider", "GPU", "ButtonAbortTutorial" };
        sequence.randomEndAudios = randomEndAudios;
        sequences.Add(sequence);

        // 2 --- put the memory character on the SSD
        sequence = new TutoSequence(ssd);
        sequence.audios = step3;
        sequence.turnArrowDelay = 8f;
        sequence.HighlightObjectsName = new string[] { character.name, "Collider", "GPU", "SSD", "ButtonAbortTutorial" };
        sequence.randomEndAudios = randomEndAudios;
        sequences.Add(sequence);

        // 3 --- Take the memory invention
        sequence = new TutoSequence(invention);
        sequence.HighlightObjectsName = new string[] { invention.name, "Collider", "ButtonAbortTutorial" };
        sequence.audios = step4;
        sequence.turnArrowDelay = 6f;
        sequence.hologramDelay = 12f;
        sequence.randomEndAudios = randomEndAudios;
        sequences.Add(sequence);

        // 4 --- merge invention and character
        sequence = new TutoSequence(character);
        sequence.HighlightObjectsName = new string[] { character.name, invention.name, "Collider", "ButtonAbortTutorial" };
        sequence.audios = step5;
        sequence.turnArrowDelay = 6f;
        sequence.endAudios = step5end;
        sequences.Add(sequence);

        // 5 --- Add invention on the slot
        sequence = new TutoSequence(slot);
        sequence.turnArrowDelay = 4f;
        sequence.HighlightObjectsName = new string[] { combined.name, slot.name, "Collider" };
        sequence.audios = step6;
        sequence.randomEndAudios = randomEndAudios;
        sequences.Add(sequence);

        // resetAndStart();
    }


    public void resetAndStart()
    {

        StartCoroutine(Restart());
    }


    IEnumerator Restart()
    {

        // --- If an object is at the place 
        yield return removeObjectFromTheGrabZone(characterStartingPoint, character);
        yield return removeObjectFromTheGrabZone(inventionStartingPoint, invention);
        yield return removeObjectFromTheGrabZone(reader, null);

        // --- set objects at the right place 
        invention.transform.position = inventionStartingPoint.transform.position;
        character.transform.position = characterStartingPoint.transform.position;
        character.SetActive(true);
        character.GetComponent<FidgiObjectScript>().activateInfos(false);
        invention.SetActive(true);
        invention.GetComponent<FidgiObjectScript>().activateInfos();
        combined.SetActive(false);
        // --- set position somewhere to be out of a XR Socket Interactor
        combined.transform.position = characterStartingPoint.transform.position + new Vector3(0, 2, 0);
        combined.transform.parent = FidjiObjects.transform;
        slot.GetComponent<ExcelCell>().RestartCell();
        doIt();
    }

    /// <summary>
    /// Remove the grabbed object if exists
    /// /// </summary>
    /// <param name="obj"></param>
    private IEnumerator removeObjectFromTheGrabZone(GameObject obj, GameObject mustBeThisObject)
    {
        if (obj)
        {
            GenericExe xr = obj.GetComponent<GenericExe>();
            if ((xr) && (xr.filled()))
            {
                if (mustBeThisObject == null)
                {
                    xr.ThrowObject();
                    yield return new WaitForSeconds(xr.throwDelay);
                }
                else
                {
                    if (xr.getTarget().GetInstanceID() != mustBeThisObject.GetInstanceID())
                    {
                        xr.ThrowObject();
                        yield return new WaitForSeconds(xr.throwDelay);
                    }
                }
            }
        }
    }

    protected override void onStart()
    {

        base.onStart();

        if (!tutoButton) return;
        tutoButton.SetActive(false);
        if (!tutoButtonAbort) return;
        tutoButtonAbort.SetActive(true);

    }

    protected override void onAbort()
    {
        if (!tutoButton) return;
        tutoButton.SetActive(true);
        if (!tutoButtonAbort) return;
        tutoButtonAbort.SetActive(false);

    }
    protected override void onEnd()
    {
        if (!tutoButton) return;
        tutoButton.SetActive(true);
        if (!tutoButtonAbort) return;
        tutoButtonAbort.SetActive(false);

    }


    /// <summary>
    /// Triggered by reader OnSelectEnter
    /// /// </summary>
    public void checkIfCharacterInReader()
    {
        GameObject target = reader.GetComponent<DisplayScript>().getTarget();
        if (!target) return;
        if (target.GetInstanceID() != character.GetInstanceID()) return;
        end1();
    }


    /// <summary>
    /// Triggered by TutoCollide on the SSD Table
    /// </summary>
    /// <param name="coll"></param>
    public void characterOnTable(GameObject ssdTable)
    {
        TutoCollide tl = ssdTable.GetComponent<TutoCollide>();
        if (!tl) return;
        if (!tl.followingObject) return;
        if (tl.followingObject.GetInstanceID() == character.GetInstanceID()) end2();
    }



    public override void Update()
    {

        base.Update();


        // --- Tuto not in progress
        if ( TutoDo == null ) return;


        // --- Check if combined appears on scene
        if (combined.activeSelf == true)
        {
            end4();
        }
    }
}