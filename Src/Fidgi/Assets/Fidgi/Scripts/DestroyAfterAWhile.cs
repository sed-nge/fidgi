using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Fidgi Destroy After a While")]

public class DestroyAfterAWhile : MonoBehaviour
{

    [SerializeField, Tooltip("The delay of staying available")]

    public float delay = 5f;

    [SerializeField, Tooltip("On Start")]

    public bool onStart = true;

    // Start is called before the first frame update
    void Start()
    {
        if (onStart == true)
        {
            destroy();
        }
    }

    public void destroy()
    {
        Destroy(gameObject, 5);
    }


}
