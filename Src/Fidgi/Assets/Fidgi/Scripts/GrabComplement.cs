using System;
using UnityEngine;
using Unity;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.Events;



/// <summary>
/// </summary>
[SelectionBase]
[DisallowMultipleComponent]
[RequireComponent(typeof(XRGrabInteractable))]
[AddComponentMenu("Fidgi GrabComplement script")]
public class GrabComplement : MonoBehaviour
{

    [SerializeField, Tooltip("Hilight object on hover")]

    public OutlineScript Outline;

    [SerializeField, Tooltip("Right Hand attach point")]

    public Transform RightHandAttach;
    [SerializeField, Tooltip("Left Hand attach point")]

    public Transform LeftHandAttach;

    [SerializeField, Tooltip("Minimal distance to consider objects in hand.")]


    public float MinimalDistanceInHand = 0.2f;


    [SerializeField, Range(0, 1f), Tooltip("Haptic intensity when object is in hand.")]

    public float hapticIntensity = 0.2f;
    public float duration = 0.2f;




    [SerializeField, Tooltip("Tutorial event To trigger on enter in hand")]
    public UnityEvent EnterHandEvent = new UnityEvent();

    [SerializeField, Tooltip("Tutorial event To trigger on leaving in hand")]
    public UnityEvent ExitHandEvent = new UnityEvent();


    private Transform normalAttach;

    private XRGrabInteractable XR;
    private XRRayInteractor xray;
    private handController HandC;
    private bool InHand = false;

    private void Start()
    {
        XR = gameObject.GetComponent<XRGrabInteractable>();
        normalAttach = XR.attachTransform;

    }

    public void OnSelectEnter(SelectEnterEventArgs eventArg)
    {

        // Debug.Log("OnSelect Enter old " + (HandC ? "hand" : "other"));

        // --- In case of hand changement, start the laser on the old one
        if (HandC) HandC.StartLaser();
        HandC = null;
        xray = null;
        InHand = false;
        changeAttachToNormal();


        if (eventArg.interactor.gameObject.tag == "Player")
        {

            xray = eventArg.interactor.gameObject.GetComponent<XRRayInteractor>();
            HandC = eventArg.interactor.gameObject.GetComponent<handController>();

            if ((xray != null) && (HandC != null))
            {

            //Debug.Log("OnSelect Enter new " + (HandC ? "hand" : "other"));

                changeAttachToHand(HandC.isRightHand());
            }
        }
    }

    private void Update()
    {
        if ((xray) && (HandC))
        {
            // --- Check if enter hand or leave hand ----
            float distance = Vector3.Distance(gameObject.transform.position, xray.transform.position);
            if ((distance < MinimalDistanceInHand) && (InHand == false))
            {
                HandC.playHaptic(hapticIntensity, duration);
                HandC.StopLaser();

                // --- Enter hand ---
                InHand = true;
                if ((EnterHandEvent != null) && (EnterHandEvent.GetPersistentEventCount() > 0)) EnterHandEvent?.Invoke();
            }

            if ((distance >= MinimalDistanceInHand) && (InHand == true))
            {
                HandC.StartLaser();

                // --- Leave hand ---
                InHand = false;
                if ((ExitHandEvent != null) && (ExitHandEvent.GetPersistentEventCount() > 0)) ExitHandEvent?.Invoke();
            }
        }
    }

    public void OnSelectExit(SelectExitEventArgs eventArg)
    {
        // --- If was in Hand of the user, call 
        if (InHand == true)
        {
            if ((ExitHandEvent != null) && (ExitHandEvent.GetPersistentEventCount() > 0)) ExitHandEvent?.Invoke();
        }

        //Debug.Log("OnSelect Exit " + (HandC ? "hand" : "other"));

        if (HandC) HandC.StartLaser();
        HandC = null;
        xray = null;
        InHand = false;
        changeAttachToNormal();
    }



    public void StartOutlineIfHoveredByHand(HoverEnterEventArgs eventArg)
    {
        handController controler = eventArg.interactor.gameObject.GetComponent<handController>();
        if ((controler) && (Outline)) Outline.StartOutline();
    }
    public void StopOutlineIfHoveredByHand(HoverExitEventArgs eventArg)
    {
        handController controler = eventArg.interactor.gameObject.GetComponent<handController>();
        if ((controler) && (Outline)) Outline.StopOutline();
    }



    private void changeAttachToHand(bool isRightHand)
    {
        XR.attachTransform = isRightHand ? RightHandAttach : LeftHandAttach;
    }
    private void changeAttachToNormal()
    {
        XR.attachTransform = normalAttach;
    }


    public void OnActivate(ActivateEventArgs eventArgs)
    {
        Debug.Log("trigger on me");
    }
    public void OnDeActivate(DeactivateEventArgs eventArgs)
    {

    }



}
