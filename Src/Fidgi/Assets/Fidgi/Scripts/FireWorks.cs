﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




[AddComponentMenu("Fidgi Fireworks")]
public class FireWorks : MonoBehaviour
{

    [SerializeField, Tooltip("List of colors")]

    public List<Color> colors = new List<Color>();

    [SerializeField, Tooltip("Positions")]
    public List<Vector3> positions = new List<Vector3>();


    [SerializeField, Tooltip("random radius position")]
    public float radius = 5f;

    [SerializeField, Tooltip("minimum Duration")]
    public float minimumDuration = 5f;

    [SerializeField, Tooltip("firework source")]
    public GameObject firework;


    private IEnumerator currentShow = null;

    private AmbiantVoice ambiantVoice;

    private Gradient chooseColor()
    {
        GradientColorKey[] colorKey = new GradientColorKey[2];
        colorKey[0].color = colors[Random.Range(0, colors.Count)];
        colorKey[0].time = 0.0f;
        colorKey[1].color = Color.white;
        colorKey[1].time = 2.0f;

        GradientAlphaKey[] alphaKey = new GradientAlphaKey[2];
        alphaKey[0].alpha = 1.0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 0.0f;
        alphaKey[1].time = 2.0f;

        Gradient gradient = new Gradient();
        gradient.SetKeys(colorKey, alphaKey);
        return gradient;

    }


    private void Start()
    {
        // --- Not comming back from the XRROom scene
        if (Scores.currentUserScore == -1) return;

        // --- compute the show duration from the player result
        if (Scores.currentUserPair > 0)
        {
            float showDuration = ((float)Scores.currentUserPair / (float)globalVariables.numberOfCellToFill) * 30f + minimumDuration;
            StartTheShow(showDuration);
        }
    }


    public void StartTheShow(float duration)
    {
        if (currentShow != null)
        {
            StopCoroutine(currentShow);
            currentShow = null;
        }
        currentShow = doShow(duration);
        StartCoroutine(currentShow);
    }

    private Vector3 getRandomPosition()
    {

        Vector3 p = positions[Random.Range(0, positions.Count)];
        return new Vector3(Random.Range(p.x - radius, p.x + radius), Random.Range(p.y - radius, p.y + radius), Random.Range(p.z - radius, p.z + radius));
    }

    private GameObject spawnfirework()
    {
        GameObject f = Instantiate(firework, getRandomPosition(), firework.transform.rotation);
        ParticleSystem p = f.GetComponent<ParticleSystem>();
        if (!p) return null;
        ParticleSystem.TrailModule trails = p.trails;
        trails.colorOverTrail = chooseColor();

        f.SetActive(true);
        ParticleSystem.EmissionModule e = p.emission;
        e.enabled = true;
        p.Play();
        return f;
    }

    IEnumerator doShow(float duration)
    {
        float t = Time.time;
        while ((Time.time - t) < duration)
        {
            GameObject f = spawnfirework();
            if (f) Destroy(f, 5f);

            yield return new WaitForSeconds(Random.Range(0.5f, 1f));

            // --- destroy finished 
        }
    }

}