﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Battery Manager Script")]

public class BatteryManager : MonoBehaviour
{

    [SerializeField, Tooltip("List of sub Objects for managing rotation and color")]
    public List<GameObject> parts = new List<GameObject>();

    [SerializeField, Tooltip("text to display")]

    public GameObject displayText;


    [SerializeField, Range(0.0f, 3.0f), Tooltip("delay beetween two update")]
    public float updateInterval = 0.2f;


    [SerializeField, Tooltip("Normal color")]

    public Color normal;
    [SerializeField, Tooltip("Color below 20%")]

    public Color color20;

    [SerializeField, Tooltip("Color below 10%")]

    public Color color10;

    [SerializeField, Tooltip("Rotation threshold")]

    public float rotatingThreshold = 0.05f;
    private Animator anim;
    private float previousBatteryLevel = 1f;

    private BatteryVoice batteryVoice;


    private TextMesh txt;

    void Start()
    {

        onOffBattery(globalVariables.getBatteryLevel());
        anim = GetComponent<Animator>();
        if (displayText)
        {
            txt = displayText.GetComponent<TextMesh>();
        }


        BatteryVoice[] s = FindObjectsOfType(typeof(BatteryVoice)) as BatteryVoice[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one BatteryVoice in the scene ;");

        }
        else
        {
            batteryVoice = s[0];
        }


        StartCoroutine(Change());


    }


    IEnumerator Change()
    {
        bool rotating = false;

        while (true)
        {
            yield return new WaitForSeconds(updateInterval);
            float batteryLevel = globalVariables.getBatteryLevel();
            if ((!rotating) && (batteryLevel < rotatingThreshold) && (anim))
            {
                anim.SetBool("rotate", true);
            }
            onOffBattery(batteryLevel);
            changeTextAndSayPercent(batteryLevel);
            previousBatteryLevel = batteryLevel;
        }
    }


    private void changeTextAndSayPercent(float batteryLevel)
    {
        if (!txt) return;

        int percent = Mathf.RoundToInt(batteryLevel * 100);
        int oldPercent = Mathf.RoundToInt(previousBatteryLevel * 100);
        if (percent == oldPercent) return;

        txt.text = string.Format("{00}%", percent);

        // --- set the sound ---
        if (batteryVoice) batteryVoice.BatteryPercentVoice(percent);

    }


    /// <summary>
    /// Set on or of battery items with the right color
    /// </summary>
    /// <param name="batteryLevel"></param>
    private void onOffBattery(float batteryLevel)
    {

        bool needToChangeColor = false;
        Color c = normal;
        if (previousBatteryLevel >= 1) needToChangeColor = true;

        if (batteryLevel < 0.3)
        {
            if (previousBatteryLevel >= 0.3) needToChangeColor = true;
            c = color20;
        }
        if (batteryLevel < 0.2)
        {
            if (previousBatteryLevel >= 0.2) needToChangeColor = true;
            c = color10;
        }

        // --- Change the color of the display text ---
        if ((needToChangeColor) && (txt))
        {
            txt.color = c;
        }

        for (int i = 0; i < parts.Count; i++)
        {
            GameObject p = parts[i];

            float ratio = (float)i / (float)parts.Count;

            // --- The part item must be visible, totaly or partially
            if (batteryLevel > ratio)
            {

                // Vue --- Full ---
                if (needToChangeColor) changeColor(p, c, c);
                if (p.activeSelf == false) p.SetActive(true);
                continue;
            }

            // --- The part item must be invisible
            if ((batteryLevel < ratio) && (p.activeSelf == true))
            {
                p.SetActive(false);
                continue;
            }
        }
    }


    private void changeColor(GameObject o, Color color, Color emissionColor)
    {

        Renderer render = o.GetComponent<Renderer>();
        if (!render) return;
        //        Debug.Log("Change color for " + o.name);
        Material[] mats = render.materials;
        foreach (Material m in mats)
        {

            m.SetColor("_EmissionColor", emissionColor);
            m.SetColor("_BaseColor", color);
            m.EnableKeyword("_EMISSION");
        }
    }
}