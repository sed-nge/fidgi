using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TutoStep
{

    public string title = null;
    public string text = null;
    public GameObject objectToFollow = null;
    public string objectToFollowName = null;
    public string congratTitle = null;
    public string congratText = null;
    public GameObject hologram = null;

    private float congratDelay = 0f;

    private AudioClip startAudio = null;
    private AudioClip endAudio = null;


    public TutoStep(GameObject ObjectToFollow, string Title, string Text)
    {
        objectToFollow = ObjectToFollow;
        title = Title;
        text = Text;
    }
    public TutoStep(string ObjectToFollowName, string Title, string Text)
    {
        objectToFollowName = ObjectToFollowName;
        title = Title;
        text = Text;
    }
    public TutoStep()
    {
        objectToFollowName = null;
        objectToFollowName = null;
    }
    public void addCongratMessage(string Title, string Text, float delay)
    {
        congratTitle = Title;
        congratText = Text;
        congratDelay = delay;
    }

    public void addAudio(AudioClip start, AudioClip end)
    {
        startAudio = start;
        endAudio = end;
    }

    private GameObject GetGameObject()
    {
        if (objectToFollow) return objectToFollow;
        if (objectToFollowName != null) return GameObject.Find(objectToFollowName);
        return null;
    }

    /// <summary>
    /// Start the help the this step
    /// </summary>
    public void Start(Helper h)
    {
        objectToFollow = GetGameObject();
        Debug.Log("Start helper ");

        h.createHelper(objectToFollow, title, text, startAudio);
    }


    public void Stop(Helper h)
    {
        h.endHelper(endAudio);
    }
    public IEnumerator StopRoutine(Helper h)
    {
        return h.endHelperInternal(endAudio);
    }
}




/**********************************************


████████╗██╗   ██╗████████╗ ██████╗ ██████╗ ██╗ █████╗ ██╗     
╚══██╔══╝██║   ██║╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗██║     
   ██║   ██║   ██║   ██║   ██║   ██║██████╔╝██║███████║██║     
   ██║   ██║   ██║   ██║   ██║   ██║██╔══██╗██║██╔══██║██║     
   ██║   ╚██████╔╝   ██║   ╚██████╔╝██║  ██║██║██║  ██║███████╗
   ╚═╝    ╚═════╝    ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚══════╝
                                                               

***********************************************/
[RequireComponent(typeof(Helper))]

public abstract class Tutorial : MonoBehaviour
{
    [SerializeField, Tooltip("Audio Sound at start")]
    public AudioClip startAudioClip = null;
    [SerializeField, Tooltip("Audio Sound at end")]
    public AudioClip endAudioClip = null;

    [SerializeField, Tooltip("Audio Sound at the beginning of each sequence")]
    public List<AudioClip> startAudiosPerStep = new List<AudioClip>();
    [SerializeField, Tooltip("Audio Sound at the end of each sequence")]
    public List<AudioClip> endAudiosPerStep = new List<AudioClip>();

    [SerializeField]
    public List<TutoStep> sequences = new List<TutoStep>();
    private int sequenceIndex = 0;
    public bool inProgress = false;

    protected Helper h = null;

    // Start is called before the first frame update
    public virtual void Start()
    {
        h = gameObject.GetComponent<Helper>();
        if (!h) Debug.LogError("Helper not found ??");

    }

    public TutoStep AddSequence(GameObject ObjectToFollow, string Title, string Text)
    {
        TutoStep t = new TutoStep(ObjectToFollow, Title, Text);
        sequences.Add(t);
        int i = sequences.Count - 1;
        AudioClip start = i >= startAudiosPerStep.Count ? null : startAudiosPerStep[i];
        AudioClip end = i >= endAudiosPerStep.Count ? null : endAudiosPerStep[i];

        t.addAudio(start, end);
        return t;
    }
    public TutoStep AddSequence()
    {
        TutoStep t = new TutoStep();
        sequences.Add(t);
        int i = sequences.Count - 1;
        AudioClip start = i >= startAudiosPerStep.Count ? null : startAudiosPerStep[i];
        AudioClip end = i >= endAudiosPerStep.Count ? null : endAudiosPerStep[i];

        t.addAudio(start, end);
        return t;
    }


    public void startTuto()
    {
        if (sequences.Count == 0) return;
        StartCoroutine(startTutoRoutine());
    }

    IEnumerator startTutoRoutine()
    {
        if (startAudioClip)
        {
            AudioSource.PlayClipAtPoint(startAudioClip, gameObject.transform.position);
            yield return new WaitForSeconds(startAudioClip.length + 1);
        }
        sequenceIndex = 0;
        inProgress = true;
        sequences[sequenceIndex].Start(h);
    }


    IEnumerator AchieveSequenceRoutine(int seqNumber)
    {
        if (sequenceIndex == seqNumber)
        {
            yield return sequences[sequenceIndex].StopRoutine(h);

            // start the next helper
            sequenceIndex++;
            if (sequenceIndex == sequences.Count)
            {
                if (endAudioClip)
                {
                    AudioSource.PlayClipAtPoint(endAudioClip, gameObject.transform.position);
                    yield return new WaitForSeconds(endAudioClip.length + 1);
                }
                inProgress = false;
                sequenceIndex = 0;
            }
            else
            {
                sequences[sequenceIndex].Start(h);
            }
        }
    }
    protected void AchieveSequence(int seqNumber)
    {
        StartCoroutine( AchieveSequenceRoutine(seqNumber) );
    }


    public void AchieveTutoStep0()
    {
        AchieveSequence(0);
    }

    public void AchieveTutoStep1()
    {
        AchieveSequence(1);
    }
    public void AchieveTutoStep2()
    {
        AchieveSequence(2);
    }
    public void AchieveTutoStep3()
    {
        AchieveSequence(3);
    }
    public void AchieveTutoStep4()
    {
        AchieveSequence(4);
    }
    public void AchieveTutoStep5()
    {
        AchieveSequence(5);
    }
    public void AchieveTutoStep6()
    {
        AchieveSequence(6);
    }
    public void AchieveTutoStep7()
    {
        AchieveSequence(7);
    }
    public void AchieveTutoStep8()
    {
        AchieveSequence(8);
    }
    public void AchieveTutoStep9()
    {
        AchieveSequence(9);
    }



}
