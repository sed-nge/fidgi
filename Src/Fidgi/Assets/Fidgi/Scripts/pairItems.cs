﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;



public class pairItems : MonoBehaviour
{
    public float dissolveTimer = 5;

    private FidgiObjectScript fidgi;

    [SerializeField]
    private Vector3 PositionOffset = new Vector3(0, 0.5f, 0);
    private Vector3 creationForce = new Vector3(0, 3f, 0);
    private ParticleSystem explosionParticles;


    void Start()
    {
        fidgi = GetComponent<FidgiObjectScript>();
    }


    private void OnTriggerEnter(Collider collider)
    {
        Debug.Log($"Trigger on {fidgi.name} started with: {collider.name}/{collider.tag}");

        Collide(collider.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"Collision on {fidgi.name} started with: {collision.collider.name}/{collision.collider.tag}");

        Collide(collision.gameObject);
    }

    private void Collide(GameObject other)
    {
        if (!fidgi) {
            Debug.Log("Initial object is not a Fidgi one");
            return;
        }

        if (!other.gameObject) return;
        FidgiObjectScript script = other.gameObject.GetComponent<FidgiObjectScript>();

        // this is not an Fidgi Game Script 
        if (!script) {
            Debug.Log("Collider object is not a Fidgi one");
            return;
        }

        if ( Match( other.gameObject ) == true ) {
            Debug.Log("The two Items matches");
            PairWith( other.gameObject );
            globalVariables.incrementCpuUsage( 1f );
        } else {
            Debug.Log("The two Items does not match");
            PairError( other.gameObject );
            globalVariables.incrementCpuUsage( 3f );
        }

    }

      /// <summary>
    /// return if this object match the other object and is the compatible one
    /// </summary>
    /// <param name="other"></param>
    /// <returns></returns>
    public bool Match(GameObject go)
    {
        if (fidgi.isCombined()) return false;

        FidgiObjectScript other = go.GetComponent<FidgiObjectScript>();
        if (!other) return false;
        if (other.isCombined()) return false;
        if (fidgi.matchName == other.matchName) {
        if (fidgi.isCharacter() && other.isInvention()) return true;
        if (fidgi.isInvention() && other.isCharacter()) return true;
        }

        return false;
    }

    /// <summary>
    /// Make the combination of the 2 objects and replace them with the new one at the same place
    /// </summary>
    /// <param name="otherDiskette"></param>
    public void PairWith(GameObject otherDiskette)
    {
        // Pairing only the invention to avoid executing the pairing two times
        if (! fidgi.isInvention()) return;

        FidgiObjectScript other = otherDiskette.GetComponent<FidgiObjectScript>();

        Debug.Log(name + " pairing with " + otherDiskette.name + " type=" + fidgi.type + " other=" + other.type);
        Debug.Log("pairing invention was: " + fidgi.matchName);

        // Activate the combined object
        FidgiObjectScript combined = GetRelatedObject(fidgi.matchName);
        if (!combined) return;
        Debug.Log("Set combined active " + combined.matchName);

        // set the position of the new Object relative to the previous one
        combined.transform.position = gameObject.transform.position + PositionOffset;
        combined.transform.rotation = Random.rotation;
        Rigidbody rc = combined.GetComponent<Rigidbody>();

        rc.useGravity = true;
        //rc.isKinematic = false;
        rc.velocity = new Vector3(0f, 0f, 0f);

        explode();
        otherDiskette.GetComponent<pairItems>().explode();
        // other.explode();
        combined.gameObject.SetActive(true);
        combined.GetComponent<SpawnEffect>().enabled = true;
        // Add a vertical small force to make a jump of the new object
        rc.AddForce(creationForce, ForceMode.Impulse);
        // Add play GoodJob.wav audio file here
    }

    public void PairError(GameObject go)
    {

        FidgiObjectScript other = go.GetComponent<FidgiObjectScript>();
        if (fidgi.isCombined()) return;
        if (other.isCombined()) return;

        Debug.Log(name + "pairing error with " + other.name + " type=" + fidgi.type + " other=" + other.type);

        explode();
        go.GetComponent<pairItems>().explode();
    }

    private FidgiObjectScript GetRelatedObject(string matchName)
    {
        var fidgiObjects = GameObject.Find("FidgiObjects");

        foreach (var fidgi in fidgiObjects.GetComponentsInChildren<FidgiObjectScript>(true))
        {
            if ((fidgi.matchName == matchName) && (fidgi.isCombined())) {
                return fidgi;
            }
        }

        return null;
    }

    /// <summary>
    /// Make an explosion of this object
    /// /// </summary>
    public void explode()
    {
        Debug.Log("explode !");

        SpawnEffect dissolve = this.GetComponent<SpawnEffect>();
        Debug.Log(dissolve.gameObject.name);

        // if (explosionParticles != null)
        if (dissolve != null)
        {
            Debug.Log("explode");
            // explosionParticles.Play();
            // isExploding = true;
            // --- make object disable but not the 
            // dissolve.gameObject.SetActive(true);
            dissolve.enabled = true;
            StartCoroutine(Dissolve());
        }
        else
        {
            Debug.Log("explode not found");
            gameObject.SetActive(false);
        }
    }


    IEnumerator Dissolve()
    {
        //Print the time of when the function is first called.
        Debug.Log("Started Coroutine at timestamp : " + Time.time);

        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(dissolveTimer);

                    SetChildActive(false);
            gameObject.SetActive(false);

        //After we have waited 5 seconds print the time again.
        Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }

    private void SetChildActive(bool active)
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(active);
        }
    }



}