using System.Collections;
using System.Collections.Generic;
using UnityEngine;




/**********************************************
██╗  ██╗███████╗██╗     ██████╗ ███████╗██████╗ 
██║  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗
███████║█████╗  ██║     ██████╔╝█████╗  ██████╔╝
██╔══██║██╔══╝  ██║     ██╔═══╝ ██╔══╝  ██╔══██╗
██║  ██║███████╗███████╗██║     ███████╗██║  ██║
╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝
***********************************************/


[AddComponentMenu("Fidgi Helper")]
public class Helper : MonoBehaviour
{

    [SerializeField, Tooltip("The prefab of the arrow")]

    public GameObject arrowPrefab;
    [SerializeField, Tooltip("The prefab of light")]

    public GameObject LightHelperPrefab;

    [SerializeField, Tooltip("Audio Sound at start")]
    public AudioClip startAudioClip = null;
    [SerializeField, Tooltip("Audio Sound at end")]
    public AudioClip endAudioClip = null;

    private GameObject helpArrow = null;
    private GameObject followObject = null;

    private GameObject lightHelp = null;
    private GameObject followLightObject = null;
    private float endTime = 0f;
    private Vector3 soundPosition = Vector3.zero;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    // Follow the object
    void Update()
    {

        if ((helpArrow) && (followObject))
        {

            helpArrow.transform.LookAt(new Vector3(0, followObject.transform.position.y, 0));
            helpArrow.transform.position = new Vector3(followObject.transform.position.x, followObject.transform.position.y, followObject.transform.position.z);
        }

        if ((lightHelp) && (followLightObject))
        {

            lightHelp.transform.LookAt(new Vector3(0, followLightObject.transform.position.y, 0));
            lightHelp.transform.position = new Vector3(followLightObject.transform.position.x, followLightObject.transform.position.y, followLightObject.transform.position.z);
            if (Time.time > endTime)
            {
                endLightHelper();
            }
        }
    }


    public IEnumerator endHelperInternal(AudioClip clipEnd)
    {
        AudioClip c = clipEnd ? clipEnd : endAudioClip;
        if (helpArrow)
        {
            if (c)
            {
                AudioSource.PlayClipAtPoint(c, soundPosition);
            }
            helpArrow.SetActive(false);
            Destroy(helpArrow);
            helpArrow = null;
            Debug.Log("wait end ");
            if (c) yield return new WaitForSeconds(c.length + 1);
        }
        followObject = null;
    }
    public void endHelper()
    {
        StartCoroutine(endHelperInternal(null));
    }

    public void endHelper(AudioClip clipEnd)
    {
        StartCoroutine(endHelperInternal(clipEnd));
    }
    public void endLightHelper()
    {
        if (lightHelp)
        {
            lightHelp.SetActive(false);
            Destroy(lightHelp);
            lightHelp = null;
            endTime = 0;
        }
        followLightObject = null;
    }



    /// <summary>
    /// Create an helper on a specific GameObject
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="title"></param>
    /// <param name="text"></param>
    public void createLightHelper(GameObject obj, float delay)
    {
        endLightHelper();
        lightHelp = Instantiate(LightHelperPrefab, obj.transform.position, Quaternion.identity);
        followLightObject = obj;
        lightHelp.transform.LookAt(new Vector3(0, followLightObject.transform.position.y, 0));
        endTime = Time.time + delay;
    }

    /// <summary>
    /// Create an helper witn arrow on a specific GameObject
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="title"></param>
    /// <param name="text"></param>
    public void createHelperInternal(GameObject obj, string helpTitle, string helpText, AudioClip startClip)
    {
        endHelper();



        if (obj)
        {
            helpArrow = Instantiate(arrowPrefab, obj.transform.position, Quaternion.identity);
            followObject = obj;
            helpArrow.transform.LookAt(new Vector3(0, followObject.transform.position.y, 0));

            soundPosition = helpArrow.transform.position;

            Transform TitleTrans = helpArrow.transform.Find("Title");
            if (TitleTrans)
            {
                TextMesh title = TitleTrans.gameObject.GetComponent<TextMesh>();
                if (title) title.text = (helpTitle == null) ? "" : helpTitle;
            }
            Transform TextTrans = helpArrow.transform.Find("Text");
            if (TextTrans)
            {
                TextMesh text = TextTrans.gameObject.GetComponent<TextMesh>();
                if (text) text.text = (helpText == null) ? "" : helpText;
            }
        }


        // play the sound
        AudioClip c = startClip ? startClip : startAudioClip;
        if (c)
        {
            AudioSource.PlayClipAtPoint(c, helpArrow.transform.position);
        }


    }
    public void createHelper(GameObject obj, string helpTitle, string helpText)
    {
        createHelperInternal(obj, helpTitle, helpText, null);
    }
    public void createHelper(GameObject obj)
    {
        createHelperInternal(obj, null, null, null);
    }
    public void createHelper(GameObject obj, AudioClip startClip)
    {
        createHelperInternal(obj, null, null, startClip);
    }

    public void createHelper(GameObject obj, string helpTitle, string helpText, AudioClip startClip)
    {
        createHelperInternal(obj, helpTitle, helpText, startClip);
    }

}
