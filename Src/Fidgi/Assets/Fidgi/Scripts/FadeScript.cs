using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

[AddComponentMenu("Fidgi Scene fade")]
[RequireComponent(typeof(RawImage))]

public class FadeScript : MonoBehaviour
{

    [SerializeField, Tooltip("Entry Scene fade speed")]
    public float SceneEntryDelay = 0.5f;
    [SerializeField, Tooltip("Exit scene fade minimum delay")]
    public float SceneExitDelay = 2f;

    private float entryTime;

    private RawImage img;
    private float fullAlpha;

    private float fadeExitpercent = 0f;
    public float ExitPrecent { get { return fadeExitpercent; } set { fadeExitpercent = value; } }

    private AudioSource[] sources;
    private float[] originalVolumes;


    void Start()
    {
        Debug.Log("RawImage Start");


        img = gameObject.GetComponent<RawImage>();
        fullAlpha = img.color.a;

        // --- Select all sound sources to decrease/increase the volume according to the fade
        sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        originalVolumes = new float[sources.Length];
        int i = 0;
        foreach (AudioSource source in sources)
        {
            // Debug.Log(source.gameObject.name);
            originalVolumes[i] = source.volume;
            //source.volume = 0;
            i++;
        }

        StartCoroutine(FadeEnter());
    }

    private void Initialize()
    {
        Debug.Log("RawImage Initialize");

    }


    private void OnEnable()
    {
        Initialize();
    }

    public void ExitFade()
    {

        fadeExitpercent = 0;
        if (!img) return;
        img.color = new Color(img.color.r, img.color.g, img.color.b, 0f);
        StartCoroutine(FadeExit( ));
    }



    IEnumerator FadeEnter()
    {
        setVolume(0);
        entryTime = Time.time;
        while ( (Time.time <= (entryTime + SceneEntryDelay)) || (fadeExitpercent>=1) )
        {
            float t = (Time.time - entryTime) / SceneEntryDelay;
            float a = Mathf.Lerp( fullAlpha, 0, t );
            img.color = new Color(img.color.r, img.color.g, img.color.b, a);
            setVolume(t);
            yield return new WaitForSeconds(0.05f);
        }
        setVolume(1f);
    }

    IEnumerator FadeExit()
    {
        float startTime = Time.time;
        float t = 0f;
        while ( t < 1 )
        {
            t = Mathf.Min( ((Time.time - startTime) / SceneExitDelay), fadeExitpercent );
            float a = Mathf.Lerp( 0, fullAlpha, t);
            img.color = new Color(img.color.r, img.color.g, img.color.b, a);
            setVolume(1 - t);
            yield return new WaitForSeconds(0.05f);
        }
        setVolume(0);
    }

    private void setVolume(float v)
    {
        int i = 0;
        foreach (AudioSource source in sources)
        {
            // Voices are still available
            if (source.gameObject.name != "Voice")
            {
                source.volume = v * originalVolumes[i];
            }
            i++;
        }
    }

}