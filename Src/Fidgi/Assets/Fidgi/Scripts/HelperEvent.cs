using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[AddComponentMenu("Fidgi Helper Event")]

public class HelperEvent : MonoBehaviour
{

    private Helper helpManager;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] helpManagerObjects = GameObject.FindGameObjectsWithTag("Helper");
        if (helpManagerObjects.Length == 0)
        {
            Debug.LogError("No respawn area available (tag Helper)");
        }

        helpManager = helpManagerObjects[0].GetComponent<Helper>();
        if (!helpManager)
        {
            Debug.LogError("No Help manager found");

        }
    }




}
