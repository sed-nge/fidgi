using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


/// <summary>
/// The object for decision. One FidgiObject per
/// item in the game.
/// </summary>
public class FidgiObject
{

    private GameObject o;
    private FidgiObjectScript fidgi;
    private List<FidgiObject> correspondingCombinedOnScene = new List<FidgiObject>();
    private List<FidgiObject> correspondingOnScene = new List<FidgiObject>();
    private List<FidgiObject> correspondingOffScene = new List<FidgiObject>();

    public FidgiObject(GameObject associatedGameObject)
    {
        o = associatedGameObject;
        fidgi = o.GetComponent<FidgiObjectScript>();
    }

    public GameObject gameObject { get { return o; } }

    public bool isOnScene()
    {
        return o.activeSelf;
    }
    public string matchName()
    {
        return fidgi.matchName;
    }

    public void addCorrespondingObject(FidgiObject corresp)
    {
        if (corresp.isVirus()) return;
        if (corresp.isCombined())
        {
            if (corresp.matchName() == matchName())
            {
                if (corresp.isOnScene() == true)
                {
                    correspondingCombinedOnScene.Add(corresp);
                }
            };
            return;
        };

        if (corresp.isCharacter() == isCharacter()) return;
        if (corresp.isInvention() == isInvention()) return;
        if (corresp.matchName() != matchName()) return;

        if (corresp.isOnScene() == true)
        {
            correspondingOnScene.Add(corresp);
        }
        else
        {
            correspondingOffScene.Add(corresp);
        }
    }

    public bool isCombined()
    {
        return fidgi.isCombined();
    }
    public bool isCombinedOnScene()
    {
        if (!fidgi.isCombined()) return false;
        if (!isOnScene()) return false;
        return true;

    }
    public bool isVirus()
    {
        return fidgi.isVirus();
    }
    public bool isCharacter()
    {
        return fidgi.isCharacter();
    }
    public bool isInvention()
    {
        return fidgi.isInvention();
    }

    public bool isCombinable()
    {
        if (isCombined() == true) return false;
        if (isVirus() == true) return false;
        if (isOnScene() == false) return false;
        if (correspondingOnScene.Count > 0)
        {
            Debug.Log(o.gameObject.name + " is on scene and combinable to " + correspondingOnScene[0].o.name);
            return true;
        }
        return false;
    }

    public bool isCandidateToSpawnForAlone()
    {
        if (isCombined() == true) return false;
        if (isOnScene() == true) return false;
        if (isVirus() == true) return false;
        if (correspondingCombinedOnScene.Count > 0) return false;
        if (correspondingOnScene.Count > 0) return false;
        if (isCharacter()) return true;
        if (isInvention()) return true;
        return false;
    }

    public bool isCandidateToSpawnForComplete()
    {
        if (isCombined() == true) return false;
        if (isOnScene() == true) return false;
        if (isVirus() == true) return false;
        if (correspondingCombinedOnScene.Count > 0) return false;
        if (correspondingOnScene.Count == 0) return false;
        if (isCharacter()) return true;
        if (isInvention()) return true;
        return false;
    }


    public void spawnInternal(GameObject[] respawnAreas, GameObject newObj)
    {
        //     Debug.Log("Spawn " + matchName());
        List<GameObject> availableRespawnAreas = new List<GameObject>();

        // find an non empty spawn area
        for (int i = 0; i < respawnAreas.Length; i++)
        {
            GameObject r = respawnAreas[i];
            XRSocketInteractor sp = r.GetComponent<XRSocketInteractor>();
            if (sp == null) continue;
            if (sp.selectTarget != null) continue;
            availableRespawnAreas.Add(r);


        }

        // --- No spawn area available
        if (availableRespawnAreas.Count == 0) return;

        GameObject spawnArea = availableRespawnAreas[Random.Range(0, availableRespawnAreas.Count)];
        Renderer rend = spawnArea.GetComponent<Renderer>();

        // --- Place the object in the center of the area ---
        newObj.transform.position = spawnArea.transform.position;
        newObj.transform.rotation = spawnArea.transform.rotation;
        newObj.SetActive(true);
        return;
    }

    public void spawn(GameObject[] respawnAreas)
    {
        spawnInternal(respawnAreas, o);
    }
    public void spawn(GameObject[] respawnAreas, GameObject newObj)
    {
        spawnInternal(respawnAreas, newObj);
    }

}


















public class Master : MonoBehaviour
{

    [SerializeField, Range(3f, 10.0f), Tooltip("Delay beetween 2 check of viruses")]
    public float checkDelayViruses = 5f;

    [SerializeField, Range(3f, 10.0f), Tooltip("Delay beetween 2 check of spawn")]
    public float checkDelaySpawn = 4f;

    [SerializeField, Range(10f, 60.0f), Tooltip("Blink duration at the end of the game")]
    public float blinkDurationAtTheEnd = 15f;


    [SerializeField, Tooltip("Delay beetween 2 helps")]

    public float delayBeetweenHelp = 120f;


    private FidgiObject virusSource;

    private GameObject[] respawnAreas = null;

    private GameObject[] blinkingObjects = null;


    private int numberOfCellToFill = globalVariables.numberOfCellToFill;

    private GameObject movableItems;
    private GameObject lastExcelCell;
    private GameObject SlotText;
    private DisplayScript[] ReaderDisplays;
    private TextDelay tabletText;

    private SceneSwitcher sceneSwitcher;
    private List<FidgiObject> objects = new List<FidgiObject>();

    private ExcelCell[] excelCells = new ExcelCell[8];


    private AmbiantVoice ambiantVoice;
    private WinAndLost winAndLostSound;
    private float lastHelp = 0f;


    // Start is called before the first frame update
    void Start()
    {
        ReaderDisplays = FindObjectsOfType(typeof(DisplayScript)) as DisplayScript[];

        AmbiantVoice[] s = FindObjectsOfType(typeof(AmbiantVoice)) as AmbiantVoice[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one AmbiantVoice in the scene ;");

        }
        else
        {
            ambiantVoice = s[0];
        }
        WinAndLost[] wl = FindObjectsOfType(typeof(WinAndLost)) as WinAndLost[];
        if (wl.Length != 1)
        {
            Debug.LogError("More than one WinAndLost in the scene ;");

        }
        else
        {
            winAndLostSound = wl[0];
        }



        SlotText = GameObject.Find("SlotText");
        if (!SlotText) Debug.LogError("SlotText Object not found !");
        tabletText = SlotText.GetComponent<TextDelay>();
        if (!tabletText) Debug.LogError("SlotText Object has no TextDelay script !");


        movableItems = GameObject.Find("FidgiObjects");
        if (!movableItems) Debug.LogError("FidgiObjects Object not found !");

        lastExcelCell = GameObject.Find("Cell7");
        if (!lastExcelCell) Debug.LogError("Cell7 Object not found !");

        respawnAreas = GameObject.FindGameObjectsWithTag("Respawn");
        if (respawnAreas.Length == 0)
        {
            Debug.LogError("No respawn area available (tag Respawn)");
        }

        blinkingObjects = GameObject.FindGameObjectsWithTag("blinkingLight");
        if (blinkingObjects.Length == 0)
        {
            Debug.Log("No blinking objects found (tag blinkingLight)");
        }

        // --- Select the sceneswitcher (to go back to the menu scene)
        GameObject sceneSwitcherGo = GameObject.Find("SceneSwitcherMain");
        if (!sceneSwitcherGo)
        {
            Debug.LogError("No SceneSwitcherMain object found !");
        }
        else
        {
            sceneSwitcher = sceneSwitcherGo.GetComponent<SceneSwitcher>();
            if (!sceneSwitcher) Debug.LogError("SceneSwitcherMain doesnt contain the script sceneSwitcher");
        }
        // --- Select excel cells to check if win --
        for (int i = 0; i < 8; i++)
        {
            string cellName = "Cell" + i;
            GameObject cell = GameObject.Find(cellName);
            if (!cell)
            {
                Debug.LogError("No excel cell " + cellName + " found !");
            }
            ExcelCell c = cell.GetComponent<ExcelCell>();
            if (!c) Debug.LogError("Excel cell " + cellName + " is not excel ??");
            excelCells[i] = c;

        }


        StartCoroutine(CheckForSpawnObject());
        StartCoroutine(CheckIfSucceed());
        StartCoroutine(DecreaseTime());

        globalVariables.startGame();


        tabletText.write(globalVariables.numberOfCellToFill + "\r\ncombinaisons\r\nà\r\nsauvegarder");

    }


    private void SpawnVirus()
    {
        if (virusSource == null) return;
        GameObject newVirusObject = Instantiate(virusSource.gameObject);
        newVirusObject.name = virusSource.gameObject.name + "+";
        FidgiObjectScript f = newVirusObject.GetComponent<FidgiObjectScript>();
        f.InfosVisible = false;
        virusSource.spawn(respawnAreas, newVirusObject);
    }


    IEnumerator CheckForSpawnObject()
    {
        while (true)
        {
            yield return new WaitForSeconds(checkDelaySpawn);
            fillCount();

            // number of objects on scene
            int numOfobjects = 0;

            // number of combined objects on scene
            int numOfCombined = 0;

            // number of pairs on scene
            int numOfCombinable = 0;

            // number of viruses on scene
            int numOfViruses = 0;

            List<FidgiObject> spawnableForAlone = new List<FidgiObject>();
            List<FidgiObject> spawnableForComplete = new List<FidgiObject>();


            for (int j = 0; j < objects.Count; j++)
            {
                FidgiObject o = objects[j];
                if (o.isCombinedOnScene()) numOfCombined++;
                if (o.isCombinable()) numOfCombinable++;
                if (o.isVirus())
                {
                    if (o.isOnScene()) numOfViruses++;
                    virusSource = o;
                }
                if (o.isOnScene()) numOfobjects++;
                if (o.isCandidateToSpawnForAlone()) spawnableForAlone.Add(o);
                if (o.isCandidateToSpawnForComplete()) spawnableForComplete.Add(o);

            }
            int numOfAlone = numOfobjects - numOfViruses - numOfCombined - numOfCombinable;

            Debug.Log("Master status : ");
            Debug.Log("num of viruses : " + numOfViruses + ", num of combined : " + numOfCombined + ", num of combinable : " + numOfCombinable + ", num of alone : " + numOfAlone + ", num of objects : " + numOfobjects);

            // --- Spawn a virus ---
            /*
            if (globalVariables.maxNumberOfViruses > numOfViruses)
            {
                // Not start at the beginning or if already some combinied on scene
                if ((numOfCombined > 0) || (Time.realtimeSinceStartup > (globalVariables.gameDuration / 3)))
                {
                    SpawnVirus();
                    continue;
                }
            }
*/

            // --- Give help that you can combine objects
            // StartCoroutine(HelpPairing(numOfCombinable, numOfCombined));

            if ((numOfCombinable > 0) && (numOfCombined == 0))
            {
                if (ambiantVoice)
                {
                    float t = Time.time;
                    if ((t - lastHelp) > delayBeetweenHelp)
                    {
                        if (ambiantVoice.HelpPairing())
                        {
                            lastHelp = t;
                        } else {
                            lastHelp += Random.Range(10,50); 
                        }
                    }
                }
            }

            // --- Give help to use SSD --
            // (if SSD is emtpy and one combined exist)
            if (numOfCombined > 0)
            {
                bool SSDempty = true;
                for (int i = 0; i < globalVariables.numberOfCellToFill; i++)
                {
                    int y = excelCells[i].getObjectDate();
                    if (y != -1)
                    {
                        SSDempty = false;
                        break;
                    }
                }

                // StartCoroutine(HelpSSD(SSDempty));
                
                if (SSDempty)
                {
                    float t = Time.time;
                    if ((t - lastHelp) > delayBeetweenHelp)
                    {
                        if (ambiantVoice.HelpSSD()) {
                            lastHelp = t;
                        } else {
                            lastHelp += Random.Range(10,50); 
                        }
                    }

                }
                
            }

            // --- enough items in the scene ( more than needed )
            if ((numOfCombined + numOfCombinable / 2) > globalVariables.numberOfCellToFill + 2) continue;

            // --- spawn an alone item if number of alone item is half the numberOfCellToFill
            if ((numOfAlone <= globalVariables.numberOfCellToFill / 2) && (spawnableForAlone.Count > 0))
            {
                int randomIndex = Random.Range(0, spawnableForAlone.Count);
                spawnableForAlone[randomIndex].spawn(respawnAreas);
                continue;
            }

            // --- spawn a object to complete 
            if (spawnableForComplete.Count > 0)
            {
                int randomIndex = Random.Range(0, spawnableForComplete.Count);
                spawnableForComplete[randomIndex].spawn(respawnAreas);
                continue;
            }
        }
    }


    /// <summary>
    /// count all objects to decide what to do
    /// </summary>
    private void fillCount()
    {
        objects.Clear();

        FidgiObjectScript[] items = FindObjectsOfType<FidgiObjectScript>(true) as FidgiObjectScript[];

        for (int i = 0; i < items.Length; i++)
        {
            GameObject g = items[i].gameObject;
            //            FidgiObjectScript fidgi = g.GetComponent<FidgiObjectScript>();
            //           if (fidgi == null) continue;


            // --- create new object and link to the others
            FidgiObject o = new FidgiObject(g);
            for (int j = 0; j < objects.Count; j++)
            {
                objects[j].addCorrespondingObject(o);
                o.addCorrespondingObject(objects[j]);
            }
            objects.Add(o);
        }

    }


    IEnumerator CheckIfSucceed()
    {
        // --- sleep close to 2 min before starting
        // yield return new WaitForSeconds(202);

        while (true)
        {
            yield return new WaitForSeconds(checkDelaySpawn);
            int count = 0;
            int year = 0;
            for (int i = 0; i < globalVariables.numberOfCellToFill; i++)
            {
                int y = excelCells[i].getObjectDate();
                if (y == -1) continue;

                if (y >= year)
                {
                    count++;
                    year = y;
                    if (count >= globalVariables.numberOfCellToFill)
                    {

                        tabletText.write("Sauvegarde\r\nen cours\r\n...");

                        float gameTime = Time.time - globalVariables.startTime;
                        float remainingTime = globalVariables.gameDuration - gameTime;
                        Scores.addToScore(100);
                        Scores.addToScore(Mathf.RoundToInt(remainingTime));

                        // Animation for cells
                        for (int j = 0; j < globalVariables.numberOfCellToFill; j++)
                        {
                            excelCells[j].AnimCell();
                        }

                        Scores.currentUserPair = count;
                        if (winAndLostSound) winAndLostSound.Win();

                        yield return new WaitForSeconds(2f);
                        if (sceneSwitcher) sceneSwitcher.GotoMenuScene();
                        yield break;
                    }
                    Scores.currentUserPair = count;
                    tabletText.write((globalVariables.numberOfCellToFill - count) + "\r\ncombinaisons\r\nà\r\nsauvegarder");
                }
                else
                {
                    tabletText.write("Erreur\r\nordre\r\nchronologique");
                    break;
                }
            }
        }
    }

    private void playWinAnimation() {
        for ( int i=0 ;i<ReaderDisplays.Length; i++) {
            ReaderDisplays[i].PlayWinVideo();
        }
    }



    /// <summary>
    /// After half time, check every second if time finished.
    /// If finished... failed.
    /// start blinking lights 
    /// </summary>
    /// <returns></returns>
    IEnumerator DecreaseTime()
    {
        bool blinkInProgress = false;
        yield return new WaitForSeconds(globalVariables.gameDuration / 2);

        while (true)
        {
            yield return new WaitForSeconds(1);
            float gameTime = Time.time - globalVariables.startTime;

            // --- Finish - you loose !!!
            if (gameTime > globalVariables.gameDuration)
            {
                tabletText.write("Batterie\r\ndéchargée");
                if (winAndLostSound) winAndLostSound.Lost();
                if (sceneSwitcher) sceneSwitcher.GotoMenuScene();
                yield break;
            }

            // -- start the blink (close to the end)
            if (!blinkInProgress)
            {
                float remainingTime = globalVariables.gameDuration - gameTime;
                if (remainingTime < blinkDurationAtTheEnd)
                {
                    blinkInProgress = true;
                    for (int i = 0; i < blinkingObjects.Length; i++)
                    {
                        LightMaterials l = blinkingObjects[i].GetComponent<LightMaterials>();
                        if (l != null) l.blink(remainingTime);
                        LightManager lm = blinkingObjects[i].GetComponent<LightManager>();
                        if (lm != null) lm.blink(remainingTime);
                    }
                }
            }
        }
    }

}

//write static in front of the variable, then in the other script you can access by typing
//origscriptname.variablename
