using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(Animator))]
[AddComponentMenu("Fidgi RAM Drawer")]


// The C# convention for class names is PascalCase.
public class RamDrawer : MonoBehaviour
{

    [SerializeField, Tooltip("Respawn areas in the drawer")]

    public List<GameObject> RespawnAreas = new List<GameObject>();

    [SerializeField, Tooltip("Delay beetween check")]
    public float checkDelay = 1f;
    [SerializeField, Tooltip("Start delay")]
    public float startDelay = 5f;

    private Animator animator;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        StartCoroutine(checkIfContent());
    }

    IEnumerator checkIfContent()
    {
        yield return new WaitForSeconds(startDelay);
        while (true)
        {
            yield return new WaitForSeconds(checkDelay);
            bool AtTeastOne = false;
            for (int i = 0; i < RespawnAreas.Count; i++)
            {
                if (isFilled(RespawnAreas[i]))
                {
                    AtTeastOne = true;
                    break;
                }
            }

            // --- one respawn area is filled : open the drawer
            if (AtTeastOne)
            {
                animator.SetBool("open", true);
                yield return new WaitForSeconds(1.5f);
                moveAreasUp();
            }
            else
            {
                moveAreasDown();
                yield return new WaitForSeconds(1.5f);
                animator.SetBool("open", false);
            }
        }
    }


    /// <summary>
    /// Called by the behaviour script to anim respawn areas to up
    /// </summary>
    public void moveAreasUp()
    {
        for (int i = 0; i < RespawnAreas.Count; i++)
        {
            Animator anim = RespawnAreas[i].GetComponent<Animator>();
            if (!anim) continue;
            anim.SetBool("open", true);
        }
    }

    /// <summary>
    /// Called by the behaviour script to anim respawn areas to up
    /// </summary>
    public void moveAreasDown()
    {
        for (int i = 0; i < RespawnAreas.Count; i++)
        {
            Animator anim = RespawnAreas[i].GetComponent<Animator>();
            if (!anim) continue;
            anim.SetBool("open", false);
        }
    }




    /// <summary>
    /// Check if one respawnArea is filled 
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    private bool isFilled(GameObject obj)
    {
        XRSocketInteractor xr = obj.GetComponent<XRSocketInteractor>();
        if (xr == null) return false;
        if (xr.selectTarget) return true;
        return false;
    }

}