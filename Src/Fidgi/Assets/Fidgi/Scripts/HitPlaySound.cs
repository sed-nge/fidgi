using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Fidgi Sound on hitSoundClip")]

public class HitPlaySound : MonoBehaviour
{
    [SerializeField, Range(0.0f, 1.0f), Tooltip("Minimum of time before another hitSoundClip")]

    public float delay = 0.3f;
    [SerializeField, Tooltip("Sound for hitSoundClip")]

    public AudioClip hitSoundClip = null;

    private AudioSource sound;
    private float lastHitTime = 0f;
    void Start()
    {

        sound = gameObject.GetComponent<AudioSource>();
        sound.loop = false;
        sound.playOnAwake = false;

    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 3)
        {            
            if ( hitSoundClip == null ) return;

            // --- Hit with the hand of the player. ignore it.
            if ( collision.gameObject.tag == "Player" ) return;

            // --- Another hitSoundClip close to this one. ignore it.
            if (( Time.time - lastHitTime ) <= delay ) return;

            // compute the volume according to the force
            float f = collision.relativeVelocity.magnitude > 10f ? 10f : collision.relativeVelocity.magnitude;
            sound.volume = f / 10f;
            // Debug.Log("collision magniture with " + collision.gameObject.name + " at " + collision.relativeVelocity.magnitude+ " volume "+ f);
            sound.PlayOneShot(hitSoundClip);
            lastHitTime = Time.time;
        }
    }


}
