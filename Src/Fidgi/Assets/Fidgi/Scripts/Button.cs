using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

[AddComponentMenu("Fidgi Button")]
// [RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]

public class Button : MonoBehaviour
{

    [SerializeField, Tooltip("Color of the button")]
    public Color color;
    [SerializeField, Tooltip("Highlight of the button")]
    public Color Hightlightcolor;
    [SerializeField, Tooltip("Text Color")]
    public Color TextColor;

    [SerializeField, Tooltip("Bip On appear")]
    public AudioClip bip;
    public float pitch = 0.2f;
    public float delta = 0.01f;


    [SerializeField, Tooltip("Text of the button")]
    public string text;
    [SerializeField, Tooltip("Text size")]
    public int fontSize = 100;

    [SerializeField, Tooltip("Random start audio")]
    public List<AudioClip> randomStartAudios = new List<AudioClip>();


    [SerializeField, Range(0.1f, 5.0f), Tooltip("Speed factor")]
    public float speed = 1f;

    [SerializeField, Tooltip("text to display")]

    public GameObject displayText;

    [SerializeField, Tooltip("trigger")]
    public UnityEvent onTriggerEvent = new UnityEvent();
    [SerializeField, Tooltip("On enter Event")]
    public UnityEvent onEnterEvent = new UnityEvent();

    [SerializeField, Tooltip("enabled")]
    public bool enable = true;

    [SerializeField, Tooltip("intensity")]
    public float intensity = 5f;

    [SerializeField, Tooltip("Respawn delay")]
    public float RespawnDelay = 10f;
    [SerializeField, Tooltip("Start delay")]
    public float startDelay = 5f;

    private List<GameObject> circles = new List<GameObject>();
    private GameObject allCircle = null;

    private TextMesh txt;

    private Color32 textNormalColor;
    private Color32 selectedTextNormalColor;

    protected AudioSource audioPlayer;

    private IEnumerator currentRoutine = null;
    private float scale = 0f;
    private float startingSoundPitch = 0f;
    private VoicePlayer voicePlayer;

    void Start()
    {

        audioPlayer = gameObject.GetComponent<AudioSource>();
        if (audioPlayer) startingSoundPitch = audioPlayer.pitch;

        gameObject.transform.localScale = new Vector3(0, 0, 0);


        VoicePlayer[] s = FindObjectsOfType(typeof(VoicePlayer)) as VoicePlayer[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one VoicePlayer in the scene ;");

        }
        else
        {
            voicePlayer = s[0];
        }


        Transform AllCirclesTransform = gameObject.transform.Find("Circles");
        if (AllCirclesTransform) allCircle = AllCirclesTransform.gameObject;

        for (int i = 2; i < 33; i++)
        {
            string name = string.Format("Circles/Circle.{0:000}", i);
            Transform CircleTransform = gameObject.transform.Find(name);
            if (!CircleTransform) continue;
            circles.Add(CircleTransform.gameObject);
            CircleTransform.gameObject.SetActive(false);
        }



        if (displayText)
        {
            txt = displayText.GetComponent<TextMesh>();
            if (txt)
            {
                //txt.text = text;
                txt.fontSize = fontSize;
            }
        }

        setColors();

    }

    private void OnEnable()
    {
        gameObject.transform.localScale = new Vector3(0, 0, 0);
        StartCoroutine(Raise());
    }


    IEnumerator Raise()
    {
        yield return new WaitForSeconds(startDelay);
        yield return Appear();
    }

    public void SetText(string newText, int newFontSize)
    {
        txt.text = newText;
        txt.fontSize = fontSize;
    }
    public void SetText(string newText)
    {
        txt.text = newText;
    }

    public void SetEnable(bool ena)
    {
        enable = ena;
        setColors();
    }

    private void PlaySound(int pitchLevel)
    {
        if (audioPlayer == null) return;
        float pitch = startingSoundPitch + delta * pitchLevel;
        audioPlayer.pitch = pitch;
        audioPlayer.Play();
    }

    private void setColors()
    {
        Color c = enable ? color : Color.gray;


        textNormalColor = new Color32((byte)Mathf.RoundToInt(c.r * 255), (byte)Mathf.RoundToInt(c.g * 255), (byte)Mathf.RoundToInt(c.b * 255), (byte)Mathf.RoundToInt(c.a * 255));
        selectedTextNormalColor = new Color32((byte)Mathf.RoundToInt(Hightlightcolor.r * 255), (byte)Mathf.RoundToInt(Hightlightcolor.g * 255), (byte)Mathf.RoundToInt(Hightlightcolor.b * 255), (byte)Mathf.RoundToInt(Hightlightcolor.a * 255));
        textNormalColor = selectedTextNormalColor;

        Transform tt = gameObject.transform.Find("Circles/plain");
        if (tt)
        {
            changeColor(tt.gameObject, c);
            changeEmissionColor(tt.gameObject, Hightlightcolor, intensity / 3);
        }

        for (int i = 0; i < circles.Count; i++)
        {
            changeColor(circles[i], Hightlightcolor);
            changeEmissionColor(circles[i], Hightlightcolor, intensity);
        }


        if (txt)
        {
            txt.color = textNormalColor;
        }

    }


    IEnumerator highLightRotate()
    {
        for (int i = 0; i < circles.Count; i++) circles[i].SetActive(false);

        for (int i = 0; i < circles.Count; i++)
        {
            circles[i].SetActive(true);
            PlaySound(i);
            yield return new WaitForSeconds(speed / 30);


        }

        for (int i = 0; i < circles.Count; i++) circles[i].SetActive(false);
        yield return Disappear();
        gameObject.transform.localScale = new Vector3(0, 0, 0);

        // --- Play voice to say something ---
        voicePlayer.abortCurrentSound();
        voicePlayer.PlayRandom(randomStartAudios, 0, 0.5f);

        onExec();
    }

    IEnumerator downLightRotate()
    {
        for (int i = circles.Count - 1; i >= 0; i--)
        {
            if (circles[i].activeSelf == false) continue;
            circles[i].SetActive(false);
            PlaySound(i);

            yield return new WaitForSeconds(speed / 30);
        }
    }


    IEnumerator Disappear()
    {
        gameObject.transform.localScale = new Vector3(1, 1, 1);
        for (scale = 1f; scale > 0; scale -= 0.1f)
        {
            gameObject.transform.localScale = new Vector3(scale, scale, scale);
            yield return new WaitForSeconds(0.02f);
        }
    }

    IEnumerator Appear()
    {
        gameObject.transform.localScale = new Vector3(0, 0, 0);
        for (scale = 0f; scale < 1f; scale += 0.1f)
        {
            gameObject.transform.localScale = new Vector3(scale, scale, scale);
            yield return new WaitForSeconds(0.02f);

        }
    }



    public void OnTriggerEnter(Collider other)
    {
        GameObject o = other.gameObject;

        if (o.tag != "Player") return;
        if (!enable) return;
        if (txt) txt.color = selectedTextNormalColor;
        if (scale < 1f) return;


        if (onEnterEvent != null)
        {
            if (onEnterEvent.GetPersistentEventCount() > 0) onEnterEvent.Invoke();
        }

        if (currentRoutine != null) StopCoroutine(currentRoutine);
        currentRoutine = highLightRotate();
        StartCoroutine(currentRoutine);
    }


    public void OnTriggerExit(Collider other)
    {
        GameObject o = other.gameObject;
        if (o.tag != "Player") return;
        if (!enable) return;
        if (txt) txt.color = textNormalColor;
        if (scale < 1f) return;

        if (currentRoutine != null) StopCoroutine(currentRoutine);
        currentRoutine = downLightRotate();
        StartCoroutine(currentRoutine);
    }



    private void changeColor(GameObject o, Color color)
    {

        Renderer render = o.GetComponent<Renderer>();
        if (!render) return;
        // Debug.Log("Change color for " + o.name);
        Material[] mats = render.materials;
        foreach (Material m in mats)
        {
            m.SetColor("_BaseColor", color);
        }
    }
    private void changeEmissionColor(GameObject o, Color color, float intens)
    {

        Renderer render = o.GetComponent<Renderer>();
        if (!render) return;
        // Debug.Log("Change emission color for " + o.name);
        Material[] mats = render.materials;
        foreach (Material m in mats)
        {
            m.SetColor("_EmissionColor", color * intens);
            m.EnableKeyword("_EMISSION");
        }
    }


    IEnumerator Respawn()
    {
        yield return new WaitForSeconds(RespawnDelay);
        // anim.SetBool("restart", true);
    }


    public void onExec()
    {
        if (onTriggerEvent == null) return;
        if (onTriggerEvent.GetPersistentEventCount() == 0) return;
        onTriggerEvent.Invoke();
    }

    public void onExit()
    {
        gameObject.SetActive(false);
    }


}