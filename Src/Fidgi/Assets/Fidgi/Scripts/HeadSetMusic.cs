using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEngine.XR.Interaction.Toolkit;


[RequireComponent(typeof(XRSocketInteractor))]
[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Fidgi HeadSet Music")]


public class HeadSetMusic : Exe
{

    private AudioSource audioPlay;

    // Start is called before the first frame update
    public override void Start()
    {

        base.Start();
        audioPlay = GetComponent<AudioSource>();

    }


    public override void OnSelectEnter()
    {
        Debug.Log("Enter Music ? "); ;
        target = gameObject.GetComponent<XRSocketInteractor>().selectTarget.gameObject;
        if (!target) return;
        if (target.name != "headset")
        {
            ThrowObject();
            return;
        }
        Debug.Log("Enter Music"); ;
        audioPlay.Play();
    }

    public override void OnSelectExit()
    {
        audioPlay.Stop();
        base.OnSelectExit();
    }


}
