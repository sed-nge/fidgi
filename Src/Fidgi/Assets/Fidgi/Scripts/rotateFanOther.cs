﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateFanOther : MonoBehaviour
{
    //Vector3 fanSpeed;
    //public Vector3 rotationSpeed = new Vector3(0, 0, 5);
    // Update is called once per frame

    private static Vector3 fanRotationSpeed = new Vector3(0, 0, 5);
    private static Vector3 fanRotation = new Vector3(0, 0, 0);


    void Update()
    {
        // Change the speed according to the cpuUsage
        fanRotationSpeed = new Vector3(0, 0, globalVariables.cpuUsage / 100f * -5.0f);

        // Set the transformation
        fanRotation+=fanRotationSpeed;
        transform.localEulerAngles = fanRotation;
    }
}