using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;


[RequireComponent(typeof(XRController))]
[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(XRInteractorLineVisual))]
[RequireComponent(typeof(AudioSource))]
// [RequireComponent(typeof(ParticleSystem))]

public class handController : MonoBehaviour
{
    public InputDeviceCharacteristics controllerCharacteristics;
    public List<GameObject> controllerPrefabs;

    public float gripMax = 0.2f;

    private GameObject spawnedController;
    private GameObject spawnedHandModel;


    [SerializeField, Tooltip("Check if hand is the left hand")]
    public bool LeftHand = false;


    [SerializeField, Tooltip("Laser pitch")]
    public float pitch;



    public GameObject handModelPrefab;
    //public InputDevice device;
    private Animator handAnimator;
    private XRController device;

    private InputDevice targetDevice;

    ParticleSystem myParticleSystem;
    private LineRenderer lineRenderer;
    private XRInteractorLineVisual Ray;
    private XRRayInteractor XRC;

    private AudioSource sound;

    private float normalpitch;
    private float currentpitch;
    private float grabInitialDistance = 2f;

    // Start is called before the first frame update
    void Start()
    {

        sound = gameObject.GetComponent<AudioSource>();
        normalpitch = sound.pitch;
        XRC = GetComponent<XRRayInteractor>();


        Ray = GetComponent<XRInteractorLineVisual>();
        lineRenderer = GetComponent<LineRenderer>();

        myParticleSystem = GetComponent<ParticleSystem>();

        List<InputDevice> devices = new List<InputDevice>();
        InputDeviceCharacteristics rightdeviceCharacteristics = controllerCharacteristics | InputDeviceCharacteristics.Controller;

        InputDevices.GetDevicesWithCharacteristics(rightdeviceCharacteristics, devices);
        //        InputDevices.GetDevices(devices);

        foreach (var item in devices)
        {
            Debug.Log(item.name + item.characteristics);
        }

        if (devices.Count > 0)
        {
            targetDevice = devices[0];
        }
        else
        {
            Debug.LogError("No target device found");
        }

        spawnedHandModel = Instantiate(handModelPrefab, transform);
        handAnimator = spawnedHandModel.GetComponent<Animator>();
        if (!handAnimator)
        {
            Debug.LogError("no hand Animator");
            return;
        }
        device = spawnedHandModel.GetComponent<XRController>();

        // Debug.Log(" handAnimator " + handAnimator.isActiveAndEnabled);

        StartLaser();


    }


    void UpdateHandAnimation()
    {
        if (targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            
            handAnimator.SetFloat("Trigger", triggerValue);
        }
        else
        {
            handAnimator.SetFloat("Trigger", 0f);
        }
        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            handAnimator.SetFloat("Grip", gripValue * gripMax);
        }
        else
        {
            handAnimator.SetFloat("Grip", 0f);
        }
    }


    public void playHaptic(float intensity, float duration)
    {
        targetDevice.SendHapticImpulse(0, intensity, duration);
    }

    public void updateLaser(float distance)
    {
        sound.pitch = pitch * 2 - ((distance / grabInitialDistance) * pitch);
    }


    public void StartLaser(bool selected = false)
    {
        Ray.enabled = true;
        sound.pitch = normalpitch;
        sound.Play();

    }
    public void StopLaser()
    {
        Ray.enabled = false;
        sound.pitch = normalpitch;
        sound.Stop();
    }


    public bool isRightHand()
    {
        return LeftHand ? false : true;
    }
    public bool isLeftHand()
    {
        return LeftHand;
    }


    public Vector2 getPrimary2DAxis()
    {
        if (targetDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 v))
        {
            return v;
        }
        return Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHandAnimation();

    }
}
