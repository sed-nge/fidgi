﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// --- use it for Outline objects --
using cakeslice;

public class DragDropMouse : MonoBehaviour
{
    private bool isSelectable = false;
    public bool isSelected = false;
    private float distance;
    private Vector3 offset;
    private Rigidbody rb;

    private Vector3 previousPosition;
    void Start()
    {
        // --- save the previous position --
        previousPosition = transform.position;
        GetComponent<Outline>().enabled = false;

        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        if (isSelectable != GetComponent<Outline>().enabled)
        {
            GetComponent<Outline>().enabled = isSelectable;
        }

        if (isSelected == true)
        {

            if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
            {
                distance += 0.1f;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
            {
                distance -= 0.1f;
            }

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 rayPoint = ray.GetPoint(distance);

            // --- save the previous position --
            previousPosition = transform.position;

            rb.MovePosition(rayPoint + offset);
            //transform.position = rayPoint + offset;
            isSelectable = true;
        }


        if (isSelectable != GetComponent<Outline>().enabled)
        {
            GetComponent<Outline>().enabled = isSelectable;
        }

    }

    private void OnMouseExit()
    {
        isSelectable = false;
    }

    void OnMouseEnter()
    {
        isSelectable = true;
    }

    private void OnMouseDown()
    {
        if (!isSelectable) return;
        isSelected = true;
        rb.isKinematic = true;
        Vector3 screenSpace = Camera.main.WorldToScreenPoint(transform.position);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));
        distance = Vector3.Distance(transform.position, Camera.main.transform.position) - 0.3f;
    }
    private void OnMouseUp()
    {
        if (!isSelected) return;
        isSelected = false;
        rb.isKinematic = false;
    }
}
