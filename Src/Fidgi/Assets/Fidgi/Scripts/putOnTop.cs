﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/**
 * Make the animation if approach the top of the
 * character.
 */
public class putOnTop : MonoBehaviour
{
    private GameObject icon;
    private Animator iconAnimator;

    // Start is called before the first frame update
    void Start()
    {
        GameObject parent = transform.parent.gameObject;
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            GameObject child = parent.transform.GetChild(i).gameObject;
            if (child.tag == "icon")
            {
                iconAnimator = child.GetComponent<Animator>();
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        // Check if the items as a pair
        if (transform.parent.name+"_invention" == other.gameObject.name)
        {
            iconAnimator.SetBool("rotate", true);
        }
        //if items are not a pair, destroy both items (Pending: explosion and delay)
        else 
        {
            Destroy(other.gameObject);
            Destroy(transform.parent.gameObject);
        }
    }
    // This only happens when a correclty paired item exits the collider
    void OnTriggerExit(Collider other)
    {
            iconAnimator.SetBool("rotate", false);
    }
}
