using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.AI;
using UnityEngine.XR.Interaction.Toolkit;




[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(XRGrabInteractable))]
[RequireComponent(typeof(FidgiObjectScript))]
[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Fidgi Virus AI Script")]

public class VirusAI : MonoBehaviour
{


    [SerializeField, Tooltip("ExplanationMessage")]
    public List<AudioClip> virusExplanation = new List<AudioClip>();


    [SerializeField, Tooltip("Virus sound when move")]
    public AudioClip virusSound;


    [SerializeField, Tooltip("sound when a memory become a virus")]
    public AudioClip wakeUpSound;

    [SerializeField, Tooltip("Virus sound volume")]
    public float volume = 1f;

    [SerializeField, Tooltip("Virus alert messages")]
    public List<AudioClip> alertMessages = new List<AudioClip>();
    [SerializeField, Tooltip("Virus distance for Message")]
    public int virusDistanceAlertMessage = 7;


    [SerializeField, Tooltip("Virus maximum hunter time before choosing a new target")]
    public float virusDelay = 30f;

    private NavMeshAgent agent;
    private Rigidbody rb;
    private FidgiObjectScript fidgi;

    private XRGrabInteractable xr;

    private GameObject virusTarget;
    private float virusTargetStartTime;

    private bool mustRestartAgentSoon = false;

    private Vector3 PositionOffset = new Vector3(0, 0.5f, 0);

    private int lastTargetPathDistance = 0;

    private AudioSource sound;

    private bool fistTimeVirusExplanationDone = false;

    private AmbiantVoice ambiantVoice;
    private float oldVolume = 0f;

    void Start()
    {

        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        xr = GetComponent<XRGrabInteractable>();
        fidgi = GetComponent<FidgiObjectScript>();
        sound = gameObject.GetComponent<AudioSource>();

        AmbiantVoice[] s = FindObjectsOfType(typeof(AmbiantVoice)) as AmbiantVoice[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one AmbiantVoice in the scene");

        }
        else
        {
            ambiantVoice = s[0];
        }


        agent.enabled = false;
        rb.isKinematic = false;

        StartCoroutine(VirusThinking());
    }

    private void playSound(AudioClip s)
    {
        oldVolume = sound.volume;
        sound.volume = volume;
        sound.PlayOneShot(s);
        sound.volume = oldVolume;

    }

    private bool isSelected()
    {
        return xr.isSelected;
    }
    private bool isHovered()
    {
        return xr.isHovered;
    }



    public void OnHoverEnterVirus()
    {
        Debug.Log("Virus Hover Enter");
        agent.enabled = false;
        rb.isKinematic = false;
    }

    public void OnHoverExitVirus()
    {
        Debug.Log("Virus Hover Exit");
        mustRestartAgentSoon = true;
        //agent.enabled = true;
        //rb.isKinematic = true;
    }


    public void OnSelectEnterVirus()
    {
        Debug.Log("Virus Select Enter");
    }

    public void OnSelectExitVirus()
    {
        Debug.Log("Virus Select Exit");
    }


    public bool PairingInProgress()
    {
        return fidgi.PairingInProgress();
    }

    /// <summary>
    /// Choose a target for virus on sceen
    /// </summary>
    /// <returns></returns>
    private GameObject selectTarget(GameObject precedentTarget)
    {
        FidgiObjectScript[] items = FindObjectsOfType<FidgiObjectScript>(true) as FidgiObjectScript[];
        List<GameObject> candidates = new List<GameObject>();
        List<int> pathLength = new List<int>();

        GameObject BestCandidate = null;
        int pathLengthToBestCandidate = 0;

        for (int i = 0; i < items.Length; i++)
        {
            // --- not a virus 
            if (items[i].isVirus()) continue;
            // --- actually disabled
            if (!items[i].gameObject.activeSelf) continue;
            // --- actually grabbed
            //            if (items[i].gameObject.GetComponent<Grab>().isSelected) continue;
            if (items[i].gameObject.GetComponent<XRGrabInteractable>().isSelected) continue;
            // --- not the previous one
            if ((precedentTarget) && (items[i].gameObject.GetInstanceID() == precedentTarget.GetInstanceID())) continue;

            candidates.Add(items[i].gameObject);

            NavMeshPath path = new NavMeshPath();
            NavMesh.CalculatePath(transform.position, items[i].gameObject.transform.position, NavMesh.AllAreas, path);
            if (path.corners.Length > 0)
            {
                if ((path.corners.Length < pathLengthToBestCandidate) || (pathLengthToBestCandidate == 0))
                {
                    pathLengthToBestCandidate = path.corners.Length;
                    BestCandidate = items[i].gameObject;
                }
            }

        }

        if (BestCandidate) return BestCandidate;

        // --- no candidate, return the precedent by default, or null
        if (candidates.Count == 0)
        {
            if (precedentTarget) return precedentTarget;
            return null;
        }

        // --- return a target by random
        int index = Random.Range(0, candidates.Count);
        return candidates[index];
    }


    private void virusThink()
    {


        // --- currently in pairing ---
        if (fidgi.InfosVisible == false)
        {
            Debug.Log("Virus " + gameObject.name + " info not visible");
            //agent.enabled = false;
            return;
        }

        // --- If selected or hovered, do nothing
        if (isSelected())
        {
            Debug.Log("Virus " + gameObject.name + " selected");
            //agent.enabled = false;
            return;
        }
        if (isHovered())
        {
            Debug.Log("Virus " + gameObject.name + " hovered");
            return;
        }


        if (mustRestartAgentSoon == true)
        {
            mustRestartAgentSoon = false;
            return;
        }

        lastTargetPathDistance = 0;
        agent.enabled = true;
        agent.destination = transform.position;
        rb.isKinematic = true;

        // --- agent not enabled ---
        if (agent.enabled == false)
        {
            Debug.Log("Virus " + gameObject.name + " agent not set");
            return;
        }


        // --- currently in pairing ---
        if (fidgi.PairingInProgress() == true)
        {
            Debug.Log("Virus " + gameObject.name + " paring");
            return;
        }

        // --- Agent not on a navMesh
        if (agent.isOnNavMesh == false)
        {
            Debug.Log("Virus " + gameObject.name + " not on navmesh");
            return;
        }

        // --- select new target --- 
        if (virusTarget == null)
        {
            Debug.Log("Virus " + gameObject.name + " search new target");
            virusTarget = selectTarget(virusTarget);
            virusTargetStartTime = Time.time;
            if (virusTarget) Debug.Log("Virus " + gameObject.name + " got new target " + virusTarget.name);
        }

        // --- change target because not active --- 
        if ((virusTarget) && (virusTarget.activeSelf == false))
        {
            Debug.Log("Virus " + gameObject.name + " target " + virusTarget.name + " disabled, change target");
            virusTarget = selectTarget(virusTarget);
            virusTargetStartTime = Time.time;
            if (virusTarget) Debug.Log("Virus " + gameObject.name + " got new target " + virusTarget.name);
        }

        // --- change target because pairing --- 
        if ((virusTarget) && (virusTarget.GetComponent<FidgiObjectScript>().PairingInProgress() == true))
        {
            Debug.Log("Virus " + gameObject.name + " target " + virusTarget.name + " paring, change target");
            virusTarget = selectTarget(virusTarget);
            virusTargetStartTime = Time.time;
            if (virusTarget) Debug.Log("Virus " + gameObject.name + " got new target " + virusTarget.name);
        }

        // --- change target because is a virus --- 
        if ((virusTarget) && (virusTarget.GetComponent<FidgiObjectScript>().isVirus() == true))
        {
            Debug.Log("Virus " + gameObject.name + " target " + virusTarget.name + " is now a virus, change target");
            virusTarget = selectTarget(virusTarget);
            virusTargetStartTime = Time.time;
            if (virusTarget) Debug.Log("Virus " + gameObject.name + " got new target " + virusTarget.name);
        }

        // --- change target because too much time on it --- 
        if ((virusTarget) && ((Time.time - virusTargetStartTime) > virusDelay))
        {
            Debug.Log("Virus " + gameObject.name + " target " + virusTarget.name + " too much time, change target");
            virusTarget = selectTarget(virusTarget);
            virusTargetStartTime = Time.time;
            if (virusTarget) Debug.Log("Virus " + gameObject.name + " got new target " + virusTarget.name);
        }

        // --- adapt to the target position or set in my position because no target or target currently selected.
        Transform destination = transform;
        if ((virusTarget) && (!virusTarget.GetComponent<XRGrabInteractable>().isSelected)) destination = virusTarget.transform;
        agent.destination = destination.position;
    }


    IEnumerator VirusThinking()
    {
        Debug.Log(gameObject.name + " start thinking");
        yield return new WaitForSeconds(2f);
        agent.enabled = true;

        resetTarget();

        while (true)
        {
            yield return new WaitForSeconds(2f);
            virusThink();
            adjustSpeed();
            playVirusSound();
        }
    }


    /// <summary>
    /// Adjust the speed to make virus more or less dangerous
    /// </summary>
    private void adjustSpeed()
    {
        return;
    }

    private void playVirusSound()
    {
        if (agent.enabled == false) return;
        if (sound == null) return;
        if (fidgi.InfosVisible == false) return;
        if (agent.hasPath == false) return;

        // --- play the first explanation for virses
        if (fistTimeVirusExplanationDone == false)
        {
            Debug.Log("play virus explanation " + (ambiantVoice.isMuted()));
            fistTimeVirusExplanationDone = true;
            if (ambiantVoice)
            {
                ambiantVoice.PlayRandom(virusExplanation, 0, 0);
            }
            return;
        }

        // --- Play the sound on each path changement ---
        if ((lastTargetPathDistance == 0) || (agent.path.corners.Length < lastTargetPathDistance))
        {
            lastTargetPathDistance = agent.path.corners.Length;
            if ((lastTargetPathDistance < virusDistanceAlertMessage) && (ambiantVoice))
            {
                Debug.Log("play virus alert " + (ambiantVoice.isMuted()));
                ambiantVoice.PlayRandom(alertMessages, 0, 1f);
            }

            playSound(virusSound);
        }
    }

    public void resetTarget()
    {
        if (agent.enabled) agent.destination = transform.position;
        virusTarget = null;
        virusTargetStartTime = 0;
    }


    public void CloneMe(Vector3 position, Quaternion rotation)
    {
        // --- Spawn a copy of me ---

        rb.isKinematic = false;
        GameObject newVirus = Instantiate(gameObject);
        rb.isKinematic = true;

        newVirus.name = gameObject.name + "_";

        // set the position of the new Object relative to the previous one
        newVirus.transform.position = position + PositionOffset;
        newVirus.transform.rotation = Random.rotation;

        fidgi.ThrowUp(newVirus);

        // --- play wakeUpSound 
        playSound(wakeUpSound);

    }





}
