using UnityEngine;
using System.Collections.Generic;
using System.Collections;


[AddComponentMenu("Fidgi Light Management")]



/// <summary>
/// Manage material light emission.
/// For each sub gameObjects contaning a material with a light emission,
/// modify the light
/// </summary>
public class LightMaterials : MonoBehaviour
{

    private class MaterialStore
    {
        public Material material;
        public Color emissionColor;

        public MaterialStore(Material m, Color e)
        {
            material = m;
            emissionColor = e;
        }

    };

    private List<MaterialStore> EmmitingMaterials = new List<MaterialStore>();

    private IEnumerator constantBlinking = null;

    public virtual void Start()
    {
        selectEmittingMaterial(gameObject);
        constantBlinking = RandomBlink();
        StartCoroutine(constantBlinking);
    }

    /// <summary>
    /// Select and store the material if there is an emissionColor
    /// and do it reccursively
    /// </summary>
    /// <param name="go"></param>
    private void selectEmittingMaterial(GameObject go)
    {
        Renderer render = go.GetComponent<Renderer>();
        if (render)
        {
            Material[] mats = render.materials;
            foreach (Material m in mats)
            {
                Color c = m.GetColor("_EmissionColor");
                if (c.r != 0f || c.g != 0f || c.b != 0f)
                {
                    //                    Debug.Log("Found a material with an emission color " + c.ToString());
                    EmmitingMaterials.Add(new MaterialStore(m, c));
                }
            }
        }
        for (int i = 0; i < go.transform.childCount; i++)
        {
            Transform t = go.transform.GetChild(i);
            selectEmittingMaterial(t.gameObject);
        }

        return;
    }

    public void stopEmiting()
    {
        for (int i = 0; i < EmmitingMaterials.Count; i++)
        {
            EmmitingMaterials[i].material.SetColor("_EmissionColor", Color.black);
        }
    }
    public void startEmiting()
    {
        for (int i = 0; i < EmmitingMaterials.Count; i++)
        {
            EmmitingMaterials[i].material.SetColor("_EmissionColor", EmmitingMaterials[i].emissionColor);
        }
    }
    public void blink(float duration)
    {
        StopCoroutine(constantBlinking);
        startEmiting();
        StartCoroutine(Blink(duration));
    }



    IEnumerator Blink(float duration)
    {
        Debug.Log("Start Blink for " + duration + " seconds");
        float t = Time.time;
        float maxBlickOff = 0.2f;
        float maxBlickOOn = 1f;
        float rtime = Time.time - t;
        while (rtime < duration)
        {
            stopEmiting();
            yield return new WaitForSeconds(Random.Range(0.1f, maxBlickOff));
            startEmiting();
            yield return new WaitForSeconds(Random.Range(0.1f, maxBlickOOn));
            rtime = Time.time - t;
            maxBlickOff = (rtime / duration) + 0.2f;
            maxBlickOOn = ((duration - rtime) / duration) + 0.2f;

        }
        stopEmiting();

    }


    IEnumerator RandomBlink()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(5f, 20f));
            stopEmiting();
            yield return new WaitForSeconds(0.5f);
            startEmiting();
        }
    }


}
