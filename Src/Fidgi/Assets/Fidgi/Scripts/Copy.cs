using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(GenericSoundEvent))]
[AddComponentMenu("Fidgi Copy Script")]
/// <summary>
/// Manage the display of a video / gameobject / Slideshow
/// </summary>
public class Copy : MonoBehaviour
{

    [SerializeField, Tooltip("Source drawer")]
    public GameObject src;
    [SerializeField, Tooltip("Destination drawer")]
    public GameObject dst;
    [SerializeField, Tooltip("Source drawer interactor ")]
    public GameObject srcInt;
    [SerializeField, Tooltip("Destination drawer interactor ")]
    public GameObject dstInt;
    [SerializeField, Tooltip("FidgiObjects")]
    public GameObject fidgiObjects;
    [SerializeField, Tooltip("Light Circle")]
    public GameObject lightCircle;
    [SerializeField, Tooltip("Delay help")]
    public float delayBeetweenHelp = 100f;


    private XRSocketInteractor XRSrc;
    private XRSocketInteractor XRDst;
    private Animator AnimSrc;
    private Animator AnimDst;

    private AmbiantVoice ambiantVoice;
    private GenericSoundEvent sound;
    private bool copyUsed = false;


    public void Start()
    {
        if (!src) Debug.LogError("No src drawer in Copy script");
        if (!dst) Debug.LogError("No dst drawer in Copy script");
        if (!srcInt) Debug.LogError("No src drawer Interactor in Copy script");
        if (!dstInt) Debug.LogError("No dst drawer Interactor in Copy script");

        sound = gameObject.GetComponent<GenericSoundEvent>();

        XRSrc = srcInt.GetComponent<XRSocketInteractor>();
        if (!XRSrc) Debug.LogError("No XRSocketInteractor in src drawer");

        XRDst = dstInt.GetComponent<XRSocketInteractor>();
        if (!XRDst) Debug.LogError("No XRSocketInteractor in dst drawer");

        AnimSrc = src.GetComponent<Animator>();
        if (!AnimSrc) Debug.LogError("No Animator in src drawer");

        AnimDst = dst.GetComponent<Animator>();
        if (!AnimDst) Debug.LogError("No Animator in dst drawer");


        AmbiantVoice[] s = FindObjectsOfType(typeof(AmbiantVoice)) as AmbiantVoice[];
        if (s.Length != 1)
        {
            Debug.LogError("More than one AmbiantVoice in the scene");

        }
        else
        {
            ambiantVoice = s[0];
            StartCoroutine( CheckIfUsedToGiveClue() );
        }



    }


    IEnumerator CheckIfUsedToGiveClue()
    {
        while (copyUsed == false )
        {
            yield return new WaitForSeconds(delayBeetweenHelp + Random.Range(10,50) );
            yield return new WaitWhile(() => ambiantVoice.isMuted());
            yield return new WaitForSeconds(Random.Range(10,50) );
            if (copyUsed == false) ambiantVoice.HelpCPU();
        }
    }



    private void OnTriggerEnter(Collider collider)
    {

        Debug.Log("-- A fidgi object enter " + collider.gameObject.name);

        if (!collider.gameObject) return;

        // --- Not a fidgi object, ignore
        FidgiObjectScript script = collider.gameObject.GetComponent<FidgiObjectScript>();
        if (!script) return;

        Debug.Log("--- A fidgi object enter " + collider.gameObject.name);

        // --- already one memory in the source
        if (XRSrc.selectTarget) return;

        Debug.Log("--- 1 A fidgi object enter " + collider.gameObject.name);

        // --- already one memory in the destination
        if (XRDst.selectTarget) return;

        Debug.Log("--- 2 A fidgi object enter " + collider.gameObject.name);

        // --- Open the source drawer and  close the Dst one
        AnimSrc.SetBool("open", true);
        AnimDst.SetBool("open", false);

    }



    private void OnTriggerExit(Collider collider)
    {

        if (!collider.gameObject) return;

        // --- Not a fidgi object, ignore
        FidgiObjectScript script = collider.gameObject.GetComponent<FidgiObjectScript>();
        if (!script) return;

        // --- already one memory in the source
        if (XRSrc.selectTarget) return;
        AnimSrc.SetBool("open", false);


        // --- already one memory in the destination
        if (XRDst.selectTarget) return;
        AnimDst.SetBool("open", false);

    }


    /// <summary>
    /// An object as been put in the src 
    /// /// </summary>
    public void srcObjectIn()
    {
        // --- already one memory in the destination
        if (XRDst.selectTarget) return;
        StartCoroutine(copyAnim());
    }


    /// <summary>
    /// An object as been put in the src 
    /// /// </summary>
    public void srcObjectOut()
    {
        AnimSrc.SetBool("open", false);
    }

    /// <summary>
    /// An object as been put in the src 
    /// /// </summary>
    public void dstObjectOut()
    {
        AnimDst.SetBool("open", false);
    }

    IEnumerator copyAnim()
    {
        AnimSrc.SetBool("open", false);
        AnimDst.SetBool("open", false);

        yield return new WaitForSeconds(0.2f);

        // --- start the animation
        if (lightCircle)
        {
            CPULight cpuLightScript = lightCircle.GetComponent<CPULight>();
            if (cpuLightScript) yield return cpuLightScript.Serpent(1, 0.1f);
        }

        // --- create the dst object
        GameObject newOne = Instantiate(XRSrc.selectTarget.gameObject, fidgiObjects.transform);

        newOne.GetComponent<Rigidbody>().isKinematic = false;
        newOne.GetComponent<Rigidbody>().useGravity = true;


        newOne.transform.position = XRDst.transform.position;

        copyUsed = true;

        AnimSrc.SetBool("open", true);
        AnimDst.SetBool("open", true);

        yield return new WaitForSeconds(0.5f);

        // --- play sound finished ---
        sound.PlayOnceSound0();
    }

}
