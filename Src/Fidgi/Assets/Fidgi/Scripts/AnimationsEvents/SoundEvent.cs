using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public abstract class SoundEvent : MonoBehaviour
{

    [SerializeField, Tooltip("List of Sounds")]
    public List<AudioClip> Sounds = new List<AudioClip>();

    protected AudioSource audioPlayer;

    public virtual void Start()
    {
        audioPlayer = gameObject.GetComponent<AudioSource>();

    }


    private AudioClip selectSound(int i)
    {
        if (i >= Sounds.Count)
        {
            Debug.Log("No sound rank [" + i + "] in list");
            return null;
        }
        AudioClip sound = Sounds[i];
        if (!sound)
        {
            Debug.Log("No sound rank [" + i + "] in list");
            return null;
        }
        return sound;

    }

    public void PlaySound(int i)
    {
        AudioClip sound = selectSound(i);
        if ( sound == null ) return;

        audioPlayer.Stop();
        audioPlayer.clip = sound;
        audioPlayer.loop = true;
        audioPlayer.Play();
    }

    public void PlayOnceSound(int i)
    {
        AudioClip sound = selectSound(i);
        if ( sound == null ) return;

        audioPlayer.Stop();
        audioPlayer.clip = sound;
        audioPlayer.loop = false;
        audioPlayer.Play();
    }




    public void PlaySound0()
    {
        PlaySound(0);
    }
    public void PlaySound1()
    {
        PlaySound(1);
    }
    public void PlaySound2()
    {
        PlaySound(2);
    }
    public void PlaySound3()
    {
        PlaySound(3);
    }


    public void PlayOnceSound0()
    {
        PlayOnceSound(0);
    }
    public void PlayOnceSound1()
    {
        PlayOnceSound(1);
    }
    public void PlayOnceSound2()
    {
        PlayOnceSound(2);
    }
    public void PlayOnceSound3()
    {
        PlayOnceSound(3);
    }


    public void Stop()
    {
        audioPlayer.Stop();
    }

}
