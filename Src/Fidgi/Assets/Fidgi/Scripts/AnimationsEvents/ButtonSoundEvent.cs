using UnityEngine;
using System.Collections.Generic;
using System.Collections;


[AddComponentMenu("Fidgi Button Sound Event")]



/// <summary>
/// Manage material light emission.
/// For each sub gameObjects contaning a material with a light emission,
/// modify the light
/// </summary>
public class ButtonSoundEvent : SoundEvent
{

    public float pitch = 0.2f;
    public float delta = 0.01f;

    private float myPitch;


    public override void Start()
    {

        base.Start();
        myPitch = pitch;
    }


    public void reinit()
    {
        myPitch = pitch;
    }

    public void playPitchIncrease()
    {
        audioPlayer.pitch = myPitch;

        PlayOnceSound(0);
        myPitch += delta;

    }

}
