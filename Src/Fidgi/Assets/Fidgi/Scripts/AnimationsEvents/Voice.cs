using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Fidgi oldv")]

public class Voice : MonoBehaviour
{
    [SerializeField, Tooltip("Minimum pause time beetween messages")]
    public float PauseDelay = 1f;

    [SerializeField, Tooltip("Delay before start welcome messages")]
    public float WelcomeDelay = 5f;


    [SerializeField, Tooltip("Welcome messages")]
    public List<AudioClip> Welcome = new List<AudioClip>();

    [SerializeField, Tooltip("Win messages")]
    public List<AudioClip> WinMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Lost messages")]
    public List<AudioClip> LostMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Abort messages")]
    public List<AudioClip> AbortMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Wrong pair messages")]
    public List<AudioClip> WrongPairMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Good pair messages")]
    public List<AudioClip> GoodPairMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Delay before another help messages")]
    public float HelpMessageDelay = 45f;
    [SerializeField, Tooltip("Help messages")]
    public List<AudioClip> HelpMessages = new List<AudioClip>();

    [SerializeField, Tooltip("Battery messages")]
    public List<AudioClip> BatteryMessages = new List<AudioClip>();



    private AudioSource audioPlayer;


    private IEnumerator playSound = null;

    private float lastHelpMessageTime = -1f;

    public virtual void Start()
    {
        audioPlayer = gameObject.GetComponent<AudioSource>();
        PlayRandomSound(Welcome, WelcomeDelay);
    }


    private void abortCurrentSound()
    {
        if (playSound == null) return;
        StopCoroutine(playSound);
        playSound = null;
    }

    private AudioClip selectRandomSound(List<AudioClip> Sounds)
    {
        AudioClip sound = null;
        int count = 0;
        while ((sound == null) || (count > 10))
        {
            int i = Random.Range(0, Sounds.Count);
            sound = Sounds[i];
            count++;
        }
        if (!sound)
        {
            Debug.Log("No sound found in list");
            return null;
        }
        return sound;
    }


    public void PlayRandomSound(List<AudioClip> Sounds, float startDelay)
    {
        AudioClip sound = selectRandomSound(Sounds);
        if (sound == null) return;

        // --- check if currently talking ---
        if (playSound != null)
        {
            Debug.Log("Cannot talk, a voice is currently talking");
            return;
        }

        playSound = PlaySoundRoutine(sound, startDelay);
        StartCoroutine(playSound);
    }


    IEnumerator PlaySoundRoutine(AudioClip sound, float startDelay)
    {
        yield return new WaitForSeconds(startDelay);
        audioPlayer.Stop();
        audioPlayer.clip = sound;
        audioPlayer.loop = false;
        audioPlayer.Play();
        yield return new WaitForSeconds(sound.length + PauseDelay);

        // --- ugly ? stop and reset the coroutine --
        StopCoroutine(playSound);
        playSound = null;
    }


    public void PlaySequencialSound(List<AudioClip> Sounds, float delay, float startDelay)
    {
        // --- check if currently talking ---
        if (playSound != null)
        {
            Debug.Log("Cannot talk, a voice is currently talking");
            return;
        }
        playSound = PlaySequencialSoundDelay(Sounds, delay, startDelay);
        StartCoroutine(playSound);
    }


    IEnumerator PlaySequencialSoundDelay(List<AudioClip> Sounds, float delay, float startDelay)
    {
        yield return new WaitForSeconds(startDelay);
        for (int i = 0; i < Sounds.Count; i++)
        {
            AudioClip sound = Sounds[i];
            audioPlayer.Stop();
            audioPlayer.clip = sound;
            audioPlayer.loop = false;
            audioPlayer.Play();
            yield return new WaitForSeconds(sound.length + delay);
        }

        yield return new WaitForSeconds(PauseDelay);

        // --- ugly ? stop and reset the coroutine --
        StopCoroutine(playSound);
        playSound = null;
    }


    /*

    ██████╗  █████╗ ████████╗████████╗███████╗██████╗ ██╗   ██╗
    ██╔══██╗██╔══██╗╚══██╔══╝╚══██╔══╝██╔════╝██╔══██╗╚██╗ ██╔╝
    ██████╔╝███████║   ██║      ██║   █████╗  ██████╔╝ ╚████╔╝ 
    ██╔══██╗██╔══██║   ██║      ██║   ██╔══╝  ██╔══██╗  ╚██╔╝  
    ██████╔╝██║  ██║   ██║      ██║   ███████╗██║  ██║   ██║   
    ╚═════╝ ╚═╝  ╚═╝   ╚═╝      ╚═╝   ╚══════╝╚═╝  ╚═╝   ╚═╝   

    */
    public void playBatteryMessage(int percent)
    {
        


    }



    /*


    ██╗  ██╗███████╗██╗     ██████╗ 
    ██║  ██║██╔════╝██║     ██╔══██╗
    ███████║█████╗  ██║     ██████╔╝
    ██╔══██║██╔══╝  ██║     ██╔═══╝ 
    ██║  ██║███████╗███████╗██║     
    ╚═╝  ╚═╝╚══════╝╚══════╝╚═╝     


    */

    public void PlayHelpMessage(int[] indexes)
    {
        PlayHelpMessageInternal(indexes);
    }
    public void PlayHelpMessage(int index)
    {
        int[] indexes = new int[1];
        indexes[0] = index;
        PlayHelpMessageInternal(indexes);
    }

    private void PlayHelpMessageInternal(int[] indexes)
    {
        float t = Time.time;
        if (lastHelpMessageTime > 0)
        {
            if (t - lastHelpMessageTime < HelpMessageDelay) return;
        }
        lastHelpMessageTime = t;

        int index = Random.Range(0, indexes.Length);
        AudioClip sound = HelpMessages[indexes[index]];

        // --- check if currently talking ---
        if (playSound != null)
        {
            Debug.Log("Cannot talk, a voice is currently talking");
            return;
        }
        playSound = PlaySoundRoutine(sound, 0f);
        StartCoroutine(playSound);

    }

    /*
     ██████╗ █████╗ ██╗     ██╗     ███████╗
    ██╔════╝██╔══██╗██║     ██║     ██╔════╝
    ██║     ███████║██║     ██║     ███████╗
    ██║     ██╔══██║██║     ██║     ╚════██║
    ╚██████╗██║  ██║███████╗███████╗███████║
     ╚═════╝╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝
    */


    public void Lost() { abortCurrentSound(); PlayRandomSound(LostMessages, 0); }
    public void Abort() { abortCurrentSound(); PlayRandomSound(AbortMessages, 0); }
    public void Win() { abortCurrentSound(); PlayRandomSound(WinMessages, 0); }
    public void WrongPair() { abortCurrentSound(); PlayRandomSound(WrongPairMessages, 0); }


}
