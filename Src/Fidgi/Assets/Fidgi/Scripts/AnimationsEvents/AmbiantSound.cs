using UnityEngine;
using System.Collections.Generic;
using System.Collections;

[AddComponentMenu("Fidgi Ambiant Sound")]

/// <summary>
/// Manage ambiant sound.
/// Play randomly sounds from the list
/// </summary>
public class AmbiantSound : SoundEvent
{


    [SerializeField, Range(0.0f, 10.0f), Tooltip("Delay beetween two sounds")]
    public float delay = 3f;
    [SerializeField, Range(0.0f, 1.0f), Tooltip("Volume")]
    public float volume = 0.5f;

    private int index = 0;
    public override void Start()
    {

        base.Start();
        index = Random.Range(0, Sounds.Count);

        StartCoroutine(playAmbientSound());
    }

    IEnumerator playAmbientSound()
    {
        //index=0;
        while (true)
        {
            AudioClip sound = Sounds[index];
            audioPlayer.Stop();
            audioPlayer.clip = sound;
            audioPlayer.volume = volume;
            audioPlayer.loop = false;
            audioPlayer.Play();
            yield return new WaitForSeconds(sound.length + delay);
            index++;
            if (index >= Sounds.Count) index = 0;
        }
    }

}
