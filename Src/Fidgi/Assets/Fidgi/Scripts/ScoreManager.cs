﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Score Manager Script")]
[RequireComponent(typeof(TextMesh))]

public class ScoreManager : MonoBehaviour
{

    [SerializeField, Range(0.0f, 3.0f), Tooltip("delay beetween two update")]
    public float updateInterval = 1f;


    private int oldScore = 0;
    private TextMesh txt;
    
    void Start()
    {
        Scores.currentUserScore = 0;

        txt = gameObject.GetComponent<TextMesh>();
                StartCoroutine(CheckScore());

    }

    IEnumerator CheckScore()
    {

        while (true)
        {
            yield return new WaitForSeconds(updateInterval);
            
            if ( Scores.currentUserScore != oldScore ) {
                txt.text = string.Format("{0:0000}", Scores.currentUserScore);
                oldScore = Scores.currentUserScore;
            }
        }
    }


    public void changeText(string batteryLevel)
    {

    }

}